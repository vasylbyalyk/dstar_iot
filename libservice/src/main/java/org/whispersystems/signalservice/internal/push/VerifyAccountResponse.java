package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VerifyAccountResponse {
  @JsonProperty
  private String uuid;

  @JsonProperty
  private boolean storageCapable;

  @SuppressWarnings("unused")
  public VerifyAccountResponse() {

  }

  public VerifyAccountResponse(String uuid, boolean storageCapable) {
    this.uuid = uuid;
    this.storageCapable = storageCapable;
  }

  public String getUuid() {
    return uuid;
  }

  public boolean isStorageCapable() {
    return storageCapable;
  }
}
