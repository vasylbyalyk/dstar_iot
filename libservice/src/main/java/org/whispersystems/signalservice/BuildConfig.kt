package org.whispersystems.signalservice

object BuildConfig {
    var appVersion = -1

    fun getAppVersionString() = appVersion.toString()
}