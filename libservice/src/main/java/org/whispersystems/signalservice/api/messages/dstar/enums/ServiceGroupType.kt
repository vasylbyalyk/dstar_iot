package org.whispersystems.signalservice.api.messages.dstar.enums

import org.whispersystems.signalservice.internal.push.SignalServiceProtos

enum class ServiceGroupType(val type: Int) {
    TEXT_SECURE(0),
    MMS(1),
    PTT(2),
    CLOUD_DRIVE(3),
    NOTES(4),
    ROOM(5);

    companion object {
        @JvmStatic
        fun toEnum(int: Int): ServiceGroupType {
            return values()[int]
        }

        @JvmStatic
        fun fromProto(groupType: SignalServiceProtos.GroupType): ServiceGroupType {
            return toEnum(groupType.number)
        }
    }

    fun toGroupTypeProto(): SignalServiceProtos.GroupType {
        return SignalServiceProtos.GroupType.forNumber(type)
    }
}