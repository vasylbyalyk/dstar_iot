package org.whispersystems.signalservice.api.messages.dstar

import kotlinx.serialization.Serializable
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.DeleteRequest
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.GetRequest
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.Response
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.SaveRequest
import org.whispersystems.signalservice.api.messages.dstar.extensions.*
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.HistorySyncType

@Serializable
sealed class DstarMessage {
    abstract fun messageExt(): DstarMessageExt
}

@Serializable
data class EmailMessage(
        val id: String = DEF_ID,
        val timeStampCreated: Long = 0,
        val subject: String = DEF_SUBJECT,
        val target: String,
        val nickname: String = DEF_NICKNAME,
        val to: List<String> = DEF_TO,
        val cc: List<String> = DEF_CC,
        val bcc: List<String> = DEF_BCC,
        val isRecipientVerified: Boolean = DEF_IS_RECIPIENT_VERIFIED,
        val from: String = DEF_FROM,
        private val ext: DstarMessageExt = DstarMessageExt.empty()) : DstarMessage() {

    override fun messageExt() = ext
}

@Serializable
data class MessengerMessage(private val ext: DstarMessageExt = DstarMessageExt.empty()) : DstarMessage() {

    companion object {
        @JvmStatic
        fun empty() = MessengerMessage(DstarMessageExt.Builder().build())

        @JvmStatic
        fun hidden() = MessengerMessage(DstarMessageExt.Builder().asHidden().build())

        @JvmStatic
        fun flickering() = MessengerMessage(DstarMessageExt.Builder().asFlickering().build())

        @JvmStatic
        fun disappearing(lifeTimeMillis: Long) = MessengerMessage(DstarMessageExt.Builder().asDisappearing(lifeTimeMillis).build())

    }

    override fun messageExt() = ext
}

@Serializable
data class PttMessage(private val ext: DstarMessageExt = DstarMessageExt.empty()) : DstarMessage() {
    override fun messageExt() = ext
}

@Serializable
class BackupMessage(var getRequest: GetRequest? = null,
                    var saveRequest: SaveRequest? = null,
                    var deleteRequest: DeleteRequest? = null,
                    var response: Response? = null) : DstarMessage() {
    override fun messageExt() = DstarMessageExt.empty()
}

// ----> IotMessage
@Serializable
sealed class IotMessage : DstarMessage() {
    override fun messageExt() = DstarMessageExt.empty()
}

@Serializable
data class SetupIotMessage(val senderId: String, val setupJson: String): IotMessage()

@Serializable
data class DynamicIotMessage(val dynamicJson: String): IotMessage()
// <---- IotMessage

// ----> HistorySyncMessage
@Serializable
sealed class HistorySyncMessage : DstarMessage() {
    override fun messageExt() = DstarMessageExt.empty()
}

@Serializable
data class GroupHistorySyncMessage(val type: HistorySyncType, val groupId: ByteArray, val recipients: List<String>): HistorySyncMessage() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GroupHistorySyncMessage

        if (type != other.type) return false
        if (!groupId.contentEquals(other.groupId)) return false
        if (recipients != other.recipients) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + groupId.contentHashCode()
        result = 31 * result + recipients.hashCode()
        return result
    }
}

@Serializable
data class DesktopHistorySyncMessage(val type: HistorySyncType, val deviceId: Int): HistorySyncMessage()
// <---- HistorySyncMessage