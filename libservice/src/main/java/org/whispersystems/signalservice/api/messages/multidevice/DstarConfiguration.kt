package org.whispersystems.signalservice.api.messages.multidevice

import org.whispersystems.signalservice.internal.push.SignalServiceProtos

data class DstarConfiguration(val os: OS, val versionCode: String)

enum class OS {
    ANDROID,
    IOS;

    fun mapToProtoOs() = when (this) {
        ANDROID -> SignalServiceProtos.SyncMessage.Configuration.OS.ANDROID
        IOS -> SignalServiceProtos.SyncMessage.Configuration.OS.IOS
    }

    companion object {
        fun mapToObjectOs(os: SignalServiceProtos.SyncMessage.Configuration.OS) = when (os) {
            SignalServiceProtos.SyncMessage.Configuration.OS.ANDROID -> ANDROID
            SignalServiceProtos.SyncMessage.Configuration.OS.IOS -> IOS
        }
    }
}