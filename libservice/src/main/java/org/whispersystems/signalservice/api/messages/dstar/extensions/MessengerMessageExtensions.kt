package org.whispersystems.signalservice.api.messages.dstar.extensions

import org.whispersystems.signalservice.api.messages.dstar.DstarMessageExt
import org.whispersystems.signalservice.api.messages.dstar.MessengerMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.MessengerMessageProto
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DstarMessageExtProto

// COMPANION
internal fun MessengerMessage.Companion.parse(proto: MessengerMessageProto): MessengerMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1, extension)
        throw UpdateRequiredException()
    }
}

private fun MessengerMessage.Companion.parse(proto: MessengerMessageProto.Version1, extension: DstarMessageExtProto?): MessengerMessage {
    val ext = extension?.let { DstarMessageExt.parse(extension) } ?: DstarMessageExt.empty()
    return MessengerMessage(ext)
}

// INSTANCE
fun MessengerMessage.messageBuilder(): MessengerMessageProto.Builder {
    return MessengerMessageProto.newBuilder()
            .apply {
                setV1(builderV1())
                setExtension(messageExt().buildProto())
            }
}

private fun builderV1(): MessengerMessageProto.Version1.Builder {
    return MessengerMessageProto.Version1.newBuilder()
}
