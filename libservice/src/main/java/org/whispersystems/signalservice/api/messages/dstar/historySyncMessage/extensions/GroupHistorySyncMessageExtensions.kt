package org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions

import com.google.protobuf.ByteString
import org.whispersystems.signalservice.api.messages.dstar.GroupHistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.HistorySyncType
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HistorySyncMessage.Version1.Group

// COMPANION
fun GroupHistorySyncMessage.Companion.parseV1(proto: Group): GroupHistorySyncMessage {
    return with(proto) {
        GroupHistorySyncMessage(HistorySyncType.mapToObjectType(type), groupId.toByteArray(), recipientsList)
    }
}

// INSTANCE
fun GroupHistorySyncMessage.builder(): Group.Builder = Group.newBuilder()
        .apply {
            type = this@builder.type.mapToProtoType()
            groupId = ByteString.copyFrom(this@builder.groupId)
            addAllRecipients(recipients)
        }

