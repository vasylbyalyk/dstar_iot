package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HiddenMessageExtProto

@Serializable
class HiddenMessageExt {

    companion object {

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: HiddenMessageExtProto): HiddenMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: HiddenMessageExtProto.Version1) = HiddenMessageExt()
    }

    fun toJson() = Json.stringify(serializer(), this)

    internal fun builder() = HiddenMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = HiddenMessageExtProto.Version1.newBuilder()
}