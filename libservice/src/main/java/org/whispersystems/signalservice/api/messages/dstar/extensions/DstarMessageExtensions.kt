package org.whispersystems.signalservice.api.messages.dstar.extensions

import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.*
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.backupBuilder
import org.whispersystems.signalservice.api.messages.dstar.backupMessage.parse
import org.whispersystems.signalservice.api.messages.dstar.exception.UndefinedDstarMessageException
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.api.messages.dstar.ext.DeleteMessageExtV2
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.HistorySyncType
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions.historySyncBuilder
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions.parse
import org.whispersystems.signalservice.api.messages.dstar.iotMessage.iotBuilder
import org.whispersystems.signalservice.api.messages.dstar.iotMessage.parse
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto

// COMPANION
fun DstarMessage.Companion.fromJson(json: String) = Json.parse(serializer(), json)

fun DstarMessage.Companion.parse(proto: DstarMessageProto): DstarMessage {
    with(proto) {
        if (hasV1()) return parse(v1)
        throw UpdateRequiredException()
    }
}

private fun DstarMessage.Companion.parse(proto: DstarMessageProto.Version1): DstarMessage {
    with(proto) {
        if (hasMessenger()) return MessengerMessage.parse(messenger)
        if (hasEmail()) return EmailMessage.parse(email)
        if (hasPtt()) return PttMessage.parse(ptt)
        if (hasBackup()) return BackupMessage.parse(backup)
        if (hasSync()) return HistorySyncMessage.parse(sync)
        if (hasIot()) return IotMessage.parse(iot)
        throw UndefinedDstarMessageException()
    }
}

// INSTANCE
fun DstarMessage.isMessengerEmpty() = messageExt().isEmpty
fun DstarMessage.isHidden() = messageExt().hiddenMessageExt?.let { true } ?: false
fun DstarMessage.isFlickering() = messageExt().flickeringMessageExt?.let { true } ?: false
fun DstarMessage.isDisappearing() = getLifeTimeMillis() > 0L
fun DstarMessage.isSpecial() = isHidden() || isFlickering() || isDisappearing()

fun DstarMessage.isEdit() = messageExt().editMessageExt?.let { true } ?: false
fun DstarMessage.isForward() = messageExt().forwardMessageExt?.let { true } ?: false
fun DstarMessage.isDelete() = messageExt().deleteMessageExt?.let { true } ?: false
fun DstarMessage.isBackup() = this is BackupMessage
fun DstarMessage.isGroupHistorySyncResponse() = (this as? GroupHistorySyncMessage)?.type == HistorySyncType.RESPONSE
fun DstarMessage.isDesktopHistorySync() = this is DesktopHistorySyncMessage
fun DstarMessage.isPttMessage() = this is PttMessage
fun DstarMessage.isEmail() = this is EmailMessage
fun DstarMessage.isSetupIotMessage() = this is SetupIotMessage
fun DstarMessage.isDynamicIotMessage() = this is DynamicIotMessage


fun DstarMessage.getLifeTimeMillis() = messageExt().disappearingMessageExt?.lifeTimeMillis ?: 0L
fun DstarMessage.getEditMessage() = messageExt().editMessageExt
fun DstarMessage.getDeleteMessage() = messageExt().deleteMessageExt

fun DstarMessage.getDeleteMessageExtV2(): DeleteMessageExtV2? = with(messageExt().deleteMessageExt) {
    return if (this is DeleteMessageExtV2) this
    else null
}

fun DstarMessage.toJson() = Json.stringify(DstarMessage.serializer(), this)

fun DstarMessage.builder() = DstarMessageProto.newBuilder()
        .apply {
            setV1(builderV1())
        }

private fun DstarMessage.builderV1() = DstarMessageProto.Version1.newBuilder()
        .apply {
            when (this@builderV1) {
                is MessengerMessage -> setMessenger(messageBuilder())
                is EmailMessage -> setEmail(emailBuilder())
                is PttMessage -> setPtt(pttBuilder())
                is HistorySyncMessage -> setSync(historySyncBuilder())
                is BackupMessage -> setBackup(backupBuilder())
                is IotMessage -> setIot(iotBuilder())
            }
        }