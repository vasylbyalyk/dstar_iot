package org.whispersystems.signalservice.api

import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage
import org.whispersystems.signalservice.api.messages.dstar.BackupMessage
import org.whispersystems.signalservice.api.messages.dstar.HistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.extensions.isBackup
import org.whispersystems.signalservice.api.messages.dstar.extensions.isDelete
import org.whispersystems.signalservice.api.messages.dstar.extensions.isEdit
import org.whispersystems.signalservice.api.push.SignalServiceAddress

enum class DstarType(val code: Int) {
    REGULAR_MESSAGE(0),
    SYSTEM_SILENT(1),
    PEER_TO_PEER_CALL(2),
    PEER_TO_PEER_MESH_CALL(3);

}

fun createDstarTypes(dataMessage: SignalServiceDataMessage, recipients: List<SignalServiceAddress>): Map<SignalServiceAddress, DstarType> {
    return recipients.map {
        it to getDstarTypeFor(dataMessage, it)
    }.toMap()
}

fun getDstarTypeFor(dataMessage: SignalServiceDataMessage, recipient: SignalServiceAddress): DstarType {
    // check for dstarMessage
    if (dataMessage.dstarMessage.isPresent) {
        val dstarMessage = dataMessage.dstarMessage.get()
        if (dstarMessage is HistorySyncMessage
                || dstarMessage.isBackup()
                || dstarMessage.isDelete() || dstarMessage.isEdit()) {
            return DstarType.SYSTEM_SILENT
        }
    }
    // check for group update
    if (dataMessage.isGroupV1Update) {
        return DstarType.SYSTEM_SILENT
    }
    val reaction = dataMessage.reaction.orNull()
    if (reaction != null && (reaction.isRemove || reaction.targetAuthor != recipient)) {
        return DstarType.SYSTEM_SILENT
    }
    return if (dataMessage.isExpirationUpdate) {
        DstarType.SYSTEM_SILENT
    } else DstarType.REGULAR_MESSAGE
}