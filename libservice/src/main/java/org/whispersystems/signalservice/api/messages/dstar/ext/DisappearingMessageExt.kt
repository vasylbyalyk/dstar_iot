package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DisappearingMessageExtProto

@Serializable
data class DisappearingMessageExt(val lifeTimeMillis: Long) {

    companion object {
        private const val DEF_TIME_STAMP_SENT = 10L

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: DisappearingMessageExtProto): DisappearingMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: DisappearingMessageExtProto.Version1): DisappearingMessageExt {
            with(proto) {
                val lifeTimeMillis = if (hasLifeTimeMillis()) lifeTimeMillis else DEF_TIME_STAMP_SENT
                return DisappearingMessageExt(lifeTimeMillis)
            }
        }
    }

    fun toJson() = Json.stringify(serializer(), this)

    internal fun builder() = DisappearingMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = DisappearingMessageExtProto.Version1.newBuilder()
            .apply {
                lifeTimeMillis = this@DisappearingMessageExt.lifeTimeMillis
            }
}