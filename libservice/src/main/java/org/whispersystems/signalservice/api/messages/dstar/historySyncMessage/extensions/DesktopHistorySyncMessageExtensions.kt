package org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions

import org.whispersystems.signalservice.api.messages.dstar.DesktopHistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.HistorySyncType
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HistorySyncMessage.Version1.Desktop

// COMPANION
fun DesktopHistorySyncMessage.Companion.parseV1(proto: Desktop): DesktopHistorySyncMessage {
    return with(proto) {
        DesktopHistorySyncMessage(HistorySyncType.mapToObjectType(type), deviceId)
    }
}

// INSTANCE
fun DesktopHistorySyncMessage.builder(): Desktop.Builder = Desktop.newBuilder()
        .apply {
            type = this@builder.type.mapToProtoType()
            deviceId = this@builder.deviceId
        }