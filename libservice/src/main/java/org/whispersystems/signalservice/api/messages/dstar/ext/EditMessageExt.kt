package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.EditMessageExtProto

@Serializable
data class EditMessageExt(val timeStamp: Long, val editedText: String) {

    companion object {
        private const val DEF_TIME_STAMP = 0L
        private const val DEF_EDITED_TEXT = ""

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: EditMessageExtProto): EditMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: EditMessageExtProto.Version1): EditMessageExt {
            with(proto) {
                val timeStamp = if (hasTimeStampCreated()) timeStampCreated else DEF_TIME_STAMP
                val editedText = if (hasEditedText()) editedText else DEF_EDITED_TEXT
                return EditMessageExt(timeStamp, editedText)
            }
        }
    }

    fun toJson() = Json.stringify(serializer(), this)

    internal fun builder() = EditMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = EditMessageExtProto.Version1.newBuilder()
            .apply {
                timeStampCreated = this@EditMessageExt.timeStamp
                editedText = this@EditMessageExt.editedText
            }
}