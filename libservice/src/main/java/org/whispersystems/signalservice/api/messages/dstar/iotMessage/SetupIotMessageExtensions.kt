package org.whispersystems.signalservice.api.messages.dstar.iotMessage

import org.whispersystems.signalservice.api.messages.dstar.SetupIotMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.IotMessageProtos.IotMessageProto.SetupMessage

// COMPANION
internal fun SetupIotMessage.Companion.parse(proto: SetupMessage): SetupIotMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1)
        throw UpdateRequiredException()
    }
}

private fun SetupIotMessage.Companion.parse(proto: SetupMessage.Version1): SetupIotMessage {
    return with(proto) {
        SetupIotMessage(senderId, setupJson)
    }
}

// INSTANCE
fun SetupIotMessage.setupBuilder(): SetupMessage.Builder = SetupMessage.newBuilder()
        .apply {
            setV1(builderV1())
        }

fun SetupIotMessage.builderV1(): SetupMessage.Version1.Builder = SetupMessage.Version1.newBuilder()
        .apply {
            senderId = this@builderV1.senderId
            setupJson = this@builderV1.setupJson
        }
