package org.whispersystems.signalservice.api.messages.dstar.extensions

import org.whispersystems.signalservice.api.messages.dstar.DstarMessageExt
import org.whispersystems.signalservice.api.messages.dstar.EmailMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.IllegalEmailException
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DstarMessageExtProto
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.EmailMessageProto
import java.util.*

// COMPANION
internal val EmailMessage.Companion.DEF_ID: String
    get() = UUID.randomUUID().toString()
internal val EmailMessage.Companion.DEF_SUBJECT: String
    get() = ""
internal val EmailMessage.Companion.DEF_NICKNAME: String
    get() = ""
internal val EmailMessage.Companion.DEF_TO: List<String>
    get() = emptyList()
internal val EmailMessage.Companion.DEF_CC: List<String>
    get() = emptyList()
internal val EmailMessage.Companion.DEF_BCC: List<String>
    get() = emptyList()
internal val EmailMessage.Companion.DEF_IS_RECIPIENT_VERIFIED: Boolean
    get() = false
internal val EmailMessage.Companion.DEF_FROM: String
    get() = ""

internal fun EmailMessage.Companion.parse(proto: EmailMessageProto): EmailMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1, extension)
        throw UpdateRequiredException()
    }
}

private fun EmailMessage.Companion.parse(proto: EmailMessageProto.Version1, extension: DstarMessageExtProto?): EmailMessage {
    return with(proto) {
        val id = if (hasId()) id else DEF_ID
        val timeStampCreated = if (hasTimeStampCreated()) timeStampCreated else 0
        val subject = if (hasSubject()) subject else DEF_SUBJECT
        val target = if (hasTarget()) target else throw IllegalEmailException()
        val nickname = if (hasNickname()) nickname else DEF_NICKNAME
        val isVerified = if (hasIsRecipientVerified()) proto.isRecipientVerified else DEF_IS_RECIPIENT_VERIFIED
        val from = if(hasFrom()) proto.from else DEF_FROM
        val ext = extension?.let { DstarMessageExt.parse(extension) } ?: DstarMessageExt.empty()
        EmailMessage(id, timeStampCreated, subject, target, nickname, toList, ccList, bccList, isVerified, from, ext)
    }
}

// INSTANCE
internal fun EmailMessage.emailBuilder(): EmailMessageProto.Builder {
    return EmailMessageProto.newBuilder()
            .apply {
                setV1(builderV1())
                setExtension(messageExt().buildProto())
            }
}

private fun EmailMessage.builderV1(): EmailMessageProto.Version1.Builder {
    return EmailMessageProto.Version1.newBuilder()
            .apply {
                id = this@builderV1.id
                timeStampCreated = this@builderV1.timeStampCreated
                subject = this@builderV1.subject
                target = this@builderV1.target
                nickname = this@builderV1.nickname
                addAllTo(to)
                addAllCc(cc)
                addAllBcc(bcc)
                from = this@builderV1.from
                isRecipientVerified = this@builderV1.isRecipientVerified
            }
}