package org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions

import org.whispersystems.signalservice.api.messages.dstar.DesktopHistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.GroupHistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.HistorySyncMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.UndefinedDstarMessageException
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HistorySyncMessage as HistorySyncMessageProto
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HistorySyncMessage.Builder

// COMPANION
internal fun HistorySyncMessage.Companion.parse(proto: HistorySyncMessageProto): HistorySyncMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1)
        throw UpdateRequiredException()
    }
}

private fun HistorySyncMessage.Companion.parse(proto: HistorySyncMessageProto.Version1): HistorySyncMessage {
    with(proto) {
        if (hasGroup()) return GroupHistorySyncMessage.parseV1(group)
        if (hasDesktop()) return DesktopHistorySyncMessage.parseV1(desktop)
        throw UndefinedDstarMessageException()
    }
}

// INSTANCE
fun HistorySyncMessage.historySyncBuilder(): Builder = HistorySyncMessageProto.newBuilder()
        .apply {
            setV1(builderV1())
        }

fun HistorySyncMessage.builderV1(): HistorySyncMessageProto.Version1.Builder = HistorySyncMessageProto.Version1.newBuilder()
        .apply {
            when(this@builderV1) {
                is GroupHistorySyncMessage -> setGroup(builder())
                is DesktopHistorySyncMessage -> setDesktop(builder())
            }
        }
