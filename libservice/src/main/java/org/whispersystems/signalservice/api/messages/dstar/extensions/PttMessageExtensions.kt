package org.whispersystems.signalservice.api.messages.dstar.extensions

import org.whispersystems.signalservice.api.messages.dstar.DstarMessageExt
import org.whispersystems.signalservice.api.messages.dstar.PttMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DstarMessageExtProto
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.PttMessageProto

// COMPANION
internal fun PttMessage.Companion.parse(proto: PttMessageProto): PttMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1, extension)
        throw UpdateRequiredException()
    }
}

private fun PttMessage.Companion.parse(proto: PttMessageProto.Version1, extension: DstarMessageExtProto?): PttMessage {
    val ext = extension?.let { DstarMessageExt.parse(extension) } ?: DstarMessageExt.empty()
    return PttMessage(ext)
}

// INSTANCE
fun PttMessage.pttBuilder(): PttMessageProto.Builder {
    return PttMessageProto.newBuilder()
            .apply {
                setV1(builderV1())
                setExtension(messageExt().buildProto())
            }
}

private fun builderV1(): PttMessageProto.Version1.Builder {
    return PttMessageProto.Version1.newBuilder()
}
