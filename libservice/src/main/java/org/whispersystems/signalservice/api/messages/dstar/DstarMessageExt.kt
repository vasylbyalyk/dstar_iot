package org.whispersystems.signalservice.api.messages.dstar

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.api.messages.dstar.ext.*
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DstarMessageExtProto

@Serializable
class DstarMessageExt(val deleteMessageExt: DeleteMessageExt?,
                      val disappearingMessageExt: DisappearingMessageExt?,
                      val editMessageExt: EditMessageExt?,
                      internal val forwardMessageExt: ForwardMessageExt?,
                      internal val hiddenMessageExt: HiddenMessageExt?,
                      internal val flickeringMessageExt: FlickeringMessageExt?
) {

    companion object {

        fun empty(): DstarMessageExt {
            return DstarMessageExt(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)
        }

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: DstarMessageExtProto): DstarMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                return empty()
//                throw UpdateRequiredException() WTF
            }
        }

        private fun parse(proto: DstarMessageExtProto.Version1): DstarMessageExt {
            with(proto) {
                val deleteMessage = if (hasDelete()) DeleteMessageExt.parse(delete) else null
                val disappearingMessage = if (hasDisappearing()) DisappearingMessageExt.parse(disappearing) else null
                val editMessage = if (hasEdit()) EditMessageExt.parse(edit) else null
                val forwardMessage = if (hasForward()) ForwardMessageExt.parse(forward) else null
                val hiddenMessage = if (hasHidden()) HiddenMessageExt.parse(hidden) else null
                val flickeringMessage = if (hasFlickering()) FlickeringMessageExt.parse(flickering) else null

                return DstarMessageExt(
                        deleteMessage,
                        disappearingMessage,
                        editMessage,
                        forwardMessage,
                        hiddenMessage,
                        flickeringMessage)
            }
        }
    }

    val isEmpty: Boolean
        get() = (deleteMessageExt == null) and (disappearingMessageExt == null) and
                (editMessageExt == null) and (forwardMessageExt == null) and
                (hiddenMessageExt == null) and (flickeringMessageExt == null)

    val isDisappearing: Boolean = disappearingMessageExt != null

    fun toJson() = Json.stringify(serializer(), this)

    internal fun buildProto(): DstarMessageExtProto.Builder {
        return DstarMessageExtProto.newBuilder()
                .setV1(buildProtoV1())
    }

    private fun buildProtoV1(): DstarMessageExtProto.Version1.Builder {
        val builder = DstarMessageExtProto.Version1.newBuilder()
        deleteMessageExt?.let { builder.setDelete(it.builder()) }
        disappearingMessageExt?.let { builder.setDisappearing(it.builder()) }
        editMessageExt?.let { builder.setEdit(it.builder()) }
        forwardMessageExt?.let { builder.setForward(it.builder()) }
        hiddenMessageExt?.let { builder.setHidden(it.builder()) }
        flickeringMessageExt?.let { builder.setFlickering(it.builder()) }
        return builder
    }

    class Builder {

        private var deleteMessageExt: DeleteMessageExt? = null
        private var disappearingMessageExt: DisappearingMessageExt? = null
        private var editMessageExt: EditMessageExt? = null
        private var forwardMessageExt: ForwardMessageExt? = null
        private var hiddenMessageExt: HiddenMessageExt? = null
        private var flickeringMessageExt: FlickeringMessageExt? = null

        fun withDelete(timeStampSent: Long, messageType: String): Builder {
            deleteMessageExt = DeleteMessageExtV1(timeStampSent, messageType)
            return this
        }

        fun withDelete(timeStampSentList: List<Long>): Builder {
            deleteMessageExt = DeleteMessageExtV2(timeStampSentList)
            return this
        }

        fun asDisappearing(lifeTimeMillis: Long): Builder {
            disappearingMessageExt = DisappearingMessageExt(lifeTimeMillis)
            return this
        }

        fun withEdit(timeStamp: Long, editedText: String): Builder {
            editMessageExt = EditMessageExt(timeStamp, editedText)
            return this
        }

        fun asForward(currentIndex: Int, forwardMessageCount: Int): Builder {
            forwardMessageExt = ForwardMessageExt(currentIndex, forwardMessageCount)
            return this
        }

        fun asHidden(): Builder {
            hiddenMessageExt = HiddenMessageExt()
            return this
        }

        fun asFlickering(): Builder {
            flickeringMessageExt = FlickeringMessageExt()
            return this
        }

        fun build(): DstarMessageExt {
            return DstarMessageExt(deleteMessageExt, disappearingMessageExt, editMessageExt, forwardMessageExt, hiddenMessageExt, flickeringMessageExt)
        }
    }
}
