package org.whispersystems.signalservice.api.messages.dstar.backupMessage

import com.google.protobuf.ByteString
import org.whispersystems.signalservice.api.messages.dstar.*
import org.whispersystems.signalservice.api.messages.dstar.exception.BackupException
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.BackupMessageProtos
import org.whispersystems.signalservice.internal.push.BackupMessageProtos.BackupMessageProto

// COMPANION
internal fun BackupMessage.Companion.parse(proto: BackupMessageProto): BackupMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1)
        return BackupMessage()
//        throw UpdateRequiredException()
    }
}

private fun BackupMessage.Companion.parse(proto: BackupMessageProto.Version1): BackupMessage {
    if (proto.hasGet()) return parseGet(proto.get)
    if (proto.hasSave()) return parseSaveRequest(proto.save)
    if (proto.hasDelete()) return parseDeleteRequest(proto.delete)
    if (proto.hasResponse()) return parseResponse(proto.response)
    throw UpdateRequiredException()
}

//--------- GET --------------
fun parseGet(getProto: BackupMessageProtos.GetRequest): BackupMessage {
    with(getProto) {
        if (getProto.hasV1()) return parseGetV1(v1)
        throw UpdateRequiredException()
    }
}

fun parseGetV1(getV1Proto: BackupMessageProtos.GetRequest.Version1): BackupMessage {
    return BackupMessage(getRequest = GetRequest())
}

//--------- SAVE --------------
fun parseSaveRequest(saveRequestProto: BackupMessageProtos.SaveRequest): BackupMessage {
    with(saveRequestProto) {
        if (saveRequestProto.hasV1()) return parseSaveRequestV1(v1)
        throw UpdateRequiredException()
    }
}

fun parseSaveRequestV1(saveRequestV1Proto: BackupMessageProtos.SaveRequest.Version1): BackupMessage {
    return with(saveRequestV1Proto) {
        val createDate = if (hasCreateDate()) createDate else 0
        val passphrase = if (hasPassphrase()) passphrase else "" //throw BackupException("Password is empty")
        BackupMessage(saveRequest = SaveRequest(createDate, passphrase))
    }
}

//--------- DELETE --------------
fun parseDeleteRequest(deleteRequestProto: BackupMessageProtos.DeleteRequest): BackupMessage {
    with(deleteRequestProto) {
        if (deleteRequestProto.hasV1()) return parseDeleteRequestV1(v1)
        throw UpdateRequiredException()
    }
}

fun parseDeleteRequestV1(deleteRequestV1: BackupMessageProtos.DeleteRequest.Version1): BackupMessage {
    return BackupMessage(deleteRequest = DeleteRequest())
}

//--------- RESPONSE --------------
fun parseResponse(parseResponseProto: BackupMessageProtos.Response): BackupMessage {
    with(parseResponseProto) {
        if (parseResponseProto.hasV1()) return parseResponseV1(v1)
        throw UpdateRequiredException()
    }
}

fun parseResponseV1(parseResponseV1Proto: BackupMessageProtos.Response.Version1): BackupMessage {
    with(parseResponseV1Proto) {
        if (hasRestore() && restore.hasV1()) return parseRestoreMetaDataProtoV1(restore.v1)
        if (hasType()) return parseRestoreMetaDataProtoTypeV1(parseResponseV1Proto.type)
        throw UpdateRequiredException()
    }
}

fun parseRestoreMetaDataProtoTypeV1(responseCode: BackupMessageProtos.Response.Version1.Type): BackupMessage {
    return BackupMessage(response = Response().apply {
        code = BackupResponseCode.toEnum(responseCode.number)
    })
}

fun parseRestoreMetaDataProtoV1(restoreMetaDataProtov1: BackupMessageProtos.Response.RestoreMetaData.Version1): BackupMessage {
    return with(restoreMetaDataProtov1) {
        BackupMessage(response =
        Response().apply {
            val name = name
            val fileSize = fileSize
            val fileId = fileId
            val createDate = createDate
            val digest = digest.toByteArray()
            val encryptKey = encryptKey.toByteArray()
            val passphrase = passphrase

            metaData = RestoreMetaData(name, fileSize, fileId, createDate, digest, encryptKey, passphrase)
        })
    }
}

//--------- INSTANCE --------------
fun BackupMessage.backupBuilder(): BackupMessageProto.Builder {
    return BackupMessageProto.newBuilder()
            .apply {
                setV1(builderV1())
            }
}

private fun BackupMessage.builderV1(): BackupMessageProto.Version1.Builder {
    return BackupMessageProto.Version1.newBuilder()
            .apply {
                this@builderV1.getRequest?.let { setGet(builderRequestV1()) }
                this@builderV1.saveRequest?.let { setSave(builderSaveV1()) }
                this@builderV1.deleteRequest?.let { setDelete(builderDeleteV1()) }
                this@builderV1.response?.let { setResponse(builderResponseV1()) }
            }
}

private fun BackupMessage.builderRequestV1(): BackupMessageProtos.GetRequest.Builder {
    return BackupMessageProtos.GetRequest.newBuilder()
            .setV1(BackupMessageProtos.GetRequest.Version1.newBuilder())
}

private fun BackupMessage.builderSaveV1(): BackupMessageProtos.SaveRequest.Builder {
    val save = BackupMessageProtos.SaveRequest.Version1.newBuilder()
            .apply {
                this@builderSaveV1.saveRequest?.let {
                    createDate = it.createDate
                    passphrase = it.passphrase
                }
            }
    return BackupMessageProtos.SaveRequest.newBuilder().setV1(save)
}

private fun BackupMessage.builderDeleteV1(): BackupMessageProtos.DeleteRequest.Builder {
    return BackupMessageProtos.DeleteRequest.newBuilder()
            .setV1(BackupMessageProtos.DeleteRequest.Version1.newBuilder())
}

private fun BackupMessage.builderResponseV1(): BackupMessageProtos.Response.Builder {
    val response = BackupMessageProtos.Response.Version1.newBuilder()
            .apply {
                setType(BackupMessageProtos.Response.Version1.Type.forNumber(this@builderResponseV1.response?.code?.type
                        ?: BackupResponseCode.NONE.type))

                this@builderResponseV1.response?.metaData?.let {
                    setRestore(builderResponseMetadataV1(it))
                }
            }
    return BackupMessageProtos.Response.newBuilder().setV1(response)
}

private fun BackupMessage.builderResponseMetadataV1(metaData: RestoreMetaData): BackupMessageProtos.Response.RestoreMetaData.Builder {
    val metaDataBuilder = BackupMessageProtos.Response.RestoreMetaData.newBuilder()
            .setV1(
                    BackupMessageProtos.Response.RestoreMetaData.Version1.newBuilder()
                            .apply {
                                with(metaData) {
                                    setCreateDate(createDate)
                                    setFileId(fileId)
                                    setFileSize(fileSize)
                                    setDigest(ByteString.copyFrom(digest))
                                    setEncryptKey(ByteString.copyFrom(encryptKey))
                                }
                            })
    return metaDataBuilder
}


