package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.FlickeringMessageExtProto

@Serializable
class FlickeringMessageExt {

    companion object {

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: FlickeringMessageExtProto): FlickeringMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: FlickeringMessageExtProto.Version1) = FlickeringMessageExt()
    }

    fun toJson() = Json.stringify(serializer(), this)

    internal fun builder() = FlickeringMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = FlickeringMessageExtProto.Version1.newBuilder()
}