package org.whispersystems.signalservice.api.messages.dstar.historySyncMessage

import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.HistorySyncMessage.Version1.Type

enum class HistorySyncType {
    REQUEST, RESPONSE;

    fun mapToProtoType() = when (this) {
        REQUEST -> Type.REQUEST
        RESPONSE -> Type.RESPONSE
    }

    companion object {
        fun mapToObjectType(type: Type) = when (type) {
            Type.REQUEST -> REQUEST
            Type.RESPONSE -> RESPONSE
        }
    }
}