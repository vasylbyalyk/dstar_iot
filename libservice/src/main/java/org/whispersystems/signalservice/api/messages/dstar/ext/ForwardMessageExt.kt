package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.ForwardMessageExtProto

@Serializable
data class ForwardMessageExt(val currentIndex: Int, val forwardMessageCount: Int) {

    companion object {
        private const val DEF_CURRENT_INDEX = 0
        private const val DEF_FORWARD_MESSAGE_COUNT = 0

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: ForwardMessageExtProto): ForwardMessageExt {
            with(proto) {
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: ForwardMessageExtProto.Version1): ForwardMessageExt {
            with(proto) {
                val currentIndex = if (hasCurrentIndex()) currentIndex else DEF_CURRENT_INDEX
                val forwardMessageCount = if (hasForwardMessageCount()) forwardMessageCount else DEF_FORWARD_MESSAGE_COUNT
                return ForwardMessageExt(currentIndex, forwardMessageCount)
            }
        }
    }

    fun toJson() = Json.stringify(serializer(), this)

    internal fun builder() = ForwardMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = ForwardMessageExtProto.Version1.newBuilder()
            .apply {
                currentIndex = this@ForwardMessageExt.currentIndex
                forwardMessageCount = this@ForwardMessageExt.forwardMessageCount
            }
}