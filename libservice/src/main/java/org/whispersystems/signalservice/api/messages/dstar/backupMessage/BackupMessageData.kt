package org.whispersystems.signalservice.api.messages.dstar.backupMessage

import kotlinx.serialization.Serializable


@Serializable
class GetRequest() {
}

@Serializable
data class SaveRequest(val createDate: Long, val passphrase: String) {
}

@Serializable
class DeleteRequest() {
}

@Serializable
class Response() {
    var code: BackupResponseCode = BackupResponseCode.NONE;
    var metaData: RestoreMetaData? = null
}

@Serializable
data class RestoreMetaData(
        val name: String,
        val fileSize: Int,
        val fileId: Long,
        val createDate: Long,
        val digest: ByteArray,
        val encryptKey: ByteArray,
        val passphrase: String) {
}

enum class BackupResponseCode(val type: Int) {
    NONE(-1),
    BR_NOT_FOUND(0),
    BR_SAVE_OK(1),
    BR_SAVE_ERR(2);

    companion object {
        @JvmStatic
        fun toEnum(int: Int): BackupResponseCode {
            return values()[int]
        }
    }
}