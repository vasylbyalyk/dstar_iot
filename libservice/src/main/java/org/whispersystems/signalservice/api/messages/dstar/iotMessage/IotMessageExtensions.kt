package org.whispersystems.signalservice.api.messages.dstar.iotMessage

import org.whispersystems.signalservice.api.messages.dstar.*
import org.whispersystems.signalservice.api.messages.dstar.exception.UndefinedDstarMessageException
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.api.messages.dstar.historySyncMessage.extensions.parse
import org.whispersystems.signalservice.internal.push.DstarMessageProtos
import org.whispersystems.signalservice.internal.push.IotMessageProtos

// COMPANION
internal fun IotMessage.Companion.parse(proto: IotMessageProtos.IotMessageProto): IotMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1)
        throw UpdateRequiredException()
    }
}

private fun IotMessage.Companion.parse(proto: IotMessageProtos.IotMessageProto.Version1): IotMessage {
    with(proto) {
        if (hasSetup()) return SetupIotMessage.parse(setup)
        if (hasDynamic()) return DynamicIotMessage.parse(dynamic)
        throw UndefinedDstarMessageException()
    }
}

// INSTANCE
fun IotMessage.iotBuilder(): IotMessageProtos.IotMessageProto.Builder = IotMessageProtos.IotMessageProto.newBuilder()
        .apply {
            setV1(builderV1())
        }

fun IotMessage.builderV1(): IotMessageProtos.IotMessageProto.Version1.Builder = IotMessageProtos.IotMessageProto.Version1.newBuilder()
        .apply {
            when(this@builderV1) {
                is SetupIotMessage -> setSetup(setupBuilder())
                is DynamicIotMessage -> setDynamic(dynamicBuilder())
            }
        }
