/**
 * Copyright (C) 2014-2016 Open Whisper Systems
 *
 * Licensed according to the LICENSE file in this repository.
 */

package org.whispersystems.signalservice.api.push;

import org.whispersystems.libsignal.util.guava.Optional;

import java.util.UUID;

/**
 * A class representing a message destination or origin.
 */
public class ComposeSignalServiceAddress extends SignalServiceAddress{

  protected static final String COMPOSE_ADDRESS_DELIMITER = ":";

  private final String sender;

  public ComposeSignalServiceAddress(String sender, String destination) {
    super(null, destination);
    this.sender  = sender;
  }

  @Override
  public Optional<String> getNumber() {
    return Optional.of(sender + COMPOSE_ADDRESS_DELIMITER + super.getNumber().get());
  }

  @Override
  public String getIdentifier() {
    return getNumber().get();
  }

  @Override
  public String getLegacyIdentifier() {
    return getNumber().get();
  }

  public String getRecipientAddress(){
    return super.getNumber().get();
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || !(other instanceof ComposeSignalServiceAddress)) return false;

    ComposeSignalServiceAddress that = (ComposeSignalServiceAddress)other;

    return equals(this.sender, that.sender) &&
            super.equals(other);
  }

  @Override
  public int hashCode() {
    int hashCode = super.hashCode();

    if (this.sender != null) hashCode ^= this.sender.hashCode();

    return hashCode;
  }

  private boolean equals(String one, String two) {
    if (one == null) return two == null;
    return one.equals(two);
  }

  private boolean equals(Optional<String> one, Optional<String> two) {
    if (one.isPresent()) return two.isPresent() && one.get().equals(two.get());
    else                 return !two.isPresent();
  }
}
