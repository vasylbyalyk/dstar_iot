package org.whispersystems.signalservice.api.messages.dstar.iotMessage

import org.whispersystems.signalservice.api.messages.dstar.DynamicIotMessage
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.IotMessageProtos.IotMessageProto.DynamicMessage

// COMPANION
internal fun DynamicIotMessage.Companion.parse(proto: DynamicMessage): DynamicIotMessage {
    with(proto) {
        if (proto.hasV1()) return parse(v1)
        throw UpdateRequiredException()
    }
}

internal fun DynamicIotMessage.Companion.parse(proto: DynamicMessage.Version1): DynamicIotMessage {
    return with(proto) {
        DynamicIotMessage(dynamicJson)
    }
}

// INSTANCE
internal fun DynamicIotMessage.dynamicBuilder(): DynamicMessage.Builder = DynamicMessage.newBuilder()
        .apply {
            setV1(builderV1())
        }

private fun DynamicIotMessage.builderV1(): DynamicMessage.Version1.Builder = DynamicMessage.Version1.newBuilder()
        .apply {
            dynamicJson = this@builderV1.dynamicJson
        }
