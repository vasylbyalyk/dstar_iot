package org.whispersystems.signalservice.api.messages.dstar.ext

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.whispersystems.signalservice.api.messages.dstar.exception.UpdateRequiredException
import org.whispersystems.signalservice.internal.push.DstarMessageProtos.DstarMessageProto.DeleteMessageExtProto

@Serializable
sealed class DeleteMessageExt {
    internal abstract fun builder(): DeleteMessageExtProto.Builder

    open fun toJson() = Json.stringify(serializer(), this)

    companion object {
        private const val DEF_TIME_STAMP_SENT = 0L
        private const val DEF_MESSAGE_TYPE = ""

        fun fromJson(json: String) = Json.parse(serializer(), json)

        @Throws(UpdateRequiredException::class)
        internal fun parse(proto: DeleteMessageExtProto): DeleteMessageExt {
            with(proto) {
                if (hasV2()) return parse(v2)
                if (hasV1()) return parse(v1)
                throw UpdateRequiredException()
            }
        }

        private fun parse(proto: DeleteMessageExtProto.Version1): DeleteMessageExt {
            with(proto) {
                val timeStampSent = if (hasTimeStampSent()) timeStampSent else DEF_TIME_STAMP_SENT
                val messageType = if (hasMessageType()) messageType else DEF_MESSAGE_TYPE
                return DeleteMessageExtV1(timeStampSent, messageType)
            }
        }

        private fun parse(proto: DeleteMessageExtProto.Version2): DeleteMessageExt {
            with(proto) {
                return DeleteMessageExtV2(timeStampSentListList)
            }
        }
    }
}

@Serializable
data class DeleteMessageExtV2(val timeStampSentList: List<Long>) : DeleteMessageExt() {
    override fun toJson() = Json.stringify(serializer(), this)

    override fun builder() = DeleteMessageExtProto.newBuilder()
            .apply {
                setV2(builderV2())
            }

    private fun builderV2() = DeleteMessageExtProto.Version2.newBuilder()
            .apply {
                this.addAllTimeStampSentList(this@DeleteMessageExtV2.timeStampSentList)
            }
}

@Serializable
data class DeleteMessageExtV1(val timeStampSent: Long, val messageType: String) : DeleteMessageExt() {
    override fun toJson() = Json.stringify(serializer(), this)

    override fun builder() = DeleteMessageExtProto.newBuilder()
            .apply {
                setV1(builderV1())
            }

    private fun builderV1() = DeleteMessageExtProto.Version1.newBuilder()
            .apply {
                timeStampSent = this@DeleteMessageExtV1.timeStampSent
                messageType = this@DeleteMessageExtV1.messageType
            }
}
