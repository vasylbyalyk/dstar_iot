package org.whispersystems.signalservice.api.messages.dstar.exception

class BackupException (var excMessage : String) : Exception(excMessage)
