param=$1

DATA_FOLDER="/mnt/DATA"
PROJECT_FOLDER=/home/pi/dstar-iot
#if [[ $USER == "pi" ]]; then
#    sudo /usr/sbin/prelink /usr/lib/gstreamer-1.0/libgstvideo4linux2.so
#else
#    prelink /usr/lib/gstreamer-1.0/libgstvideo4linux2.so
#fi

if [[ "$param" == "-run" ]]; then
  pkill -f java\ -jar\ /home/pi/*dstar*
  killall -9 gst-launch-1.0 gst-device-monitor-1.0
  mkdir -p "$DATA_FOLDER"/usb_cameras

  array=($(ls "$PROJECT_FOLDER"))
  should_i_do=false

  for each in "${array[@]}"; do
    if [[ $each == *"new-"* ]]; then
      new_version=$each
      should_i_do=true
    fi
    if [[ "$each" == *"dstar_iot"* ]] && [[ $each != *"new-"* ]]; then
      current_version=$each
    fi
  done

  echo "$current_version"
  echo "$new_version"

  if [[ "$should_i_do" == true ]]; then
    new_truncated_version="${new_version/new-/}"
    rm "$PROJECT_FOLDER"/"$current_version"
    echo "$new_truncated_version"
    mv "$PROJECT_FOLDER"/"$new_version" "$PROJECT_FOLDER"/"$new_truncated_version"
    java -jar "$PROJECT_FOLDER"/"$new_truncated_version" >"$PROJECT_FOLDER"/bot.log
  else
    java -jar "$PROJECT_FOLDER"/"$current_version" >"$PROJECT_FOLDER"/bot.log
  fi
fi

# TODO backup logic
#  array=($(ls "$PROJECT_FOLDER"))
#  should_i_do=false
#
#  for each in "${array[@]}"; do
#    if [[ $each == *"new-"* ]]; then
#      new_version=$each
#      should_i_do=true
#    fi
#    if [[ $each == *"old-"* ]]; then
#      old_version=$each
#    fi
#    if [[ "$each" == *"dstar_iot"* ]] && [[ $each != *"old-"* ]] && [[ $each != *"new-"* ]]; then
#      current_version=$each
#    fi
#  done
#
#  echo "$old_version"
#  echo "$current_version"
#  echo "$new_version"
#
#  if [[ "$should_i_do" == true ]]; then
#    new_truncated_version="${new_version/new-/}"
#    cur_new_truncated_version="${current_version/dstar/old-dstar}"
#    mv "$new_version" "$new_truncated_version"
#    mv "$current_version" "$cur_new_truncated_version"
#    if [ -z "$old_version" ]; then
#      echo "$old_version is empty"
#    else
#      rm "$old_version"
#    fi
#    nohup java -jar "$PROJECT_FOLDER"/"$new_truncated_version" >"$PROJECT_FOLDER"/bot.log &
#  else
#    nohup java -jar "$PROJECT_FOLDER"/"$current_version" >"$PROJECT_FOLDER"/bot.log &
#  fi

if [[ "$param" == "-stop" ]]; then
  pkill -f java\ -jar\ /home/pi/*dstar*
  killall -9 gst-launch-1.0 gst-device-monitor-1.0
fi

if [[ "$param" == "-status" ]]; then
  tail -f "$PROJECT_FOLDER"/bot.log
fi
