package com.dstarlab.iot.signal.commands;

import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

import com.dstarlab.iot.signal.manager.Manager;

public class UploadStickerPackCommand implements LocalCommand {

    @Override
    public void attachToSubparser(final Subparser subparser) {
        subparser.addArgument("path")
                .help("The path of the manifest.json or a zip file containing the sticker pack you wish to upload.");
    }

    @Override
    public int handleCommand(final Namespace ns, final Manager m) {
        String path = ns.getString("path");
//            String url = m.uploadStickerPack(path);
        return 0;
    }
}
