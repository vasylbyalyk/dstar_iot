package com.dstarlab.iot.signal.commands;

import com.dstarlab.iot.Signal;

import net.sourceforge.argparse4j.inf.Namespace;

public interface DbusCommand extends Command {

    int handleCommand(Namespace ns, Signal signal);
}
