package com.dstarlab.iot.signal.commands;

import com.dstarlab.iot.signal.manager.ProvisioningManager;

import net.sourceforge.argparse4j.inf.Namespace;

public interface ProvisioningCommand extends Command {

    int handleCommand(Namespace ns, ProvisioningManager m);
}
