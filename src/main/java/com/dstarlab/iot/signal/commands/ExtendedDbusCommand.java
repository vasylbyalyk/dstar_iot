package com.dstarlab.iot.signal.commands;

import net.sourceforge.argparse4j.inf.Namespace;

import com.dstarlab.iot.Signal;
import org.freedesktop.dbus.connections.impl.DBusConnection;

public interface ExtendedDbusCommand extends Command {

    int handleCommand(Namespace ns, Signal signal, DBusConnection dbusconnection);
}
