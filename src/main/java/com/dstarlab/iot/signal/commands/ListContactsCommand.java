package com.dstarlab.iot.signal.commands;

import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

import com.dstarlab.iot.signal.manager.Manager;
import com.dstarlab.iot.signal.storage.contacts.ContactInfo;

import java.util.List;

public class ListContactsCommand implements LocalCommand {

    @Override
    public void attachToSubparser(final Subparser subparser) {
    }

    @Override
    public int handleCommand(final Namespace ns, final Manager m) {
        if (!m.isRegistered()) {
            System.err.println("User is not registered.");
            return 1;
        }
        List<ContactInfo> contacts = m.getContacts();
        for (ContactInfo c : contacts) {
            System.out.println(String.format("Number: %s Name: %s  Blocked: %b", c.number, c.name, c.blocked));
        }
        return 0;
    }
}
