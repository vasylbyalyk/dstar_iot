package com.dstarlab.iot.signal.commands;

import com.dstarlab.iot.signal.manager.Manager;

import net.sourceforge.argparse4j.inf.Namespace;

public interface LocalCommand extends Command {

    int handleCommand(Namespace ns, Manager m);
}
