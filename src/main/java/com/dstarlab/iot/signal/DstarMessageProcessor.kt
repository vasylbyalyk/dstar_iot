package com.dstarlab.iot.signal

import com.dstarlab.bot.nucleus.IDstarMessageProcessor
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage
import org.whispersystems.signalservice.api.messages.dstar.DstarMessage
import org.whispersystems.signalservice.api.messages.dstar.DynamicIotMessage
import org.whispersystems.signalservice.api.messages.dstar.extensions.isDynamicIotMessage

class DstarMessageProcessor: IDstarMessageProcessor {

    /**
     * @return true -> if message not required further processing
     */
    override fun handleDstarMessageWithoutFurtherProcessing(message: SignalServiceDataMessage,
                                                   dstarMessage: DstarMessage
                                                   ): String {

        if (message.body.isPresent) println(message.body.get())
        with(dstarMessage) {
            return when {
                isDynamicIotMessage() -> handleDynamicIotMessage(this as DynamicIotMessage)
                else -> "false"
            }

        }
    }

    private fun handleDynamicIotMessage(dynamicIotMessage: DynamicIotMessage) : String {
        println("handleDynamicIotMessage")
        println(dynamicIotMessage.dynamicJson)
//        MessageController().processingBotCommand(dynamicIotMessage.dynamicJson)
        return dynamicIotMessage.dynamicJson
    }
}