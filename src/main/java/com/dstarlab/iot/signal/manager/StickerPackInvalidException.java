package com.dstarlab.iot.signal.manager;

public class StickerPackInvalidException extends Exception {

    public StickerPackInvalidException(String message) {
        super(message);
    }
}
