package com.dstarlab.iot.signal.json;

public class JsonError {

    String message;

    public JsonError(Throwable exception) {
        this.message = exception.getMessage();
    }
}
