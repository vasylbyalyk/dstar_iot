/*
  Copyright (C) 2015-2020 AsamK and contributors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dstarlab.iot.signal;

import com.dstarlab.bot.camera.scanner.DeviceMonitorManager;
import com.dstarlab.bot.camera.scanner.DeviceMonitorScanner;
import com.dstarlab.bot.controller.system.TimezoneController;
import com.dstarlab.bot.properties.application.ApplicationSettings;
import com.dstarlab.bot.properties.config.ConfigSettings;
import com.dstarlab.bot.update.Updater;
import com.dstarlab.iot.signal.manager.Manager;
import com.dstarlab.iot.signal.manager.ServiceConfig;
import com.dstarlab.iot.signal.util.IOUtils;
import com.dstarlab.iot.signal.util.SecurityProvider;
import com.j256.ormlite.logger.LocalLog;
import com.j256.ormlite.logger.LoggerFactory;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.whispersystems.signalservice.api.push.exceptions.EncapsulatedExceptions;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.Security;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static com.dstarlab.iot.signal.util.ErrorUtils.handleAssertionError;

public class Main {

    public static void main(String[] args) {

        System.setProperty(LoggerFactory.LOG_TYPE_SYSTEM_PROPERTY, LoggerFactory.LogType.LOCAL.name());
        System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "INFO");

        installSecurityProviderWorkaround();

        TimezoneController timezoneController = new TimezoneController();
        String currentTimezone = timezoneController.getCurrentTimezone();
        if (!currentTimezone.equals("")) {
            timezoneController.setJavaTimezone(currentTimezone);
        }

        runDeviceMonitorScanner();

        Manager manager = initialization();

        manager.sendJsonSetupStartIot();

        Updater updater = new Updater((message) -> {
            try {
                manager.sendMessage(message, null, ConfigSettings.INSTANCE.getOwner());
            } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
                encapsulatedExceptions.printStackTrace();
            }
        });
        updater.startUpdateMonitoring();

        try {
            final Manager.ReceiveMessageHandler handler = new ReceiveMessageHandler(manager);
            manager.receiveMessages(1, TimeUnit.MILLISECONDS, false, true, handler);
        } catch (IOException e) {
            System.err.println("Error while receiving messages: " + e.getMessage());
        } catch (AssertionError e) {
            handleAssertionError(e);
        }
    }

    private static Manager initialization() {
        System.out.println("manager initialization");
        String dataPath = getDefaultDataPath();

        final String username = ConfigSettings.INSTANCE.getNumber();
        final SignalServiceConfiguration serviceConfiguration = ServiceConfig.createDefaultServiceConfiguration(BaseConfig.USER_AGENT);
        Manager manager = null;
        try {
            System.out.println(BaseConfig.USER_AGENT);

            manager = Manager.init(username, dataPath, serviceConfiguration, BaseConfig.USER_AGENT);
            System.out.println("manager Username: " + manager.getUsername());
            if (manager.isRegistered()) {

                System.out.println("User is register");
            } else {
                try {
                    manager.register(false);
                } catch (IOException ex) {
                    System.out.println("---manager.register IOException");
                    ex.printStackTrace();
                    System.exit(1);
                }
                try {
                    manager.verifyAccount(ConfigSettings.INSTANCE.getVerify(), null);
                } catch (IOException ex) {
                    System.out.println("---manager.verifyAccount IOException");
                    ex.printStackTrace();
                }

            }
            try {
                System.out.println("----- " + ConfigSettings.INSTANCE.getOwner());
                manager.sendMessage(ApplicationSettings.INSTANCE.getHiText(), null, ConfigSettings.INSTANCE.getOwner());
            } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
                System.out.println("failed to send message");
                System.out.println(encapsulatedExceptions.getMessage());
                System.out.println(Arrays.toString(encapsulatedExceptions.getStackTrace()));
                encapsulatedExceptions.printStackTrace();
            }

        } catch (Throwable e) {
            System.err.println("Error loading state file: " + e.getMessage());
            e.printStackTrace();
        }
        return manager;
    }

    public static void installSecurityProviderWorkaround() {
        // Register our own security provider
        Security.insertProviderAt(new SecurityProvider(), 3);
        Security.addProvider(new BouncyCastleProvider());
    }

    private static void runDeviceMonitorScanner() {
        DeviceMonitorScanner deviceMonitorScanner = new DeviceMonitorScanner(new DeviceMonitorManager());
        deviceMonitorScanner.scan();
    }

    private static void setTimezone() {
        // TODO replace by secure and reliable resource
        final TimezoneController timezoneController = new TimezoneController();
        try {
            InputStream stream = new URL("https://ipapi.co/timezone").openStream();
            Scanner timezone = new Scanner(stream, "UTF-8").useDelimiter("\\A");
            timezoneController.setTimezone(timezone.next());
            stream.close();
            timezone.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses $XDG_DATA_HOME/signal-cli if it exists, or if none of the legacy directories exist:
     * - $HOME/.config/signal
     * - $HOME/.config/textsecure
     *
     * @return the data directory to be used by signal-cli.
     */
    private static String getDefaultDataPath() {
        String dataPath = IOUtils.getDataHomeDir() + "/signal-cli";
        if (new File(dataPath).exists()) {
            return dataPath;
        }

        String legacySettingsPath = IOUtils.getDataHomeDir() + "/.config/signal";
        if (new File(legacySettingsPath).exists()) {
            return legacySettingsPath;
        }

        legacySettingsPath = IOUtils.getDataHomeDir() + "/.config/textsecure";
        if (new File(legacySettingsPath).exists()) {
            return legacySettingsPath;
        }

        return dataPath;
    }

}
