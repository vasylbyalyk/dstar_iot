[ ! -d "bot" ] || [ ! -d "libservice" ] && echo "Directory bot or/and libservice DOES NOT exists. Please cd to source project folder!" && exit 1

partition=$1
mount_point="/mnt/root"
dstar_folder="$mount_point/home/pi/dstar-iot/"
if [[ $partition == "" ]]; then
  echo "Pass partition from lsblk. Example: bash deploy_partition.sh /dev/sdb"
  exit 0
fi

./gradlew clean --parallel
./gradlew build --parallel
./gradlew fatJar --parallel

#sudo umount "$partition"2
#sudo e2fsck -f -y -v /dev/sdb2
#sudo sfdisk --force /dev/sdb -N 2
#sudo growpart $partition 2
#sudo resize2fs /dev/sdb2
#sudo e2fsck -f -y -v /dev/sdb2

sudo mount "$partition"2 "$mount_point"
sudo mkdir -p "$dstar_folder"
sudo rm -rf "$dstar_folder"/*
sudo mkdir -p "$mount_point/mnt/DATA/usb_cameras"
sudo rsync -zaP build/libs/dstar_iot-all-*.jar resources "$dstar_folder"
sudo rsync -zaP iot.sh "$(dirname "$dstar_folder")"
sudo umount "$partition"2
