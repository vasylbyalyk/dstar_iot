[ ! -d "bot" ] || [ ! -d "libservice" ] && echo "Directory bot or/and libservice DOES NOT exists. Please cd to source project folder!" && exit 1

ip=$1
if [[ $ip == "" ]]; then
  echo "Pass name:ip_address. Example: bash deploy.sh pi@192.168.3.83"
  exit 0
fi

cat /dev/zero | ssh-keygen -q -N ""
ssh-copy-id "$ip"

TARGET_NAME=$(echo "$ip" | awk -F "@" '{print $1}')
TARGET_IP=$(echo "$ip" | awk -F "@" '{print $2}')

ssh pi@"$TARGET_IP" "echo "robocode" | sudo -S rm -rf /home/$TARGET_NAME/* ;
                     echo "robocode" | sudo -S killall -9 gst-launch-1.0 gst-device-monitor-1.0;
                     echo "robocode" | sudo -S pkill -f java\ -jar\ /home/pi/*dstar*;
                     echo "robocode" | sudo -S chmod 777 /dev/vchiq"

ssh pi@"$TARGET_IP" "echo "robocode" | sudo -S chmod 777 /dev/video*"

PROJECT_FOLDER=/home/"$TARGET_NAME"/dstar-iot

./gradlew clean --parallel
./gradlew build --parallel
./gradlew fatJar --parallel

rsync -zaP build/libs/dstar_iot-all-*.jar resources "$TARGET_NAME"@"$TARGET_IP":"$PROJECT_FOLDER"
rsync -zaP iot.sh "$TARGET_NAME"@"$TARGET_IP":/home/"$TARGET_NAME"/
ssh pi@"$TARGET_IP" "chmod +x /home/$TARGET_NAME/iot.sh"

#ssh root@"$TARGET_IP" "sudo /sbin/reboot"

#ssh "$TARGET_NAME"@"$TARGET_IP" "/home/$TARGET_NAME/iot.sh -run"
#ssh "$TARGET_NAME"@"$TARGET_IP" "/home/$TARGET_NAME/iot.sh -status"
