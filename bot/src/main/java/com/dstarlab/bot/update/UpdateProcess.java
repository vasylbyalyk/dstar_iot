package com.dstarlab.bot.update;

import com.dstarlab.bot.Main;
import com.dstarlab.bot.controller.bash.BashConsoleController;
import com.fasterxml.jackson.core.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class UpdateProcess implements Runnable {

    final static Logger logger = LoggerFactory.getLogger(UpdateProcess.class);
    private static final Pattern p = Pattern.compile("\\d*\\.\\d*\\.\\d*");
    private final List<String> filesOnFTP;
    private final List<Version> availableVersions;
    private final FtpClient ftpsClient;
    private final UpdateEventListener signalUpdateEventListener;
    private String downloadedFileName;
    private final Path homePath;

    public UpdateProcess(UpdateEventListener updateEventListener) {
        this.ftpsClient = new FtpClient();
        this.filesOnFTP = new ArrayList<>();
        this.availableVersions = new ArrayList<>();
        this.signalUpdateEventListener = updateEventListener;
        this.homePath = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();
    }

    @Override
    public void run() {
        startFTP();

        String jarPath = getCurrentJarPath();
        System.out.println("YOUR CURRENT JarPath: " + jarPath);

        Version currentVersion = getCurrentVersion(jarPath);
        System.out.println("YOUR CURRENT VERSION: " + currentVersion);

        getAvailableVersions();
        System.out.println("AVAILABLE VERSIONS: " + availableVersions);

        Version lastVersion = availableVersions.get(availableVersions.size() - 1);
        System.out.println("LAST VERSION: " + lastVersion.toString());

        if (currentVersion.compareTo(lastVersion) == 0) {
            System.out.println("You have current version");
        } else if (currentVersion.compareTo(lastVersion) < 0) {
            System.out.println("You need to update");
            signalUpdateEventListener.onUpdate(
                    "New Bot version " + lastVersion + " is available." +
                            "\nStarting to download...");
            try {
                downloadNewVersion(lastVersion);
                confirmDownload();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("CLOSING");
        try {
            ftpsClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCurrentJarPath() {
        System.out.println("TRYING TO GET JAR");
        try {
            File jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            return jarPath.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return "";
    }

    private Version getCurrentVersion(String jarPath) {
        System.out.println("TRYING TO CALC CUR VER ");
        return parseRegex(jarPath);
    }

    private void getAvailableVersions() {
        System.out.println("TRYING TO GET AVAILABLE VERSIONS");
        for (String file : filesOnFTP) {
            if (((file.contains("dstar_iot-all")) && (!file.contains("md5")))) {
                availableVersions.add(parseRegex(file));
            }
        }
        availableVersions.sort(Version::compareTo);
    }

    private Version parseRegex(String file) {
        Version version = Version.unknownVersion();
        final Matcher m = p.matcher(file);
        if (m.find()) {
            String[] parts = m.group(0).split("\\.");
            version = new Version(Integer.parseInt(parts[0]),
                    Integer.parseInt(parts[1]),
                    Integer.parseInt(parts[2]),
                    null, null, null);
        }
        return version;
    }

    private void downloadNewVersion(Version lastVersion) throws IOException {
        for (String file : filesOnFTP)
            if (file.contains(lastVersion.toString()) && !file.contains("md5")) {
                System.out.println("DOWNLOADING");
                downloadedFileName = file;
                ftpsClient.downloadFile(file, homePath + "/new-" + file);
            }
    }

    private void confirmDownload() throws IOException {
        String md5FileName = "md5_" + downloadedFileName.split("\\.jar")[0] + ".txt";
        ftpsClient.downloadFile(md5FileName, homePath + "/" + md5FileName);
        String fileContent = ftpsClient.retrieveFromFile(md5FileName);

        String commandContent = new BashConsoleController().run(
                "md5sum " + homePath + "/new-" + downloadedFileName
        ).split(" ")[0].trim();

        System.out.println("commandContent: " + commandContent);
        System.out.println("fileContent: " + fileContent);

        if (commandContent.equals(fileContent.trim())) {
            signalUpdateEventListener.onUpdate("The download is finished.\n" +
                    "Restart Bot to upgrade." +
                    "\nAll your scheduled tasks will be lost.");
        } else {
            signalUpdateEventListener.onUpdate("Something went wrong. I will try one day later or at restart.");
            try {
                Files.deleteIfExists(Paths.get(homePath + "/new-" + downloadedFileName));
            } catch (NoSuchFileException e) {
                System.out.println("No such file/directory exists");
            } catch (DirectoryNotEmptyException e) {
                System.out.println("Directory is not empty.");
            } catch (IOException e) {
                System.out.println("Invalid permissions.");
            }
        }
        try {
            Files.deleteIfExists(Paths.get(homePath + "/" + md5FileName));
        } catch (NoSuchFileException e) {
            System.out.println("No such file/directory exists");
        } catch (DirectoryNotEmptyException e) {
            System.out.println("Directory is not empty.");
        } catch (IOException e) {
            System.out.println("Invalid permissions.");
        }
    }

    private void startFTP() {
        System.out.println("Starting FTP WORK!");
        try {
            ftpsClient.open();
            filesOnFTP.addAll(ftpsClient.listFiles());
            System.out.println("FTP FILES: " + filesOnFTP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
