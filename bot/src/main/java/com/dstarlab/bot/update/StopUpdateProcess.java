package com.dstarlab.bot.update;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

class StopUpdateProcess implements Runnable {

    private final ScheduledExecutorService scheduler;
    private final ScheduledFuture<?> scheduledFuture;

    private static final boolean DONT_INTERRUPT_IF_RUNNING = false;

    final static Logger logger = LoggerFactory.getLogger(StopUpdateProcess.class);

    StopUpdateProcess(ScheduledFuture<?> scheduledFuture, ScheduledExecutorService scheduler) {
        this.scheduledFuture = scheduledFuture;
        this.scheduler = scheduler;
    }

    @Override
    public void run() {
        logger.info("Update finished.");

        scheduledFuture.cancel(DONT_INTERRUPT_IF_RUNNING);
        scheduler.shutdown();
    }
}