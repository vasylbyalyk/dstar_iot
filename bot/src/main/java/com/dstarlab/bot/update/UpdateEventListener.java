package com.dstarlab.bot.update;

public interface UpdateEventListener {
    void onUpdate(String message);
}