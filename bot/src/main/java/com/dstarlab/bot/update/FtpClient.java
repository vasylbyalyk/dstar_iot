package com.dstarlab.bot.update;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FtpClient {

    private final String server = "188.165.35.255";
    private final int port =  21;
    private final String user = "download";
    private final String password = "8T2u6B5x";
    private final FTPSClient ftpsClient;


    public FtpClient() {
        this.ftpsClient = new FTPSClient();
    }

    public void open() throws IOException {
        ftpsClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

        ftpsClient.connect(server, port);
        int reply = ftpsClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftpsClient.disconnect();
            throw new IOException("Exception in connecting to FTP Server");
        }

        ftpsClient.login(user, password);
        ftpsClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpsClient.enterLocalPassiveMode();
    }

    public void close() throws IOException {
        ftpsClient.disconnect();
    }

    public void putFileToPath(File file, String path) throws IOException {
        ftpsClient.storeFile(path, new FileInputStream(file));
    }

    public void downloadFile(String source, String destination) throws IOException {
        FileOutputStream out = new FileOutputStream(destination);
        ftpsClient.retrieveFile(source, out);
    }

    public Collection<String> listFiles() throws IOException {
        return Arrays.stream(ftpsClient.listFiles())
                .map(FTPFile::getName)
                .collect(Collectors.toList());
    }

    public Collection<String> listFiles(String path) throws IOException {
        FTPFile[] files = ftpsClient.listFiles(path);
        return Arrays.stream(ftpsClient.listFiles())
                .map(FTPFile::getName)
                .collect(Collectors.toList());
    }

    public String retrieveFromFile(String filePath){
        try {
            InputStream inputStream = ftpsClient.retrieveFileStream(filePath);
            Scanner sc = new Scanner(inputStream);
            StringBuffer sb = new StringBuffer();
            while(sc.hasNext()){
                sb.append(sc.nextLine());
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}