package com.dstarlab.bot.update;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class Updater {

    final static Logger logger = LoggerFactory.getLogger(Updater.class);

    private final long initialDelay;
    private final long delayBetweenRuns;
    private final long shutdownAfter;
    private final ScheduledExecutorService scheduler;
    private final UpdateEventListener updateEventListener;

    public Updater (final UpdateEventListener updateEventListener) {
        this.updateEventListener = updateEventListener;
        this.initialDelay = 0;
        this.delayBetweenRuns = 1;
        this.shutdownAfter = Long.MAX_VALUE;
        this.scheduler = Executors.newScheduledThreadPool(1);
    }

    public void startUpdateMonitoring() {
        logger.info("Update monitor started with "
                + "periodicity of " + delayBetweenRuns
                + " for " + Long.MAX_VALUE + " days");
        Runnable updateProcess = new UpdateProcess(updateEventListener);
        ScheduledFuture<?> updateProcessFuture = scheduler.scheduleWithFixedDelay(
                updateProcess, initialDelay, delayBetweenRuns, TimeUnit.DAYS
        );
        Runnable stopUpdateProcess = new StopUpdateProcess(updateProcessFuture, scheduler);
        scheduler.schedule(stopUpdateProcess, shutdownAfter, TimeUnit.DAYS);
    }
}