package com.dstarlab.bot.controller.bash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BashConsoleController {

    private String result = "";

    public BashConsoleController() {
    }

    public void start(String command) {
        if (command == null) {
            return;
        }

        if (command.equals("")) {
            return;
        }

        final ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);
        try {
            final Process process = processBuilder.start();
            int exitVal = process.waitFor();
            System.out.println(exitVal);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    // public int whiles(String command) {

    //     if (command.equals("")) {
    //         return -1;
    //     }
    //     int exitVal = 0;
    //     final ProcessBuilder processBuilder = new ProcessBuilder();
    //     processBuilder.command("bash", "-c", command);
    //     try {
    //         final Process process = processBuilder.start();

    //         final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

    //         String line;
    //         while (exitVal == 0) {
    //             line = reader.readLine();
    //             process.waitFor();
    //         }
    //     } catch (IOException e) {
    //         e.printStackTrace();
    //     } catch (InterruptedException e) {
    //         e.printStackTrace();
    //     }

    //     return exitVal;
    // }

    public int starter(String command) {
        if (command == null) {
            return -1;
        }

        if (command.equals("")) {
            return -1;
        }
        int exitVal = 0;
        final ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);
        try {
            final Process process = processBuilder.start();

            final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while (exitVal == 0) {
                line = reader.readLine();
                System.out.println(line);
                exitVal = process.waitFor();
                System.out.println("stream " + exitVal);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return exitVal;
    }

    public String runRoot(String command, String password) {
        return run(String.format("echo '%s' | sudo -kS %s", password, command));
    }

    public void reboot(String command) {                    
          boolean success = false;
        System.out.println("Executing BASH command:\n   " + command);
        Runtime r = Runtime.getRuntime();
        String[] commands = {"bash", "-c", command};
        try {
            Process p = r.exec(commands);

            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";

            while ((line = b.readLine()) != null) {
                System.out.println(line);
            }

            b.close();
            success = true;
        } catch (Exception e) {
            System.err.println("Failed to execute bash with command: " + command);
            e.printStackTrace();
        }
        System.out.println("success: " + success);
    }

    public String run(String command) {
        System.out.println("run : " + command);
        result = "";

        if (command == null || command.equals("")) {
            return null;
        }

        ProcessBuilder processBuilder = new ProcessBuilder();

        // -- Linux --

        // Run a shell command
        processBuilder.command("/bin/bash", "-c", command);

        // -- Windows --

        // Run a command
        // processBuilder.command("cmd.exe", "/c", "");

        try {
            System.out.println("try : " + command);
            final Process process = processBuilder.start();

            StringBuilder output = new StringBuilder();

            final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            System.out.println("while");
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                result += String.valueOf(output) + "\n";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String python3(String command)  {
        result = "";
        if (command == null || command.equals("")) {
            return "empty command";
        }

//        ProcessBuilder processBuilder = new ProcessBuilder();
//        processBuilder.command("python3", command);


        try {
            String str = "python3 " + command;
            Process process = Runtime.getRuntime().exec(str);

            StringBuilder output = new StringBuilder();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                result += String.valueOf(output) + "\n";
            }
        }catch (IOException | InterruptedException e){
            e.printStackTrace();
        }
        return result;
    }

    public String getResult() {
        return result;
    }
}
