package com.dstarlab.bot.controller.system;

public enum SystemArchitectureController {

    INSTANCE;

    private OSName os;

    public enum OSName {
        RPI3,
        RPI4,
        HOST
    }

    SystemArchitectureController() {
        final String osName = System.getProperty("os.arch");
        final String osVersion = System.getProperty("os.version");
        if (osVersion.contains("v7l") && osName.contains("arm")) {
            os = OSName.RPI4;
        } else if (osVersion.contains("v7") && osName.contains("arm")) {
            os = OSName.RPI3;
        }else{
            os = OSName.HOST;
        }
    }

    public OSName getOS() {
        return os;
    }
}

