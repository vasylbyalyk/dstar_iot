package com.dstarlab.bot.controller.system;

import com.dstarlab.bot.camera.Recorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class SystemUtils {
    private static final Logger logger = LoggerFactory.getLogger(SystemUtils.class);

    public static void deleteFile(String path) {
        try {
            Files.deleteIfExists(Paths.get(path));
        } catch (NoSuchFileException e) {
            logger.info("No such file/directory exists: " + path);
        } catch (DirectoryNotEmptyException e) {
            logger.info("Directory is not empty: " + path);
        } catch (IOException e) {
            logger.info("Invalid permissions: " + path);
        }
//        logger.info("Deletion successful: " + path);
    }

    public static String pathToVideo(long time) {
        Date dateFile = new Date(time);
        String yyyy = "yyyy";
        SimpleDateFormat sy = new SimpleDateFormat(yyyy);
        String mm = "MM";
        SimpleDateFormat sm = new SimpleDateFormat(mm);
        String dd = "dd";
        SimpleDateFormat sd = new SimpleDateFormat(dd);
        String hh = "HH";
        SimpleDateFormat sh = new SimpleDateFormat(hh);

        return sy.format(dateFile)
                + "/"
                + sm.format(dateFile)
                + "/"
                + sd.format(dateFile)
                + "/"
                + sh.format(dateFile)
                + "/";
    }

//    public static void replaceInFile(File inputFile, String toBeReplaced, String replacement) {
//        try {
//            File tempFile = new File(inputFile.getAbsolutePath() + "_temp");
//
//            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
//            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
//
//            String currentLine;
//
//            while ((currentLine = reader.readLine()) != null) {
//                if (currentLine.equalsIgnoreCase(toBeReplaced)) {
//                    writer.write(replacement);
//                    if (!replacement.isEmpty())
//                        writer.write('\n');
//                } else {
//                    writer.write(currentLine + '\n');
//                }
//            }
//            writer.close();
//            reader.close();
//            SystemUtils.deleteFile(inputFile.getAbsolutePath());
//            boolean success = tempFile.renameTo(inputFile);
//            System.out.println("result of replacement is " + success);
//        } catch (NoSuchFileException e) {
//            System.out.println("No such file/directory exists: " + inputFile);
//        } catch (DirectoryNotEmptyException e) {
//            System.out.println("Directory is not empty: " + inputFile);
//        } catch (IOException e) {
//            System.out.println("Invalid permissions: " + inputFile);
//        }
//
//    }

    public static void replaceInFile(File inputFile, String toBeReplaced, String replacement) throws IOException {
        String content = String.join("\n", read(inputFile.getAbsolutePath()));
        String replaced = content.replace(toBeReplaced, replacement);

        File tempFile = new File(inputFile.getAbsolutePath() + "_temp");
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        writer.write(replaced);
        writer.close();

        SystemUtils.deleteFile(inputFile.getAbsolutePath());
        boolean success = tempFile.renameTo(inputFile);
        System.out.println("result of replacement is " + success);
    }

    public static void write(String content, String fileName) {
        try {
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            }
            FileWriter fileWriter = new FileWriter(fileName, true);
            fileWriter.write(content + '\n');
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> read(String fileName) {
        List<String> content = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                if (!s.isEmpty())
                    content.add(s.trim());
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return content;
    }

    public static String findStringInFile(String fileName, String content) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileName));
        String wantedString = "";

        while (scanner.hasNextLine()) {
            wantedString = scanner.nextLine();
            if (wantedString.contains(content)) {
                break;
            }
        }
        return wantedString;
    }

}
