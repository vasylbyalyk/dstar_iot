package com.dstarlab.bot.controller.system;

import com.dstarlab.bot.controller.bash.BashConsoleController;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.Collectors;

public class TimezoneController {

    private final BashConsoleController bashConsole;
    private final List<String> regions;

    public TimezoneController() {
        this.bashConsole = new BashConsoleController();
        this.regions = getAllRegions();
    }

    public String setTimezone(String timeZone) {
        setJavaTimezone(timeZone);
        bashConsole.run("sudo unlink /etc/localtime");
        bashConsole.run(
                "sudo ln -s /usr/share/zoneinfo/"
                        + timeZone
                        + " /etc/localtime"
        );
        if (getCurrentTimezone().equals(timeZone)) {
            return "The current timezone: " + timeZone;
        }
        return "Something went wrong. Please try again";
    }

    public String setTimezone(String region, String zoneName) {
        String timeZone = region + "/" + zoneName;
        return setTimezone(timeZone);
    }

    public String getCurrentTimezone() {
        if (Files.exists(Paths.get("/etc/localtime"))) {
            return bashConsole.run("ls -l /etc/localtime | awk -F'zoneinfo/' '{print $2}'").trim();
        }
        return "";
    }

    public void setJavaTimezone(String timezone) {
        TimeZone.setDefault(TimeZone.getTimeZone(timezone));
    }

    public List<String> getAllRegions() {
        String[] regions = bashConsole.run(
                "ls /usr/share/zoneinfo/ " +
                        "| grep -v 'tab' " +
                        "| grep -v 'SystemV' " +
                        "| grep -v 'posix' " +
                        "| grep -v 'Etc' " +
                        "| grep -v 'Pacific' " +
                        "| grep -v 'right'").split("\n");
        return Arrays.asList(regions);
    }

    public List<String> getAllZones(String region) {
        if (regions.contains(region)) {
            final String regionFolder = "/usr/share/zoneinfo/";
            if (new File(regionFolder + region).isDirectory())
                return Arrays.asList(Objects.requireNonNull(new File(regionFolder + region).list()));
        }
        return Collections.emptyList();
    }

    public List<String> getFullTimezoneList() {
        List<String> fullList = new ArrayList<>();
        for (String region : regions) {
            List<String> zones = getAllZones(region);
            zones = zones.stream().map(zone -> region + "/" + zone).collect(Collectors.toList());
            zones.sort(Collator.getInstance());
            fullList.addAll(zones);
        }
        return fullList;
    }
}