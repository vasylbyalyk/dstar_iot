package com.dstarlab.bot.controller.system;

import com.dstarlab.bot.controller.Wifi;
import com.dstarlab.bot.controller.bash.BashConsoleController;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WifiController {

    //    private final static String wpa_path = "/home/roman/wpa_supplicant.conf";
    private final static String wpa_path = "/etc/wpa_supplicant.conf";
    private final static String networkTemplate = "network={\n%s\n}";

    public WifiController() {

    }

    public static List<Wifi> wifiParser(List<String> wpaSupplicantContent) {
        ArrayList<Wifi> wifi = new ArrayList<>();
        String fullString = String.join("\n", wpaSupplicantContent);
        Pattern ssidPattern = Pattern.compile("ssid=\"(.*)\"");
        Pattern pskPattern = Pattern.compile("psk=\"(.*)\"");
        Pattern priorityPattern = Pattern.compile("priority=(.*)");
        for (String string : fullString.split("network")) {
            Matcher ssidMatcher = ssidPattern.matcher(string);
            Matcher pskMatcher = pskPattern.matcher(string);
            Matcher priorityMatcher = priorityPattern.matcher(string);
            while (ssidMatcher.find() && pskMatcher.find() && priorityMatcher.find()) {
                String ssid = ssidMatcher.group(1);
                String psk = pskMatcher.group(1);
                String priority = priorityMatcher.group(1);
                wifi.add(new Wifi(ssid, psk, Integer.parseInt(priority)));
            }
        }
        return wifi;
    }

    public String setCurrentWifi() {
        // get number from wpa_cli list_networks
        // run("wpa_cli select_network 0")
        return "";
    }

    public String getCurrentWIFI() {
        BashConsoleController bash = new BashConsoleController();
        String s = bash.run("sudo iw dev wlan0 info | grep ssid");
        return s.trim().replace("ssid ", "");
    }

    public List<Wifi> getAllWifi() {
        return wifiParser(SystemUtils.read(wpa_path));
    }

    private String buildNetworkConf(Wifi wifi) {
        return String.format(networkTemplate, wifi.toString()) + '\n';
    }

    public void removeWifi(String wifiSSID) {
        List<Wifi> wifis = getAllWifi();
        for (Wifi wifi : wifis) {
            if (wifiSSID.equals(wifi.getSsid())) {
                System.out.println("I HAVE FOUND YOU: " + wifi);
                System.out.println("I WILL REPLACE: " + String.format(networkTemplate, wifi.toString()));
                System.out.println("TO: " + "");

                try {
                    SystemUtils.replaceInFile(new File(wpa_path), String.format(networkTemplate, wifi.toString()), "");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String setNewWIFI(Wifi wifi) {
        if (wifi.getPsk().length() >= 8) {

            List<Wifi> wifis = getAllWifi();

            if (wifis.isEmpty())
                wifi.setPriority(0);
            else {
                List<Integer> values = wifis.stream().map(Wifi::getPriority)
                        .collect(Collectors.toList());
                wifi.setPriority(Collections.max(values) + 1);
            }

            SystemUtils.write(buildNetworkConf(wifi), wpa_path);
            new BashConsoleController().run("sudo ifdown -a && sudo ifup -a");
            String currentWifi = getCurrentWIFI();
            if (currentWifi.isEmpty())
                return "There is no available network";
            return "Current Wifi network: " + getCurrentWIFI();
        }
        return "WiFi password should be >= 8 symbols";
    }
}