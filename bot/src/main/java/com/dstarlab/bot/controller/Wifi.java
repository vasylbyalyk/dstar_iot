package com.dstarlab.bot.controller;

// If the network is hidden, so that the access point does not broadcast its presence, you must specify the scan_ssid=1 option:
public class Wifi {

    boolean hidden = false;
    private String ssid;
    private String psk;
    private int priority = 0;

    Wifi() {

    }

    public Wifi(String ssid, String psk) {
        this.ssid = ssid;
        this.psk = psk;
        this.hidden = false;
    }

    public Wifi(String ssid, String psk, boolean hidden) {
        this.ssid = ssid;
        this.psk = psk;
        this.hidden = hidden;
    }

    public Wifi(String ssid, String psk, int priority) {
        this.ssid = ssid;
        this.psk = psk;
        this.priority = priority;
        this.hidden = false;
    }

    public Wifi(String ssid, String psk, int priority, boolean hidden) {
        this.ssid = ssid;
        this.psk = psk;
        this.priority = priority;
        this.hidden = hidden;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(final boolean hidden) {
        this.hidden = hidden;
    }

    public String getSsid() {
        return this.ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPsk() {
        return this.psk;
    }

    public void setPsk(String psk) {
        this.psk = psk;
    }

    @Override
    public String toString() {
        return "ssid=\"" + ssid + '\"' + '\n' +
                "psk=\"" + psk + '\"' + '\n' +
                "scan_ssid=" + (isHidden() ? 1 : 0) + '\n' +
                "priority=" + priority;
    }
}