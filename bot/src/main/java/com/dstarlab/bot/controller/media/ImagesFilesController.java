package com.dstarlab.bot.controller.media;

import com.dstarlab.bot.camera.GStreamerUtil;
import com.dstarlab.bot.controller.system.SystemUtils;
import com.dstarlab.bot.database.managers.CameraManager;

import org.freedesktop.gstreamer.Pipeline;
import org.freedesktop.gstreamer.lowlevel.MainLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.dstarlab.bot.properties.ConfigStrings.PATH_PHOTO;

public class ImagesFilesController {

    private static final Logger logger = LoggerFactory.getLogger(ImagesFilesController.class);

    public String getImageFile(int cameraId) {

        final MainLoop loop = new MainLoop();

        String pathPhotoCameraId = String.format(PATH_PHOTO, cameraId) + System.currentTimeMillis() + ".jpg";
        String takeSnapshotCommand = "udpsrc timeout=1 port=" + "500" + cameraId + " !"
                + " application/x-rtp,encoding-name=JPEG,payload=26 !"
                + " rtpjpegdepay ! jpegdec !"
                + " queue ! clockoverlay text=\"Camera " + cameraId + ":\""
                + " ! jpegenc snapshot=TRUE ! " + "filesink location=\""
                + pathPhotoCameraId + "\"";
        System.out.println(takeSnapshotCommand);
        final Pipeline pipe = GStreamerUtil.buildPipeline(takeSnapshotCommand);

        GStreamerUtil.setLoggingPipe(pipe, logger, ImagesFilesController.class.getName(), true, loop, null);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future futureTask = executorService.submit(() -> {
            pipe.play();
            loop.run();
        });

        executorService.shutdown();
        try {
            executorService.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (futureTask.isDone()) {
            return pathPhotoCameraId;
        }

        pipe.stop();
        loop.quit();

        return Integer.toString(cameraId);

    }

    public ArrayList<String> getImageFileFromAll() {

        CameraManager cameraManager = CameraManager.INSTANCE;

        ArrayList<String> filePaths = new ArrayList<>();
        List<Integer> indexes = cameraManager.getActiveIndexes();
        ExecutorService exec = Executors.newFixedThreadPool(indexes.size());

        for (int index : indexes) {
            exec.submit(() -> {
                filePaths.add(getImageFile(index));
            });
        }

        exec.shutdown();
        try {
            exec.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return filePaths;
    }

    public void deleteImage(String path) {
        SystemUtils.deleteFile(path);
    }

    public void deleteImage(List<String> paths) {
        for (String path : paths) {
            SystemUtils.deleteFile(path);
        }
    }
}
