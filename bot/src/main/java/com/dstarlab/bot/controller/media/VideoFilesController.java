package com.dstarlab.bot.controller.media;

import com.dstarlab.bot.camera.VideoMerger;
import com.dstarlab.bot.controller.system.SystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.dstarlab.bot.properties.ConfigStrings.PATH;

public class VideoFilesController {

    //    private static final Logger logger = LoggerFactory.getLogger(Recorder.class);
    private String pathCameraId;

    private int cameraId;

    public String getPathCameraId() {
        return pathCameraId;
    }
    public VideoFilesController(){}
    public VideoFilesController(int cameraId) {
        this.cameraId = cameraId;
        pathCameraId = String.format(PATH, cameraId);
    }

    public String getFilesListToDuration(long timeStart, long duration) {
        long timeEnd = timeStart + duration;
        int indexStart = -1;
        int indexEnd = -1;
        String startFolder = getFolderVideo(timeStart);
        String endFolder = getFolderVideo(timeEnd);

        List<File> listFile = getListFileFromFolder(startFolder);
        if (!startFolder.equals(endFolder)) {
            listFile.addAll(getListFileFromFolder(endFolder));
        }

        List<Long> listFileTime = getListTimestampFileFromFolder(listFile);

        for (int i = 0; i < listFileTime.size() - 2; i++) {
            if (listFileTime.get(i) <= timeStart && listFileTime.get(i + 1) > timeStart) {
                indexStart = i;
            }
        }
        indexStart = (indexStart == -1 ? listFileTime.size() - 1 : indexStart);
        for (int i = indexStart; i < listFileTime.size(); i++) {
            if (listFileTime.get(i) < timeEnd) {
                indexEnd = i;
            }
        }

        indexEnd = (indexEnd == -1 ? listFileTime.size() - 1 : indexEnd);

        if (indexStart == indexEnd || indexEnd == -1) {
            return listFile.get(indexStart).getPath();
        } else {
            ArrayList<String> fileAttachment = new ArrayList<>();
            for (int i = indexStart; i <= indexEnd; i++) {
                fileAttachment.add(listFile.get(i).getPath());
            }
            return VideoMerger.merge(fileAttachment, cameraId);
        }
    }

    public List<File> getListFileFromFolder(String pathVideoFolder) {
        List<File> videoListFiles = null;

        try {
            videoListFiles = Files.walk(Paths.get(pathVideoFolder), 1)
                    .filter(Files::isRegularFile)
                    .map(path -> new File(path.toString()))
                    .sorted()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return videoListFiles;
    }

    private List<Long> getListTimestampFileFromFolder(List<File> listFile) {
        return listFile.stream()
                .map(file -> Long.parseLong(file.getName().substring(0, file.getName().indexOf('.'))) * 1000)
                .collect(Collectors.toList());
    }

    private String getFolderVideo(long time) {
        return pathCameraId + SystemUtils.pathToVideo(time);
    }

    public String getLastVideo() {
        String pathCameraId = String.format(PATH, cameraId);

        String lastFolder = getLastFolder(pathCameraId);
    List<File> videoListFiles = getListFileFromFolder(lastFolder);
        return videoListFiles.get(videoListFiles.size() - 2).getPath();
    }

    private String getLastFolder(String pathFolder) {
        List<File> findFolder = findFolder(pathFolder);
        if (findFolder.size() == 0) {
            return pathFolder;
        }
        return getLastFolder(findFolder.get(findFolder.size() - 1).getPath());
    }

    public String getFirstFolder(String pathFolder) {
        List<File> findFolder = findFolder(pathFolder);
        if (findFolder.size() == 0) {
            return pathFolder;
        }
        return getFirstFolder(findFolder.get(0).getPath());
    }

    public List<File> findFolder(String pathFolder) {
        List<File> findFolder = null;
        try {
            findFolder = Files.walk(Paths.get(pathFolder), 1)
                    .filter(Files::isDirectory)
                    .map(path -> new File(path.toString()))
                    .sorted()
                    .collect(Collectors.toList());
            findFolder.remove(0);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return findFolder;
    }

    public void deleteVideo(String path) {
        SystemUtils.deleteFile(path);
    }
}
