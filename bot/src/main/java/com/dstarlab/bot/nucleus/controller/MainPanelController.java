package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;

class MainPanelController implements DSarCommand {

    @Override
    public void processDStarCommand(MessageModel messageModel) {
        CommandsManager commandsManager = new CommandsManager(messageModel.getManager());
        commandsManager.sendMessage(messageModel.getRecipientNumber(), "Wrong command");
    }
}