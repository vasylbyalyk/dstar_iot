package com.dstarlab.bot.nucleus.model;

import org.json.JSONException;
import org.json.JSONObject;

import static com.dstarlab.bot.properties.ConfigStrings.*;

public class DStarCommandModel {

    private String name;
    private String command;
    private String commandTyp;
    private JSONObject commandJson;

    private String adminCommands;
    private int cameraId;
    private long time_start;
    private long time_end;
    private long duration;
    private long timer;
    private String text_field;
    private String additional_text_field;

    DStarCommandModel(String text) {
        try {
            commandJson = new JSONObject(text);
            try {
                name = (String) commandJson.get("name");
            } catch (JSONException e) {
                name = null;
            }
            try {
                commandTyp = (String) commandJson.get("commandTyp");
            } catch (JSONException e) {
                commandTyp = null;
            }
            try {
                command = (String) commandJson.get("command");
            } catch (JSONException e) {
                if (text.equals(TEXT_HELLO) || text.equals(TEXT_UPDATE)) {
                    command = text;
                } else {
                    command = null;
                }
            }
            try {
                adminCommands = (String) commandJson.get("command");
            } catch (JSONException e) {
                adminCommands = null;
            }
            try {
                text_field = (String) commandJson.get("text_field");
            } catch (JSONException e) {
                text_field = null;
            }
            try {
                additional_text_field = (String) commandJson.get("additional_text_field");
            } catch (JSONException e) {
                additional_text_field = null;
            }
            try {
                cameraId = Integer.parseInt(commandJson.get("cameraId").toString());
            } catch (JSONException | NumberFormatException e) {
                cameraId = 1;
            }
            try {
                time_start = Long.parseLong(commandJson.get("time_start").toString());
            } catch (JSONException | NumberFormatException e) {
                time_start = 0;
            }
            try {
                time_end = Long.parseLong(commandJson.get("time_end").toString());
            } catch (JSONException | NumberFormatException e) {
                time_end = 0;
            }
            try {
                duration = Long.parseLong(commandJson.get("duration").toString());
                    duration = duration * 60000;
            } catch (JSONException e) {
                duration = 10000;
            }
            try {
                timer = Long.parseLong(commandJson.get("timer").toString());
                if (timer < 10000)
                    timer = 10000;
            } catch (JSONException | NumberFormatException e) {
                timer = 0;
            }
        } catch (JSONException e) {
            name = null;
            command = null;
            cameraId = 1;
            time_start = 0;
            time_end = 0;
            duration = 0;
            timer = 0;
        }
    }

    DStarCommandModel(String text, boolean flag) {
        System.out.println("--------------TextCommand");
        name = "IotCommand";
        commandTyp = "TextCommand";

        switch (text.toUpperCase()) {
            case TEXT_UPDATE:
                System.out.println("-------------- : " + TEXT_UPDATE);
                command = TEXT_UPDATE;
                break;
            case TEXT_HELLO:
                System.out.println("-------------- : " + TEXT_HELLO);
                command = TEXT_HELLO;
                break;
            case TEXT_VIDEO:
                System.out.println("-------------- : " + TEXT_VIDEO);
                command = TEXT_VIDEO;
                break;
            case TEXT_HI:
                System.out.println("-------------- : " + TEXT_HI);
                command = TEXT_HI;
                break;
            default:
                name = null;
                commandTyp = null;
                command = null;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommandTyp() {
        return commandTyp;
    }

    public void setCommandTyp(String commandTyp) {
        this.commandTyp = commandTyp;
    }

    public JSONObject getCommandJson() {
        return commandJson;
    }

    public void setCommandJson(JSONObject commandJson) {
        this.commandJson = commandJson;
    }

    public String getCommand() {
        return command;
    }

    public String getAdminCommands() {
        return adminCommands;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getCameraId() {
        return cameraId;
    }

    public void setCameraId(int cameraId) {
        this.cameraId = cameraId;
    }

    public long getTime_start() {
        return time_start;
    }

    public void setTime_start(long time_start) {
        this.time_start = time_start;
    }

    public long getTime_end() {
        return time_end;
    }

    public void setTime_end(long time_end) {
        this.time_end = time_end;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getTimer() {
        return timer;
    }

    public String getTextField() {
        return text_field;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public String getAdditional_text_field() {
        return additional_text_field;
    }

    public void setAdditional_text_field(final String additional_text_field) {
        this.additional_text_field = additional_text_field;
    }

    @Override
    public String toString() {
        return "DStarCommandModel{" +
                "name='" + name + '\'' +
                ", commandTyp='" + commandTyp + '\'' +
                ", command='" + command + '\'' +
                ", cameraId=" + cameraId +
                ", time_start=" + time_start +
                ", time_end=" + time_end +
                ", duration=" + duration +
                ", timer=" + timer +
                ", adminCommands=" + adminCommands +
                ", text_field=" + text_field +
                ", additional_text_field=" + additional_text_field +
                '}';
    }
}
