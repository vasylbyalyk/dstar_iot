package com.dstarlab.bot.nucleus.model;

import com.dstarlab.bot.nucleus.IDstarMessageProcessor;
import com.dstarlab.bot.nucleus.SignalManager;

//import org.whispersystems.libsignal.util.guava.Optional;
//import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.dstar.DstarMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

public class MessageModel {

    private String recipientNumber;
    private int device;
    private String massage;
    private SignalServiceDataMessage signalServiceDataMessage;
    private boolean receipt;
    private boolean decryptMessage = true;
    private int typMessage;
    private GroupModel groupModel = new GroupModel();
    private DStarCommandModel dStarCommandModel = null;
    private boolean isCallMessage = false;
    private boolean workMessageModel = true;
    private SignalManager manager;
    private SignalServiceAddress signalServiceAddress;
    private long timestamp;
    private IDstarMessageProcessor iDstarMessageProcessor;


    public SignalServiceAddress getSignalServiceAddress() {
        return signalServiceAddress;
    }

    public void setSignalServiceAddress(final SignalServiceAddress signalServiceAddress) {
        this.signalServiceAddress = signalServiceAddress;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public SignalManager getManager() {
        return manager;
    }

    public MessageModel(SignalManager manager, IDstarMessageProcessor iDstarMessageProtocol) {
        this.manager = manager;
        this.iDstarMessageProcessor = iDstarMessageProtocol;
    }

    public DStarCommandModel getDStarCommandModel() {
        return dStarCommandModel;
    }

    public void setDStarCommandModel() {
        Optional<DstarMessage> dstarMessageOptional = signalServiceDataMessage.getDstarMessage();
        this.dStarCommandModel = null;

        String isMessageWithoutFurtherProcessing = iDstarMessageProcessor
                .handleDstarMessageWithoutFurtherProcessing(signalServiceDataMessage, dstarMessageOptional.get());
        if (!isMessageWithoutFurtherProcessing.equals("false")) {
            this.dStarCommandModel = new DStarCommandModel(isMessageWithoutFurtherProcessing);
        } else {
            this.dStarCommandModel = new DStarCommandModel(signalServiceDataMessage.getBody().get(), true);
        }

    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getRecipientNumber() {
        return recipientNumber;
    }

    public void setRecipientNumber(String recipientNumber) {
        this.recipientNumber = recipientNumber;
    }

    public boolean isReceipt() {
        return receipt;
    }

    public void setReceipt(boolean receipt) {
        this.receipt = receipt;
    }

    public boolean isDecryptMessage() {
        return decryptMessage;
    }

    public void setDecryptMessage(boolean decryptMessage) {
        this.decryptMessage = decryptMessage;
    }

    public int getTypMessage() {
        return typMessage;
    }

    public void setTypMessage(int typMessage) {
        this.typMessage = typMessage;
    }

//    public List<AttachmentsModel> getAttachments() {
//        return attachments;
//    }
//
//    public void setAttachments(List<AttachmentsModel> attachments) {
//        this.attachments = attachments;
//    }

    public GroupModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(GroupModel groupModel) {
        this.groupModel = groupModel;
    }

    //    public Quote getQuote() { return quote; }
//
//    public void setQuote(Quote quote) { this.quote = quote; }
    public SignalServiceDataMessage getSignalServiceDataMessage() {
        return signalServiceDataMessage;
    }

    public void setSignalServiceDataMessage(final SignalServiceDataMessage signalServiceDataMessage) {
        this.signalServiceDataMessage = signalServiceDataMessage;
        setDStarCommandModel();
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "recipientNumber='" + recipientNumber + '\'' +
                ", device=" + device +
                ", timestamp='" + timestamp + '\'' +
                ", massage='" + massage + '\'' +
                ", receipt=" + receipt +
                ", decryptMessage=" + decryptMessage +
                ", typMessage='" + typMessage + '\'' +
                ", groupModel=" + groupModel +
                ", isCallMessage=" + isCallMessage +
                ", dStarCommandModel=" + dStarCommandModel +
                '}';
    }

    public boolean getIsCallMessage() {
        return isCallMessage;
    }

    public void setIsCallMessage(boolean callMessage) {
        this.isCallMessage = callMessage;
    }

    public boolean isWorkMessageModel() {
        return workMessageModel;
    }

    public void setWorkMessageModel(boolean workMessageModel) {
        this.workMessageModel = workMessageModel;
    }

}
