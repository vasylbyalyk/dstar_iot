package com.dstarlab.bot.nucleus.view;

import com.dstarlab.bot.nucleus.controller.DStarCommandsController;
import com.dstarlab.bot.nucleus.model.DStarCommandModel;
import com.dstarlab.bot.nucleus.model.MessageModel;

import java.util.Date;
import java.util.Timer;

import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_SCHEDULE;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_RECORD;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_SCHEDULE;


public class ScheduleView {

    public ScheduleView(MessageModel messageModel) {
        Timer timer = new Timer();
        DStarCommandsController dStarCommandsController = new DStarCommandsController(messageModel.getManager());
        ScheduleTimerTaskView scheduleTimerTaskView = new ScheduleTimerTaskView(timer, messageModel);
        DStarCommandModel command =  messageModel.getDStarCommandModel();

        switch (command.getCommand()){

            case COMMAND_PHOTO_EVERY:
                if (command.getTime_start() + 10000 < messageModel.getTimestamp())
                    command.setTime_start(messageModel.getTimestamp());

                if (command.getTime_start() < command.getTime_end()) {
                    ScheduleTimerTaskView schedule = new ScheduleTimerTaskView(timer, messageModel);
                    timer.schedule(schedule,
                            new Date(command.getTime_start()),
                            command.getTimer());
                }else{
                    System.out.println("incorrect time period COMMAND_PHOTO_EVERY" );
                }
                break;
            case COMMAND_VIDEO_EVERY:
                if (command.getTime_start() + 10000 < messageModel.getTimestamp())
                    command.setTime_start(messageModel.getTimestamp());

                if (command.getTime_start() < command.getTime_end()) {
                    ScheduleTimerTaskView schedule = new ScheduleTimerTaskView(timer, messageModel);
                    long timeStart = command.getTime_start() + command.getDuration();
                    timer.schedule(schedule,
                            new Date(timeStart),
                            command.getTimer());
                }else{
                    System.out.println("incorrect time period COMMAND_VIDEO_EVERY");
                }
                break;
            case COMMAND_VIDEO_RECORD:
                if (command.getTime_start() > messageModel.getTimestamp()){
                    System.out.println("incorrect time period COMMAND_VIDEO_RECORD");
                } else {
                    timer.schedule(scheduleTimerTaskView, new Date(command.getTime_start()));
                }
                break;
            case COMMAND_PHOTO_SCHEDULE:
                if (command.getTime_start() + 10000 < messageModel.getTimestamp())  {
                    command.setTime_start(messageModel.getTimestamp());
                    System.out.println("incorrect time period");
                }

                timer.schedule(scheduleTimerTaskView, new Date(command.getTime_start()));
                break;
            case COMMAND_VIDEO_SCHEDULE:
                long timeStart = command.getTime_start() + command.getDuration() ;
                timer.schedule(scheduleTimerTaskView, new Date(timeStart));
                break;
            case COMMAND_PHOTO:
            case COMMAND_VIDEO:
            default:
                dStarCommandsController.buildMessageForSend(messageModel, command.getCommand());
        }
    }
}
