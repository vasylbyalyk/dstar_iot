package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.controller.bash.BashConsoleController;
import com.dstarlab.bot.properties.config.ConfigSettings;
import com.dstarlab.bot.properties.DevicesProperties;
import com.dstarlab.bot.properties.management.DevicePanel;
import com.dstarlab.bot.properties.management.PanelIot;
import com.dstarlab.bot.nucleus.model.MessageModel;

import org.whispersystems.signalservice.api.messages.dstar.IotMessage;
import org.whispersystems.signalservice.api.messages.dstar.SetupIotMessage;

class DevicePanelController  implements DSarCommand {

    @Override
    public void processDStarCommand(final MessageModel messageModel) {
        CommandsManager commandsManager = new CommandsManager(messageModel.getManager());
        DevicesProperties devicesProperties = new DevicesProperties();
        BashConsoleController bash = new BashConsoleController();
        PanelIot panel = new DevicePanel();
        IotMessage iotMessage;

        switch (messageModel.getDStarCommandModel().getAdminCommands()) {
            case "showDevice":
//                System.out.println("case \"showDevice\":");
//                iotMessage = new SetupIotMessage(
//                        ConfigSettings.INSTANCE.getNumber(),
//                        panel.sendPanelIot(messageModel.getRecipientNumber()));
//                commandsManager.sendMessage(messageModel.getRecipientNumber(), iotMessage);
                break;
            case "discoverDevice":
//                String commandDiscover = "/home/pi/iot-raspberry-pi/python-devices/scripts/discover.py";
//                String result1 = bash.python3(commandDiscover);
//                commandsManager.sendMessage(messageModel.getRecipientNumber(), result1);
//
//
//                iotMessage = new SetupIotMessage(
//                        ConfigSettings.INSTANCE.getNumber(),
//                        panel.sendPanelIot(messageModel.getRecipientNumber()));
//                commandsManager.sendMessage(messageModel.getRecipientNumber(), iotMessage);
                break;
//            case "commandDevice":
//                System.out.println("commandDevice");
//                try {
//                    String runString = "";
//                    System.out.println(runString);
//                    JSONObject obj = new JSONObject(messageModel.getDStarCommandModel().getCommandJson().toString());
//
//                    String commandDevice = "/home/pi/iot-raspberry-pi/python-devices/scripts/" + devicesProperties.getDeviceScript(obj.getString("device")) + " " + obj.get("device") + " --" + obj.get("device_command");
//
//                    String result = bash.python3(commandDevice);
//                    commandsManager.sendMessage(messageModel.getRecipientNumber(), result);
//
//                }catch (JSONException e){
//                    commandsManager.sendMessage(messageModel.getRecipientNumber(), "Wrong command");
//                    System.out.println("wrong commandDevice" );
//                    e.printStackTrace();
//                }
//                break;

            default:
                commandsManager.sendMessage(messageModel.getRecipientNumber(), "Wrong command");
        }
    }
}