package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;
import com.dstarlab.bot.properties.application.ApplicationSettings;

import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.push.exceptions.EncapsulatedExceptions;

import java.io.IOException;

import static com.dstarlab.bot.properties.ConfigStrings.IOT_IS_PRIVATE_TEXT;
import static com.dstarlab.bot.properties.ConfigStrings.TEXT_UPDATE;

public class MessageController {

    public void processingBotCommand(MessageModel messageModel) {

        CommandsManager commandsManager = new CommandsManager(messageModel.getManager());

        if (!messageModel.isDecryptMessage()) {
            System.out.println("updateSecurityKeys");
//            commandsManager.updateSecurityKeys(messageModel);
        }

        System.out.println(
                "---" + messageModel.toString()
        );

        if (messageModel.getGroupModel().getType() != null
                && messageModel.getGroupModel().getType().equals(TEXT_UPDATE)) {
            commandsManager.sendSettingsIotMessage(messageModel);
            return;
        }
        if (messageModel.getDStarCommandModel() != null) {
            try {
                messageModel.getManager().sendRead(messageModel.getSignalServiceAddress(), messageModel.getTimestamp());
            } catch (IOException | UntrustedIdentityException e) {
                e.printStackTrace();
            }

            if (ApplicationSettings.INSTANCE.isWhiteListed(messageModel.getRecipientNumber())
                    || ApplicationSettings.INSTANCE.isAdminListed(messageModel.getRecipientNumber()) ||
                    ApplicationSettings.INSTANCE.isPublic()) {

                DSarCommand dSarCommand = new DStarCommandFactory().createCommand(messageModel.getDStarCommandModel().getCommandTyp());
                dSarCommand.processDStarCommand(messageModel);

//            } else if (ApplicationSettings.INSTANCE.isPublic()) {
//                DSarCommand dSarCommand = new DStarCommandFactory().createCommand(messageModel.getDStarCommandModel().getCommandTyp());
//                dSarCommand.processDStarCommand(messageModel);
//                if (dSarCommand != null) {
//                    messageModel.getManager().sendJsonSetupIotToNumber(messageModel.getRecipientNumber());
//                }

            } else {
                DSarCommand dSarCommand = new DStarCommandFactory().createCommand(messageModel.getDStarCommandModel().getCommandTyp());

                if (dSarCommand != null) {
                    messageModel.getManager().sendSettingsIotPermissionPanel(messageModel.getRecipientNumber());
                }
                try {
                    messageModel.getManager().sendMessage(IOT_IS_PRIVATE_TEXT, null, messageModel.getRecipientNumber());
                } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
                    encapsulatedExceptions.printStackTrace();
                }
            }
        } else {
            System.out.println("/* not a command */");
        }

    }

}