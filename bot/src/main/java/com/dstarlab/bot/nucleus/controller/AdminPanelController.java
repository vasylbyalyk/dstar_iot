package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.camera.cleaner.Cleaner;
import com.dstarlab.bot.database.managers.CameraManager;
import com.dstarlab.bot.nucleus.model.MessageModel;

import java.text.DecimalFormat;

import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST_ADD;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST_DELL;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_CLEAN_SPACE_BUTTON;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PRIVATE;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PUBLIC;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_REBOOT;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_TIMEZONE;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST_ADD;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST_DELL;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WIFI;

class AdminPanelController implements DSarCommand {

    @Override
    public void processDStarCommand(final MessageModel messageModel) {

        CommandsManager commandsManager = new CommandsManager(messageModel.getManager());

        switch (messageModel.getDStarCommandModel().getCommand()) {
            case ADMIN_PANEL_WHITE_LIST:
                commandsManager.sendWhiteListIot(messageModel.getRecipientNumber());
                break;
            case ADMIN_PANEL_WHITE_LIST_ADD:
                commandsManager.sendWhiteListAddIot(
                        messageModel.getDStarCommandModel().getTextField().trim(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_WHITE_LIST_DELL:
                commandsManager.sendWhiteListDellIot(
                        messageModel.getDStarCommandModel().getTextField(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_ADMIN_LIST:
                commandsManager.sendAdminListIot(messageModel.getRecipientNumber());
                break;
            case ADMIN_PANEL_ADMIN_LIST_ADD:
                commandsManager.sendAdminListAddIot(
                        messageModel.getDStarCommandModel().getTextField().trim(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_ADMIN_LIST_DELL:
                commandsManager.sendAdminListDellIot(
                        messageModel.getDStarCommandModel().getTextField(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_REBOOT:
                if (messageModel.getDStarCommandModel().getTextField().equals("Yes")) {
                    commandsManager.sendRebootIot();
                }
                break;
            case ADMIN_PANEL_IS_PUBLIC:
            case ADMIN_PANEL_IS_PRIVATE:
                commandsManager.changeIsPublic(
                        messageModel.getDStarCommandModel().getTextField(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_TIMEZONE:
                commandsManager.setTimezone(
                        messageModel.getDStarCommandModel().getTextField(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_WIFI:
                commandsManager.setWifi(
                        messageModel.getDStarCommandModel().getTextField(),
                        messageModel.getDStarCommandModel().getAdditional_text_field(),
                        messageModel.getRecipientNumber()
                );
                break;
            case ADMIN_PANEL_CLEAN_SPACE_BUTTON:
                    System.out.println("------------------" + commandsManager);
                Cleaner cleaner = new Cleaner(CameraManager.INSTANCE);
                DecimalFormat df = new DecimalFormat("0.000");
                String using = "Using space before: " + df.format(cleaner.getUsedSpace()) + "%\n";
                cleaner.allCleaning();
                using += "Using space after: " + df.format(cleaner.getUsedSpace()) + "%";

                commandsManager.sendMessage(messageModel.getRecipientNumber(), using);
                break;
            default:
                commandsManager.sendMessage(messageModel.getRecipientNumber(), "Wrong command");
        }
    }
}