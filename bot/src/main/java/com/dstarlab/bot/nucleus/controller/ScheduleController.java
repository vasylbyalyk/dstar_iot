package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;
import com.dstarlab.bot.nucleus.view.ScheduleView;

class ScheduleController {

    void execute(MessageModel messageModel) {
         new ScheduleView(messageModel);
    }

}
