package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.database.managers.CameraManager;
import com.dstarlab.bot.nucleus.SignalManager;
import com.dstarlab.bot.properties.config.ConfigSettings;
import com.dstarlab.bot.properties.management.CameraPanel;
import com.dstarlab.bot.properties.management.PanelIot;
import com.j256.ormlite.dao.Dao;

import org.whispersystems.signalservice.api.messages.dstar.IotMessage;
import org.whispersystems.signalservice.api.messages.dstar.SetupIotMessage;
import org.whispersystems.signalservice.api.push.exceptions.EncapsulatedExceptions;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public enum CameraStatusNotifier implements Dao.DaoObserver {

    INSTANCE;

    SignalManager manager;
    Set<String> currentUsers = new HashSet<>();
    HashSet<Integer> previousIndexes;
    HashSet<Integer> currentIndexes;
//    PanelIot currentPanel;

    @Override
    public void onChange() {

        try {
            CameraManager cameraManager = CameraManager.INSTANCE;
            currentIndexes = new HashSet<>(cameraManager.getActiveIndexes());

            if (currentIndexes.size() > previousIndexes.size()) {
                currentIndexes.removeAll(previousIndexes);
                manager.sendMessage("Camera " + currentIndexes + " has been connected", null, ConfigSettings.INSTANCE.getOwner());
            } else if (currentIndexes.size() < previousIndexes.size()) {
                previousIndexes.removeAll(currentIndexes);
                manager.sendMessage("Camera " + previousIndexes + " has been disconnected", null, ConfigSettings.INSTANCE.getOwner());
            }
            previousIndexes = new HashSet<>(cameraManager.getActiveIndexes());

        } catch (EncapsulatedExceptions
                | IOException encapsulatedExceptions) {
            encapsulatedExceptions.printStackTrace();
        }
        manager.sendJsonSetupIotToAll();

    }

    public void addToList(String user) {
        currentUsers.add(user);
    }

    public Set<String> getCurrentUsers() {
        return currentUsers;
    }

    public void setManager(SignalManager manager) {
        if (this.manager == null) {
            this.manager = manager;
            CameraManager cameraManager = null;
            cameraManager = CameraManager.INSTANCE;
            previousIndexes = new HashSet<>(cameraManager.getActiveIndexes());
        }
    }

    public void removeFromList(String number) {
        currentUsers.remove(number);
    }
}
