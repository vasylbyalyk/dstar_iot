package com.dstarlab.bot.nucleus;

import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.dstar.DstarMessage;

public interface IDstarMessageProcessor {
    public String handleDstarMessageWithoutFurtherProcessing(SignalServiceDataMessage message, DstarMessage dstarMessage);
}
