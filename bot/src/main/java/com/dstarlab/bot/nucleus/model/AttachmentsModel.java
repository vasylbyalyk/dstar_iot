package com.dstarlab.bot.nucleus.model;

import java.io.File;

public class AttachmentsModel {

    private long id;
    private int keyLength;
    private String Filename;
    private Integer size;
    private boolean voiceNote;
    private DimensionsModel dimensions;
    private File file;

    public AttachmentsModel() {
    }

    public AttachmentsModel(final long id,
                            final int keyLength,
                            final String filename,
                            final Integer size,
                            final boolean voiceNote,
                            final DimensionsModel dimensions,
                            final File file) {
        this.id = id;
        this.keyLength = keyLength;
        Filename = filename;
        this.size = size;
        this.voiceNote = voiceNote;
        this.dimensions = dimensions;
        this.file = file;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    public String getFilename() {
        return Filename;
    }

    public void setFilename(String filename) {
        Filename = filename;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean isVoiceNote() {
        return voiceNote;
    }

    public void setVoiceNote(boolean voiceNote) {
        this.voiceNote = voiceNote;
    }

    public DimensionsModel getDimensions() {
        return dimensions;
    }

    public void setDimensions(DimensionsModel dimensions) {
        this.dimensions = dimensions;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "AttachmentsModel{" +
                "id=" + id +
                ", keyLength=" + keyLength +
                ", Filename='" + Filename + '\'' +
                ", size=" + size +
                ", voiceNote=" + voiceNote +
                ", dimensions=" + dimensions +
                ", file=" + file +
                '}';
    }
}
