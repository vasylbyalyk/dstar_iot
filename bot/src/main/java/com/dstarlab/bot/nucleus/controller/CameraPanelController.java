package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;

class CameraPanelController implements DSarCommand{

    @Override
    public void processDStarCommand(final MessageModel messageModel) {

        new ScheduleController().execute(messageModel);

    }
}