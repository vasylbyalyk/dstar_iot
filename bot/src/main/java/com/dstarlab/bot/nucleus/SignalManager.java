package com.dstarlab.bot.nucleus;

import com.dstarlab.bot.database.managers.NumberType;

import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.dstar.DstarMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.EncapsulatedExceptions;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

import java.io.IOException;
import java.util.List;

public interface SignalManager {

    void sendMessage(String message, List<String> attachments, String recipient)
            throws EncapsulatedExceptions, IOException;

    void sendMessage(String message, List<String> attachments, String recipient, DstarMessage dstarMessage);

    boolean trustIdentityAllKeys(String name);

    void sendEndSessionMessage(List<String> recipients)
            throws IOException, EncapsulatedExceptions, InvalidNumberException;

    void sendTyping(SignalServiceAddress remoteAddress)
            throws IOException, UntrustedIdentityException;

    long sendGroupMessage(String messageText, List<String> attachments, byte[] groupId)
            throws IOException, EncapsulatedExceptions;

    void sendSettingsIotMessageReboot();

    void sendJsonSetupStartIot();

    void sendSignalGroupMessage(String message, List<String> attachments, byte[] groupId);

    void sendRead(SignalServiceAddress remoteAddress, long messageId) throws IOException, UntrustedIdentityException;

    void sendJsonSetupIotToNumber(String number);

    void sendJsonSetupIotToAll();

    void sendSettingsIotPermissionPanel(String recipientNumber);

    void sendAdminActionMessage(String s, NumberType type);

    void sendAdminActionMessageToNumber(String s, String number);

}
