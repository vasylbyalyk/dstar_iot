package com.dstarlab.bot.nucleus.model;

import java.util.List;

public class GroupModel {

    private String id = null;
    private String name = null;
    private String type = null;
    private List<String> member = null;
    private AttachmentsModel avatar = null;

    public GroupModel(){ }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getMember() {
        return member;
    }

    public void setMember(List<String> member) {
        this.member = member;
    }

    public AttachmentsModel getAvatar() {
        return avatar;
    }

    public void setAvatar(AttachmentsModel avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "GroupModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", member=" + member +
                ", avatar=" + avatar +
                '}';
    }
}
