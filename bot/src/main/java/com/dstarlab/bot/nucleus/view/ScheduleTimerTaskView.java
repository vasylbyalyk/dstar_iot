package com.dstarlab.bot.nucleus.view;

import com.dstarlab.bot.nucleus.controller.DStarCommandsController;
import com.dstarlab.bot.nucleus.model.MessageModel;

import java.util.Timer;
import java.util.TimerTask;

import static com.dstarlab.bot.properties.ConfigStrings.THREAD_NAME_STOP;

class ScheduleTimerTaskView extends TimerTask {

    private final DStarCommandsController dStarCommandsController;
    private final MessageModel messageModel;
    private final Timer timer;
    private int i = 0;
    private int count = 1;

    ScheduleTimerTaskView(Timer timer, MessageModel messageModel) {
        this.messageModel = messageModel;
        this.dStarCommandsController = new DStarCommandsController(messageModel.getManager());
        this.timer = timer;
        if (messageModel.getDStarCommandModel().getTime_start() != 0 && messageModel.getDStarCommandModel().getTime_end() != 0 && messageModel.getDStarCommandModel().getTimer() != 0) {
            count = (int) ((messageModel.getDStarCommandModel().getTime_end() - messageModel.getDStarCommandModel().getTime_start()) / messageModel.getDStarCommandModel().getTimer());
        }
    }

    @Override
    public void run() {
        i++;
        if (Thread.currentThread().getName().equals(THREAD_NAME_STOP + Thread.currentThread().getId())) {
            i = count;
        } else {
            System.out.println("ScheduleTimerTaskView");
            dStarCommandsController.buildMessageForSend(messageModel, messageModel.getDStarCommandModel().getCommand());

        }
        if ((i >= count)) {
            timer.cancel();
            timer.purge();

        }
    }
}
