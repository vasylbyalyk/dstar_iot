package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.camera.Recorder;
import com.dstarlab.bot.controller.media.ImagesFilesController;
import com.dstarlab.bot.controller.media.VideoFilesController;
import com.dstarlab.bot.nucleus.SignalManager;
import com.dstarlab.bot.nucleus.model.MessageModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_ALL;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_SCHEDULE;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_TEXT_PHOTO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_TEXT_PHOTO_ALL;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_TEXT_PHOTO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_TEXT_PHOTO_SCHEDULE;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_TEXT_VIDEO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_RECORD;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_SCHEDULE;
import static com.dstarlab.bot.properties.ConfigStrings.PATTERN_TIME;

public class DStarCommandsController {
    private static final Logger logger = LoggerFactory.getLogger(Recorder.class);

    private CommandsManager commands;

    public DStarCommandsController(SignalManager manager) {
        commands = new CommandsManager(manager);
    }

    public void buildMessageForSend(final MessageModel messages, final String iotCommand) {
        String fileName;

        switch (iotCommand) {
            case COMMAND_PHOTO_ALL:
                ImagesFilesController imagesFilesController = new ImagesFilesController();
                ArrayList<String> filePaths = imagesFilesController.getImageFileFromAll();
                ArrayList<String> validFilePaths = new ArrayList<>(filePaths);
                for (String filePath : filePaths) {
                    if (!filePath.contains(".jpg")) {
                        commands.sendMessage(messages.getRecipientNumber(),
                                "Camera " + filePath + " is not responding.");
                        validFilePaths.remove(filePath);
                    }
                }
                commands.attachmentSend(messages, createMessageForAttachment(messages), validFilePaths);
                imagesFilesController.deleteImage(filePaths);
                break;
            case COMMAND_PHOTO_EVERY:
            case COMMAND_PHOTO:
            case COMMAND_PHOTO_SCHEDULE:
                long overallStartTime = System.currentTimeMillis();

                imagesFilesController = new ImagesFilesController();

                long photoStartTime = System.currentTimeMillis();
                fileName = imagesFilesController.getImageFile(messages.getDStarCommandModel().getCameraId());
                logger.info("Overall time to TAKE the PHOTO: " + (System.currentTimeMillis() - photoStartTime));

                long sendMessageStartTime = System.currentTimeMillis();
                if (!new File(fileName).exists()) {
                    commands.sendMessage(messages.getRecipientNumber(),
                            "Camera " + fileName + " is not responding.");
                    break;
                }
                commands.attachmentSend(messages, createMessageForAttachment(messages), fileName);
                logger.info("Overall time to SEND the PHOTO: " + (System.currentTimeMillis() - sendMessageStartTime));

                imagesFilesController.deleteImage(fileName);
                logger.info("Overall time to TAKE + SEND the PHOTO: " + (System.currentTimeMillis() - overallStartTime));
                break;
            case COMMAND_VIDEO:
            case COMMAND_VIDEO_EVERY:
            case COMMAND_VIDEO_SCHEDULE:
            case COMMAND_VIDEO_RECORD:
                SendAttachmentVideoMessage(messages);
                break;
            default:
                imagesFilesController = new ImagesFilesController();
                fileName = imagesFilesController.getImageFile(messages.getDStarCommandModel().getCameraId());
                String textSms =
                        "You sent a request with a very bad command, but we sent you a photo so as not to upset you.";
                commands.attachmentSend(messages, textSms, fileName);
                imagesFilesController.deleteImage(fileName);
                break;
        }
    }

    private void SendAttachmentVideoMessage(MessageModel messageModel) {
        VideoFilesController videoFilesController = new VideoFilesController(messageModel.getDStarCommandModel().getCameraId());

        logger.info("getCameraId(): " + messageModel.getDStarCommandModel().getCameraId());
        logger.info("getPathCameraId(): " + videoFilesController.getPathCameraId());

        logger.info(String.valueOf(messageModel.getDStarCommandModel().getDuration() <= 10000
                && messageModel.getDStarCommandModel().getTime_start() == 0));
        if (messageModel.getDStarCommandModel().getDuration() <= 10000
                && messageModel.getDStarCommandModel().getTime_start() == 0) {
            String fileName = videoFilesController.getLastVideo();
            logger.info("fileName: " + fileName);
            commands.attachmentSend(messageModel, createMessageForAttachment(messageModel), fileName);
        } else {
            long timeStart;
            if (messageModel.getDStarCommandModel().getCommand().equals(COMMAND_VIDEO_EVERY)){
                timeStart = System.currentTimeMillis() - messageModel.getDStarCommandModel().getDuration();
            }else {
                timeStart = messageModel.getDStarCommandModel().getTime_start();
            }

            String fileAttachmentPath = videoFilesController.getFilesListToDuration(
                    timeStart,
                    messageModel.getDStarCommandModel().getDuration());
            logger.info("fileAttachmentPath: " + fileAttachmentPath);
            commands.attachmentSend(messageModel, createMessageForAttachment(messageModel), fileAttachmentPath);
            videoFilesController.deleteVideo(fileAttachmentPath);
        }
    }

    private String createMessageForAttachment(final MessageModel messageModel) {
        final String message;
        if (messageModel.getDStarCommandModel() != null) {
//            final SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_DATA);

            message = createMessageForSchedule(messageModel);

        } else {
            message = messageModel.getMassage();
        }
        return message;
    }

    private String createMessageForSchedule(final MessageModel messageModel) {

        String message = "";
        switch (messageModel.getDStarCommandModel().getCommand()) {
            case COMMAND_PHOTO_ALL:
                message = COMMAND_TEXT_PHOTO_ALL;
                break;
            case COMMAND_PHOTO_EVERY:
                message = COMMAND_TEXT_PHOTO_EVERY;
                break;
            case COMMAND_PHOTO:
                message = COMMAND_TEXT_PHOTO;
                break;
            case COMMAND_PHOTO_SCHEDULE:
                message = COMMAND_TEXT_PHOTO_SCHEDULE;
                break;
            case COMMAND_VIDEO:
            case COMMAND_VIDEO_EVERY:
            case COMMAND_VIDEO_SCHEDULE:
            case COMMAND_VIDEO_RECORD:
                message = COMMAND_TEXT_VIDEO;
                break;
            default:
                break;
        }

//        message += messageModel.getDStarCommandModel().getCommand() + ", camera: " + messageModel.getDStarCommandModel().getCameraId() + ",\n";
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_TIME);
        if (messageModel.getDStarCommandModel().getTime_start() != 0 && messageModel.getDStarCommandModel().getDuration() >= 10000)
            message += "\nStart at: " + dateFormat.format(messageModel.getDStarCommandModel().getTime_start()) + ".";
        if (messageModel.getDStarCommandModel().getTime_end() != 0)
            message += "\nFinish at: " + dateFormat.format(messageModel.getDStarCommandModel().getTime_end()) + ".";
        if (messageModel.getDStarCommandModel().getDuration() >= 10000 && !messageModel.getDStarCommandModel().getCommand().contains(COMMAND_PHOTO))
            message += "\nDuration: " + (buildTimeSecond(messageModel.getDStarCommandModel().getDuration() / 1000));
        if (messageModel.getDStarCommandModel().getTimer() >= 10000 && messageModel.getDStarCommandModel().getTime_end() != 0)
            message += "\nInterval: " + (buildTimeSecond(messageModel.getDStarCommandModel().getTimer() / 1000));

        if (messageModel.getGroupModel().getId() != null) {
            message += "\nTo: " + messageModel.getRecipientNumber();
        }
        return message;
    }

    private String buildTimeSecond(long second) {
        long min = second / 60;
        long sec = second % 60;
        String t = "";
        if (min > 0)
            t += min + " min ";
        if (sec > 0)
            t += sec + " sec ";
        return t + " \n";
    }

    void getCommandSendHello(final MessageModel messages) {
        buildMessageForSend(messages, COMMAND_PHOTO);
    }

    void getCommandSendHi(final MessageModel messages) {

        if (messages.getGroupModel().getId() != null) {
            commands.sendGroupMessage(messages.getGroupModel().getId(), messages);
        } else {
            commands.sendMessage(messages.getRecipientNumber(), "Hi");
        }
    }

    void getCommandSendVideo(final MessageModel messages) {
        buildMessageForSend(messages, COMMAND_VIDEO);
    }

}
