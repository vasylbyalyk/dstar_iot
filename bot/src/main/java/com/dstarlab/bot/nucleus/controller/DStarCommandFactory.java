package com.dstarlab.bot.nucleus.controller;

class DStarCommandFactory {
    public DSarCommand createCommand(String command){
        DSarCommand dSarCommand = null;
        switch (command){
            case "Camera":
                dSarCommand = new CameraPanelController();
                break;
            case "Device":
                dSarCommand = new DevicePanelController();
                break;
            case "Admin":
                dSarCommand = new AdminPanelController();
                break;
            case "MainPanel":
                dSarCommand = new MainPanelController();
                break;
            case "TextCommand":
                dSarCommand =  new TextCommandController();
                break;

            default:
                break;
        }
        return dSarCommand;
    }
}
