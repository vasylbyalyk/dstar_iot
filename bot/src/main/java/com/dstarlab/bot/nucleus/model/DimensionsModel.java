package com.dstarlab.bot.nucleus.model;

public class DimensionsModel {

    private int width;
    private int height;

    public DimensionsModel() {
    }

    public DimensionsModel(final int width,
                           final int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "DimensionsModel{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
