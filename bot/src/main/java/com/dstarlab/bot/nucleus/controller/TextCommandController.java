package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;

import static com.dstarlab.bot.properties.ConfigStrings.*;

class TextCommandController implements DSarCommand {

    @Override
    public void processDStarCommand(MessageModel messageModel) {
        System.out.println("------processDStarCommand");
        final DStarCommandsController dStarCommandsController = new DStarCommandsController(messageModel.getManager());
       switch (messageModel.getMassage().toUpperCase()) {
                case TEXT_UPDATE:
                    System.out.println("------ : " + TEXT_UPDATE);
                    messageModel.getManager().sendJsonSetupStartIot();
                    break;
//                case TEXT_HELLO:
//                    System.out.println("------ : " + TEXT_HELLO);
//                    dStarCommandsController.getCommandSendHello(messageModel);
//                    break;
//                case TEXT_VIDEO:
//                    System.out.println("------ : " + TEXT_VIDEO);
//                    dStarCommandsController.getCommandSendVideo(messageModel);
//                    break;
                case TEXT_HI:
                    System.out.println("------ : " + TEXT_HI);
                    dStarCommandsController.getCommandSendHi(messageModel);
                    break;
                default:
                    System.out.println("Message not contain command");
            }
    }
}