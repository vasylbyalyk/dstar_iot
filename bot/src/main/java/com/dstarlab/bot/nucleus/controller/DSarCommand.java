package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.nucleus.model.MessageModel;

interface DSarCommand {
    void processDStarCommand(MessageModel messageModel);
}
