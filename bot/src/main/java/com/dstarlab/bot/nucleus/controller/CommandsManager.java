package com.dstarlab.bot.nucleus.controller;

import com.dstarlab.bot.controller.Wifi;
import com.dstarlab.bot.controller.bash.BashConsoleController;
import com.dstarlab.bot.controller.system.TimezoneController;
import com.dstarlab.bot.controller.system.WifiController;
import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.nucleus.SignalManager;
import com.dstarlab.bot.nucleus.model.MessageModel;
import com.dstarlab.bot.properties.application.ApplicationSettings;
import com.dstarlab.bot.properties.config.ConfigSettings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.whispersystems.signalservice.api.messages.dstar.DstarMessage;
import org.whispersystems.signalservice.api.push.exceptions.EncapsulatedExceptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class CommandsManager {

    private static final Logger logger = LoggerFactory.getLogger(CommandsManager.class);

    private SignalManager manager;
//    private MainSignal signal;

    public CommandsManager(SignalManager manager) {
        this.manager = manager;
    }

//    public void sendEndSessionMessage(final String recipientNumber) throws IOException, EncapsulatedExceptions {
//        manager.sendEndSessionMessage(Collections.singletonList(recipientNumber));
//        SignalServiceDataMessage.Builder messageBuilder = SignalServiceDataMessage.newBuilder()
//                .asEndSessionMessage();
//        try {
//            manager.sendMessageLegacy(messageBuilder, recipientNumber);
//        } catch (IOException | EncapsulatedExceptions e) {
//            e.printStackTrace();
//        }
//    }

//    private boolean trustIdentityAllKeys(final String recipientNumber) {
//        return manager.trustIdentityAllKeys(recipientNumber);
//    }

    //    void updateSecurityKeys(final MessageModel messageModel) {
//
//        List<String> list = new ArrayList<>();
//        list.add(messageModel.getRecipientNumber());
//
//        try {
//            manager.sendEndSessionMessage(list);
//        } catch (IOException | EncapsulatedExceptions | InvalidNumberException e) {
//            e.printStackTrace();
//        }
//        try {
//            manager.sendTyping(messageModel.getSignalServiceAddress());
//        } catch (IOException | UntrustedIdentityException e) {
//            e.printStackTrace();
//        }
//
//        boolean b = trustIdentityAllKeys(messageModel.getRecipientNumber());
//        System.out.println(b);
//    }
    public void sendMessage(String message, String attachment, String recipient, DstarMessage dstarMessage) {
        logger.info("``````sendMessage");
        List<String> attachments = new ArrayList<>(1);
        attachments.add(attachment);

        manager.sendMessage(message, attachments, recipient, dstarMessage);

    }

    public void sendMessage(final String recipientNumber, final String message) {
        try {
            manager.sendMessage(message, null, recipientNumber);
        } catch (IOException | EncapsulatedExceptions e) {
            logger.info("Do not send -" + recipientNumber);
            e.printStackTrace();
        }
    }

//    void sendMessageGroup(final String groupId,
//                          final String message) {
//        try {
////            manager.sendGroupMessageBot(message, null, Base64.getDecoder().decode(groupId));
//
//            manager.sendGroupMessage(message, null, Base64.getDecoder().decode(groupId));
//        } catch (IOException | EncapsulatedExceptions e) {
//            e.printStackTrace();
//        }
//
//    }

//    void sendMessage(String recipient, DstarMessage dstarMessage) {
//        manager.sendMessage("", null, recipient, dstarMessage);
//    }

    void attachmentSend(
            final MessageModel recipient,
            final String message,
            final String fileAttachment) {

        if (recipient.getGroupModel().getId() == null) {
            logger.info("attachmentSend Private");
            sendAttachment(recipient.getRecipientNumber(), message, fileAttachment);
        } else {
            logger.info("attachmentSend Group");
            ArrayList<String> list = new ArrayList<>();
            list.add(fileAttachment);
            sendGroupAttachment(recipient.getGroupModel().getId(), message, list);
        }

    }

    void attachmentSend(
            final MessageModel recipient,
            final String message,
            final ArrayList<String> fileAttachment) {

        if (recipient.getGroupModel().getId() == null) {
            logger.info("attachmentSend Private");
            sendAttachment(recipient.getRecipientNumber(), message, fileAttachment);
        } else {
            logger.info("attachmentSend Group");
            sendGroupAttachment(recipient.getGroupModel().getId(), message, fileAttachment);
        }
    }

    private void sendAttachment(
            final String recipientNumber,
            final String message,
            final String fileAttachment) {
        try {
            manager.sendMessage(message, Collections.singletonList(fileAttachment), recipientNumber);
        } catch (IOException | EncapsulatedExceptions e) {
            logger.info("Do not send - " + recipientNumber);
            e.printStackTrace();
        }
    }

    private void sendAttachment(
            final String recipientNumber,
            final String message,
            final List<String> fileAttachment) {
        try {
            manager.sendMessage(message, fileAttachment, recipientNumber);
        } catch (IOException | EncapsulatedExceptions e) {
            logger.info("Was not send to - " + recipientNumber);

            try {
                manager.sendMessage("Something went wrong, try again.", null, recipientNumber);
            } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
                encapsulatedExceptions.printStackTrace();
            }

            e.printStackTrace();
        }
    }

//    private void sendGroupMessage(final String groupId, final String message) {
//        manager.sendSignalGroupMessage(message, null, Base64.getDecoder().decode(groupId));
//    }

    void sendSettingsIotMessage(MessageModel messageModel) {

        if (messageModel.getGroupModel().getType() != null) {
            final List<String> membersSend = new ArrayList<>(messageModel.getGroupModel().getMember());

//            HashSet<String> stringWhiteList = ApplicationSettings.INSTANCE.getWhiteList();

            for (String number : membersSend) {
                messageModel.getManager().sendJsonSetupIotToNumber(number);
            }

        } else {
            messageModel.getManager().sendJsonSetupIotToNumber(messageModel.getRecipientNumber());
        }

    }

    public void changeIsPublic(String s, String recipientNumber) {
        if (s.equals("Yes")) {
            ApplicationSettings.INSTANCE.setPublic(true);
            if (!ApplicationSettings.INSTANCE.isPublic()) {
                manager.sendAdminActionMessage("Hello! User " + recipientNumber +" changed the IoT Bot type to Public. Now you can send commands to it.", NumberType.USER_LIST);
            }
            else {
                manager.sendAdminActionMessage("Hello! User " + recipientNumber + " changed the IoT Bot type to Public. Now you cannot send commands to it.", NumberType.USER_LIST);
            }
            manager.sendJsonSetupIotToAll();
        }
    }

    public void sendRebootIot() {
        manager.sendSettingsIotMessageReboot();
        new BashConsoleController().reboot("sudo /sbin/reboot ");
    }

    public void setWifi(String ssid, String psk, String recipientNumber) {
        try {
            manager.sendMessage(new WifiController().setNewWIFI(new Wifi(ssid, psk)), null, recipientNumber);
        } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
            encapsulatedExceptions.printStackTrace();
        }
    }

    public void setTimezone(String timezone, String recipientNumber) {
        System.out.println(recipientNumber);
        try {
            manager.sendMessage(new TimezoneController().setTimezone(timezone), null, recipientNumber);
        } catch (EncapsulatedExceptions | IOException encapsulatedExceptions) {
            encapsulatedExceptions.printStackTrace();
        }
    }

    private String convertHashSetToString(HashSet<String> hashSet) {
        StringBuilder result = new StringBuilder();
        for (String item : hashSet) {
            result.append("\n").append(item);
        }
        return result.toString();
    }

    public void sendWhiteListIot(String number) {
        if (!ApplicationSettings.INSTANCE.getWhiteList().isEmpty())
            sendMessage(number, "User from whitelist: " + convertHashSetToString(ApplicationSettings.INSTANCE.getWhiteList()));
        else
            sendMessage(number, "Whitelist is empty");
    }

    //    void sendCameraPanel(String number) {
//
//        PanelIot panel = new CameraPanel();
//
//        CameraStatusNotifier cameraStatusNotifier = CameraStatusNotifier.INSTANCE;
//        CameraManager cameraManager = CameraManager.INSTANCE;
//        cameraManager.registerObserver(cameraStatusNotifier);
//        cameraStatusNotifier.addToList(number);
//        cameraStatusNotifier.setManager(manager);
//
//        IotMessage iotMessage = new SetupIotMessage(
//                ConfigSettings.INSTANCE.getNumber(),
//                panel.sendPanelIot(number));
//
//        manager.sendMessage("", null, number, iotMessage);
//    }
    public void sendAdminListAddIot(String number, String recipientNumber) {
        if (number.matches("(\\D+)(\\d{11})") || number.matches("(\\D+)(\\d{12})") || number.matches("(\\D+)(\\d{15})")) {
            if (number.equals(ConfigSettings.INSTANCE.getNumber())) {
                sendMessage(recipientNumber, "Controller number cannot be added.");
                return;
            } else if (ApplicationSettings.INSTANCE.isAdminListed(number)) {
                sendMessage(ConfigSettings.INSTANCE.getOwner(), "Admin number cannot be added.");
                return;
            } else if (ApplicationSettings.INSTANCE.isWhiteListed(number)) {
                ApplicationSettings.INSTANCE.removeFromWhiteList(number);
            } else {
                ApplicationSettings.INSTANCE.addToAdminList(number);
                manager.sendJsonSetupIotToNumber(recipientNumber);
                manager.sendJsonSetupIotToNumber(number);
                manager.sendAdminActionMessageToNumber("Hello! User " + recipientNumber + " added your number to the Administrator List. It allows you to send commands to the IoT Bot and change any settings.", number);
            }
        } else {
            sendMessage(recipientNumber, "Invalid phone number format");
        }
        sendAdminListIot(recipientNumber);
    }

    public void sendAdminListIot(String number) {
        if (!ApplicationSettings.INSTANCE.getAdminList().isEmpty())
            sendMessage(number, "User from admin list: " + convertHashSetToString(ApplicationSettings.INSTANCE.getAdminList()));
        else
            sendMessage(number, "Admin list is empty");

    }

    public void sendWhiteListAddIot(String number, String recipientNumber) {
        if (number.matches("(\\D+)(\\d{12})") || number.matches("(\\D+)(\\d{15})")) {
            if (number.equals(ConfigSettings.INSTANCE.getNumber())) {
                sendMessage(recipientNumber, "Controller number cannot be added.");
                return;
            } else if (ApplicationSettings.INSTANCE.isAdminListed(number)) {
                sendMessage(recipientNumber, "Admin number cannot be added.");
                return;
            } else {
                ApplicationSettings.INSTANCE.addToWhiteList(number);
                manager.sendJsonSetupIotToNumber(recipientNumber);
                manager.sendJsonSetupIotToNumber(number);
                manager.sendAdminActionMessageToNumber("Hello! User " + recipientNumber + " added your number to the White List. It allows you to send commands to the IoT Bot.", number);
            }
        } else {
            sendMessage(ConfigSettings.INSTANCE.getOwner(), "Invalid phone number format");
        }
        sendWhiteListIot(recipientNumber);
    }

    public void sendWhiteListDellIot(String number, String recipientNumber) {
        ApplicationSettings.INSTANCE.removeFromWhiteList(number);
        manager.sendAdminActionMessageToNumber("Hello! User " + recipientNumber + " deleted your number from the White List. You can no longer send commands to the IoT Bot.", number);
        manager.sendJsonSetupIotToNumber(recipientNumber);
        manager.sendJsonSetupIotToNumber(number);
        sendWhiteListIot(recipientNumber);
    }

    public void sendAdminListDellIot(String number, String recipientNumber) {

//        ApplicationSettings.INSTANCE.removeFromBlackList(number);
        ApplicationSettings.INSTANCE.removeFromAdminList(number);
        manager.sendAdminActionMessageToNumber("Hello! User " + recipientNumber + " deleted your number from the Admin List. You can no longer send commands to the IoT Bot and change any settings.", number);
        manager.sendJsonSetupIotToNumber(recipientNumber);
        manager.sendJsonSetupIotToNumber(number);
        sendAdminListIot(recipientNumber);
    }

    private void sendGroupAttachment(
            final String groupId,
            final String message,
            final ArrayList<String> fileAttachment) {
        try {
            long res = manager.sendGroupMessage(message, fileAttachment, Base64.getDecoder().decode(groupId));
        } catch (IOException | EncapsulatedExceptions e) {
            e.printStackTrace();
        }
    }

    void sendGroupMessage(final String id, final MessageModel messages) {
        manager.sendSignalGroupMessage("hi group", null, Base64.getDecoder().decode(id));
    }

//    public void sendJsonSetupStartIot() {
//        manager.sendJsonSetupStartIot();
//    }
//
//    private void sendGroupAttachment(
//            final String groupId,
//            final String message,
//            final List<String> fileAttachment) {
//
//        signal.sendGroupMessage(message, fileAttachment, Base64.getDecoder().decode(groupId));
//    }
}
