package com.dstarlab.bot.properties.config;

public enum ConfigSettings {

    INSTANCE;

    ConfigInterface configInterface = new ConfigDatabase();
    // ConfigInterface configInterface = ConfigFile.INSTANCE;

    public void setApplicationInterface(ConfigInterface configInterface) {
        this.configInterface = configInterface;
    }

    public String getNumber() {
        return configInterface.getNumber();
    }

    public String getOwner() {
        return configInterface.getOwner();
    }

    public String getVerify() {
        return configInterface.getVerify();
    }
}
