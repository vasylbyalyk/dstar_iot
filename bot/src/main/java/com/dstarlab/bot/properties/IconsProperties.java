package com.dstarlab.bot.properties;

import com.dstarlab.bot.Main;
import com.dstarlab.bot.database.managers.IconManager;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class IconsProperties implements FilenameFilter {
    private IconManager iconManager = null;
    private String str;

    public IconsProperties() {
        iconManager = new IconManager();
        str = ".png";
    }

    public void setIcons(){

        iconManager.addIcon("default", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAfCAYAAACCox+xAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAc6SURBVHgBxVd7cFTVGf/du3v37m6SzWNNiOTBIiSAJpliYEqaAgmaOkzbwdYJKoJ1WrWUoTO0/7Qd2xodR9upTqcdR2dQHBUdHaMy6IwCOgoqBkE0RojREJ6CMTHZzWOz73v8nbO7uCQBEkfHk/ly7557zvf9vt/5HvdqmMJouEU4+4Jwx+3QnXF4O1q1T/EdDw3THAvXiFm6iSbDhdC+h7Sn8R2NaQOBEBpWQ68vxwq7A3V7tuNfat4FgRxKAyy08ApNTEetPuWVEkCDsKMCDhyFufc1vKu7YDauwUYkYGIUTgzy+ihlLtfItXLPuFGzTmR9ayDzbxbemltxTZENXvrrRD+NReHsaccWZw7+dGkF8smBiQE+0ymAuaAAJbW/QxNaCChjZIW5btpA6NHlzaK87HJ8UHkVXq1egX0uLzzcZSII8+R+RBgrJyuWYCnGOCfnbXDMX4Qq33J0+VZgZyPQpthBkp22Vm2QrBRNAwg3LoLd4UF13iyU5/oAXn0zS3k4WtKgNKzZEXB7MD8NQj7zFOLagjlw5nGPpxSLfMUoRS3OMqMnMHPqQBqoFjC+PIZDgWM4NXQMCJxAd89BHOcuQ4HR4TDc8Jo5mCXv1VwUjoHjeNN/FIP+48Dw53jj+D4EpK4kM8rFCYzYJwXRInQ8zmf8+2IEQf+TWFZ2BWpPfIzDhGcgxv8COr11O7JRY3PATRAOxBlBNug9h9ET+AorvaUo+Ww/9hGeHUN8NgBr8W1iIdeNTo2Rl6nOzc0OShT2cBRW9360R6N8FicQCYYMLL8dN9pNuMxsVNffgjrFiq4AGQO9GPrsPXyk3KEOhCluddTXHniMcXNRIJKNAZoKUyJJVqg4eU1Q5LHQ0KLVqPL68AednOoGMOcnaKlcinIFNLXm7N4YxQlbYzP+zNoTnKx6TZyS58h6UFiAHGcesk59RFVJ2qW3RvEc5C/5LW7IK8U6XWcZSw1hAVYMvWc6sfm1/5BTaT5BsRCbfSVclcvxR66p2LUVP8c8utmqJc4LpPZ2YeTPwSbTjVX2LJQwEGfYDHWeXZYFP1O1mMdQpfHgND1jt/gGjKD6eAT90VF0x6Lwc0kpAVWM+fHU/m24e+A0hpFNIAe12KRAlt0hymh0k65hx4EdODjcpwLZrF+HxTOrcKs7HyvlMUgAaTlnCFl2kkCshGIHiSjCw714onsvnuzahZM8nigthrk6giNaZAKQZRtEWcyFn7Y9gGfhU5XPqVIxmY4qTVfdi43ecqwnG0izoaU1pEGIFCtxxcrgxzuxvv15dPJ4IjzYJAjBe53XI/yd0Y+UX6MC+W0PaM/gAmP7Hdg8OoiXzvFAy/ihZdDLm74e/K/9BZrLWE5AmpL4xNjU594kPOEQ+s6dhbi0DAXfkJ4c7z+LzSLj9/kG2Tiy817soDmR2i+cTPqSy5ArdfPQJ3RnPTcLoc7Htd6zM1y07Aa01t6Izsa1eJl5byo/dFhH96KPRnrOohMZUDMQBgewR+2JK/9FcQVK6tdiz5XX4dPla3Anj2mCM/ovZyIjjTSxsA7V3go0zagBLrkCNbNrsECCUImoIxEP47RIG07HxDhAY0NkmGvTDsy4DEuLqlBWRJ2eEmwqLCKUccPe0qJZmRMRvm2EhzAUGUZuJIBYOIDTaRAUXZdtXhpPZ42WwYiVFIdJFhP8s6lKkogG0R3y02umQXQMPf2FCE0AMn6icwv8zg2oG/kSNwfO4J0Th5h2sqvaaFKDbrNjniWhWxgJ+vF6ZASnNA2G04NKZzbqCcjpzMWPCHoLYci6Gv9kL97ms99kF8I31I+taIU13u7kr4qp6sqe6Ui1ewc9M37xD6wumI37ggFseesRPNHfxXItUjoYCwuaUFy9EhtMF64+sA3XqNoR505ZPxKUCGUeZbcWnxoQ2ajnpkCMUgwYhRXIuXoj3hgbxNPb72JvtlQH1jKBqJjgcfz6Htxls7Gc/x+rh76ghhj/PLJuUDp5n8oY+drYsVULyvvzvI9w4VpuiFBcSk2s9jo08aw7tt+Nh2kuqoqUvMoCJVL3lvI89sq/8bdICO0rbsOLP27GbGRRQ5TSSX6ou7ZWGHzzK2bgxy7CSGo0CxsOkA8Txqq/4MHQMF7Z9V+8RXU2Gb55ZcivXIybEnEMHN6N58LDqQBN9unEz36Pq9g2ro+FMBJjwDJQT1ijOBoKIUAmDmWasl8QiOyQzWTxQyZDjJ24my85cfrmSuZQTSPuLFmCX1n005GFS9q24X7mgwxEmWGJXfezC+dgm2L3Hs26kKkLA0mDYaVgIetjljCpFc2yPupGDmaYuckmx6wpUkeULfMpxcoCwt7Na8vFv3Gm+F2jWYyPl3zz2dKLUt0TCDO9/97bga6+Drw/fAb3yTmGdpgAInyzTWXH9D60Ljoa+H3S8E9xffKX/DSQJW1Smf7X43RH3V/F3Jr1E79JfpCxZL3w4XsYXwP2zdmTM4d4WwAAAABJRU5ErkJggg==");
    }

    public void setIcons(String key, String iconB64){
        iconManager.addIcon(key,iconB64);
    }
    public HashMap<String, String> getIconList(){
        return iconManager.getIconList();
    }
    public JSONObject getIconsJson(){
        JSONObject iconsJson = new JSONObject();
        HashMap<String, String> listIcon = getIconList();
        for (Map.Entry<String, String> entry : listIcon.entrySet()){
            iconsJson.put(entry.getKey(),entry.getValue());
        }

        return iconsJson;
    }
    public void generateIcons() {


        File dir = new File(Main.getUserDirPath() + "icons");
        FilenameFilter filter = new  IconsProperties();
        if(dir.isDirectory()){
            String[] list = dir.list(filter);

            assert list != null;
            for (String nameIcon : list){
               File fileIcon = new File(dir + "/" + nameIcon);
               String keyIcon = fileIcon.getName();

                String b64Icon = "";

                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(fileIcon);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                byte[] byteArray = new byte[(int)fileIcon.length()];
                try {
                    assert fis != null;
                    fis.read(byteArray);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                b64Icon = Base64.encodeBase64String(byteArray);
                setIcons(keyIcon.substring(0, keyIcon.length() - 4), b64Icon);
            }
        }

    }

    @Override
    public boolean accept(final File dir, final String name) {
        return name.endsWith(str);
    }
}
