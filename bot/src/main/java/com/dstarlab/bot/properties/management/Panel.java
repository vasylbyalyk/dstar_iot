package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.camera.Recorder;
import com.dstarlab.bot.database.managers.CameraManager;
import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.nucleus.SignalManager;
import com.dstarlab.bot.nucleus.controller.CameraStatusNotifier;
import com.dstarlab.bot.properties.IconsProperties;
import com.dstarlab.bot.properties.VocabularyProperties;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Panel implements PanelIot {
    private static final Logger logger = LoggerFactory.getLogger(Recorder.class);

    private JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject panelJson = jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();

    @Override
    public String sendPanelIot(NumberType numberType) {
//        logger.info("\n\n\n\n------sendPanelIot");
        ArrayList<JSONObject> mainPanel = new MainPanel().getPanelCommand(numberType);
        if (!mainPanel.isEmpty())
            panelJson.put("main_panel", mainPanel);

        ArrayList<JSONObject> cameraPanel = new CameraPanel().getPanelCommand(numberType);
        if (!cameraPanel.isEmpty())
            panelJson.put("camera_panel", cameraPanel);

        ArrayList<JSONObject> adminPanel = new AdminPanel().getPanelCommand(numberType);
        if (!adminPanel.isEmpty())
            panelJson.put("admin_panel", adminPanel);

        ArrayList<JSONObject> permissionPanel = new PermissionPanel().getPanelCommand(numberType);
        if (!permissionPanel.isEmpty())
            panelJson.put("permission_panel", permissionPanel);

        JSONObject panel = new JSONObject();

        panel.put("vocabulary", new JSONObject(VocabularyProperties.INSTANCE.getVocabulary()));
        panel.put("panels", panelJson);
        panel.put("icons", new IconsProperties().getIconsJson());
        
        logger.info("\n\n\n------panel: " + panel.toString() + "\n\n\n");

        return panel.toString();
    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        buildPanel();
        return commandArray;
    }


    @Override
    public void buildPanel() {
        JSONObject panel = new JSONObject();
    }

    public void cameraNotifier(SignalManager manager){
         CameraStatusNotifier cameraStatusNotifier = CameraStatusNotifier.INSTANCE;
        CameraManager cameraManager = CameraManager.INSTANCE;
        cameraManager.registerObserver(cameraStatusNotifier);
        cameraStatusNotifier.setManager(manager);
    }
}
