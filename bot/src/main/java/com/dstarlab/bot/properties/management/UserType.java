package com.dstarlab.bot.properties.management;

public enum UserType {
    USER, USER_WT, ADMIN
}
