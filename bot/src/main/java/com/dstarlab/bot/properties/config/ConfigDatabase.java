package com.dstarlab.bot.properties.config;

import com.dstarlab.bot.database.managers.BotManager;

public class ConfigDatabase implements ConfigInterface {

    public String getNumber() {
        return BotManager.getInstance().getNumber();
    }

    public String getOwner() {
        return BotManager.getInstance().getOwner();
    }

    public String getVerify() {
        return BotManager.getInstance().getVerificationKey();
    }

}