package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.controller.system.TimezoneController;
import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.application.ApplicationSettings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST_ADD;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_ADMIN_LIST_DELL;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_CLEAN_SPACE_BUTTON;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PRIVATE;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PRIVATE_BOT;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PUBLIC;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_IS_PUBLIC_BOT;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_REBOOT;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_TIMEZONE;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST_ADD;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WHITE_LIST_DELL;
import static com.dstarlab.bot.properties.ConfigStrings.ADMIN_PANEL_WIFI;

public class AdminPanel implements PanelIot {

    private JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject adminPanelJson = jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();
    private ButtonToPanel buttonPanel = null;

    @Override
    public String sendPanelIot(NumberType numberType) {
        buttonPanel = new ButtonToPanel("Admin", numberType);
        
        if (numberType != NumberType.ADMIN)
            return new MainPanel().sendPanelIot(numberType);
        
        buildPanel();
        jsonBuilder.buildPanelExit(commandArray);

        adminPanelJson.put("admin_panel", commandArray);


        return adminPanelJson.toString();
    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        buttonPanel = new ButtonToPanel("Admin", numberType);
        if (numberType == NumberType.ADMIN)
            buildPanel();
        return commandArray;
    }

    @Override
    public void buildPanel() {
        List<String> listYesNo = new ArrayList<>(Arrays.asList("Yes", "No"));

        //        # region get White List
        JSONObject getWhiteList = buttonPanel.getButtonSimple(ADMIN_PANEL_WHITE_LIST);
        commandArray.add(commandArray.size(), getWhiteList);
        // #endregion

        // #region add to White List
        JSONObject whiteListAdd = buttonPanel.getButtonTypeText(ADMIN_PANEL_WHITE_LIST_ADD,
                                                                "enter_number");
        commandArray.add(commandArray.size(), whiteListAdd);
        // #endregion

        // #region delete from White List
        if (!ApplicationSettings.INSTANCE.getWhiteList().isEmpty()) {
            List<String> list = new ArrayList<>(ApplicationSettings.INSTANCE.getWhiteList());

            JSONObject whiteListDell = buttonPanel.getButtonTypeChoose(ADMIN_PANEL_WHITE_LIST_DELL,
                                                                        list,
                                                                        "select",
                                                                        "text_field");
            commandArray.add(commandArray.size(), whiteListDell);
        }
        // #endregion

        // start region add to admin list
        adminListButton();

        // end region add to admin list

        // #region isPublic
         if (ApplicationSettings.INSTANCE.isPublic()){
             JSONObject isPublic =  buttonPanel.getButtonTypeChoose(ADMIN_PANEL_IS_PUBLIC,
                                                        listYesNo,
                                                        ADMIN_PANEL_IS_PUBLIC_BOT,
                                            "text_field");
             commandArray.add(commandArray.size(), isPublic);
         } else {
             JSONObject isPublic =  buttonPanel.getButtonTypeChoose(ADMIN_PANEL_IS_PRIVATE,
                                                        listYesNo,
                                                         ADMIN_PANEL_IS_PRIVATE_BOT,
                                            "text_field");
             commandArray.add(commandArray.size(), isPublic);

         }


        // #endregion

        // #region reboot
        JSONObject rebootIot =  buttonPanel.getButtonTypeChoose(ADMIN_PANEL_REBOOT,
                                                                listYesNo,
                                                                "reboot_text",
                                                    "text_field");

        commandArray.add(commandArray.size(), rebootIot);


        JSONObject getButtonAlertMessage =  buttonPanel.getButtonAlertMessage("AlertMessage",
                                                                    "reboot_text");
        commandArray.add(commandArray.size(), getButtonAlertMessage);
        // #endregion

        // #region timezone choose
        TimezoneController timezoneController = new TimezoneController();
        List<String> timezones = timezoneController.getFullTimezoneList();

        JSONObject timezone =  buttonPanel.getButtonTypeChoose(ADMIN_PANEL_TIMEZONE,
                                                                timezones,
                                                                "select",
                                                    "text_field");

        commandArray.add(commandArray.size(), timezone);
        // #endregion

        // wifi
        JSONObject wifi = buttonPanel.getButtonDoubleTypeText(ADMIN_PANEL_WIFI,
                                                            "wifi_ssid",
                                                            "wifi_psk");
        commandArray.add(commandArray.size(), wifi);
        // #endregion

        // Clean Space Button
        JSONObject cleanSpaceButton = buttonPanel.getButtonAlertMessage(ADMIN_PANEL_CLEAN_SPACE_BUTTON, "The command will clear all files to prevent camera space deadlock.");
        commandArray.add(commandArray.size(), cleanSpaceButton);
        // #endregion


        JSONObject buttonExit = buttonPanel.buttonExit("main_panel");
        commandArray.add(commandArray.size(), buttonExit);
    }

    private void adminListButton() {
        //        # region get White List
        JSONObject getAdminList = buttonPanel.getButtonSimple(ADMIN_PANEL_ADMIN_LIST);
        commandArray.add(commandArray.size(), getAdminList);
        // #endregion

        // #region add to White List
        JSONObject adminListAdd = buttonPanel.getButtonTypeText(ADMIN_PANEL_ADMIN_LIST_ADD,
                                                                "enter_number");
        commandArray.add(commandArray.size(), adminListAdd);
        // #endregion

        // #region delete from White List
        if(!ApplicationSettings.INSTANCE.getAdminList().isEmpty()) {
            List<String> list = new ArrayList<>(ApplicationSettings.INSTANCE.getAdminList());
            JSONObject adminListDell = buttonPanel.getButtonTypeChoose(ADMIN_PANEL_ADMIN_LIST_DELL,
                                                                    list,
                                                                    "select",
                                                        "text_field");
            commandArray.add(commandArray.size(), adminListDell);
        }
        // #endregion
    }

}
