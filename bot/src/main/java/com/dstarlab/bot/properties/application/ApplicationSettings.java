package com.dstarlab.bot.properties.application;

import com.dstarlab.bot.properties.config.ConfigSettings;

import java.util.HashSet;

public enum ApplicationSettings implements ApplicationInterface {

    INSTANCE;

    ApplicationInterface application = new ApplicationDatabase();
    // ApplicationInterface application = ApplicationFile.INSTANCE;

    public void setApplicationInterface(ApplicationInterface applicationInterface) {
        this.application = applicationInterface;
    }

    @Override
    public boolean isPublic() {
        return application.isPublic();
    }

    @Override
    public void setPublic(boolean isPublic) {
        if (isPublic) {
            if (!application.isPublic()) {
                application.setPublic(true);
            }
            else {
                application.setPublic(false);
            }
        }

    }

    @Override
    public void setHiText(String hiText) {
        application.setHiText(hiText);
    }

    @Override
    public String getHiText() {
        return application.getHiText();
    }

    @Override
    public HashSet<String> getWhiteList() {
        return application.getWhiteList();
    }

   @Override
    public HashSet<String> getAdminList() {
        return application.getAdminList();
    }

    public HashSet<String> getWhiteListAll() {
        HashSet<String> list = application.getWhiteList();
        list.add(ConfigSettings.INSTANCE.getOwner());
        return list;
    }

    @Override
    public HashSet<String> getBlackList() {
        return application.getBlackList();
    }

    @Override
    public void addToWhiteList(String number) {
        application.addToWhiteList(number);
    }

    @Override
    public void addToAdminList(final String number) {
        application.addToAdminList(number);
    }

    @Override
    public void addToBlackList(String number) {
        application.addToBlackList(number);
    }

    @Override
    public void removeFromWhiteList(String number) {
        application.removeFromWhiteList(number);
    }

    @Override
    public void removeFromAdminList(final String number) {
        application.removeFromAdminList(number);
    }

    @Override
    public void removeFromBlackList(String number) {
        application.removeFromBlackList(number);
    }

    @Override
    public boolean isWhiteListed(String number) {
        return application.isWhiteListed(number);
    }

    @Override
    public boolean isAdminListed(final String number) {
        return application.isAdminListed(number);
    }

    @Override
    public boolean isBlackListed(String number) {
        return application.isBlackListed(number);
    }

    @Override
    public int getNumberOfFilesInMessage() {
        return application.getNumberOfFilesInMessage();
    }
}
