package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.properties.VocabularyProperties;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class JsonBuilder {
    JSONObject createJson() {

        return new JSONObject();
    }

    JSONObject addDStarCommandsToJson(List<JSONObject> commandArray){
        JSONObject dStarCommands = new JSONObject();
        dStarCommands.put("command", commandArray);
        return dStarCommands;
    }

    void buildPanelExit(ArrayList<JSONObject> commandArray) {
        JSONObject panelExit = new JSONObject();

        panelExit.put("vocabulary", "main_panel");
        panelExit.put("version", 1);
        panelExit.put("openPanel", "main_panel");
        panelExit.put("icon", "link/icon");

        commandArray.add(commandArray.size(), panelExit);
    }
    JSONObject buttonExit() {
        JSONObject panelExit = new JSONObject();

        panelExit.put("vocabulary", "main_panel");
        panelExit.put("version", 1);
        panelExit.put("openPanel", "main_panel");
        panelExit.put("icon", "link/icon");

        return panelExit;
    }
}
