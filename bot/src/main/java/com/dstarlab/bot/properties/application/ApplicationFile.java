package com.dstarlab.bot.properties.application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

import com.dstarlab.bot.Main;
import com.dstarlab.bot.properties.config.ConfigSettings;

public enum ApplicationFile implements ApplicationInterface {

    INSTANCE;

    private final Properties properties;
    String configName = Main.getUserDirPath() + "application.properties";

    ApplicationFile() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream(configName));
        } catch (IOException e) {
            System.out.println("not ApplicationProperties");
        }
    }

    public boolean isPublic() {
        return Boolean.parseBoolean(properties.getProperty("isPublic"));
    }

    public void setPublic(boolean isPublic) {
        savePropertiesData("isPublic", String.valueOf(isPublic));
    }

    public String getHiText() {
        return properties.getProperty("hiText");
    }

    public void setHiText(String hiText) {
        savePropertiesData("hiText", hiText);
    }

    public int getNumberOfFilesInMessage() {
        return Integer.parseInt(properties.getProperty("numberFilesInMessage"));
    }

    public HashSet<String> getWhiteList() {
        ArrayList<String> list = new ArrayList<>(Arrays.asList(properties.getProperty("whitelist").split(",")));
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).trim());
        }
        return new HashSet<>(list);
    }
    public HashSet<String> getAdminList() {
        ArrayList<String> list = new ArrayList<>(Arrays.asList(properties.getProperty("adminlist").split(",")));
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).trim());
        }
        return new HashSet<>(list);
    }

    public HashSet<String> getBlackList() {
        ArrayList<String> list = new ArrayList<>(Arrays.asList(properties.getProperty("blacklist").split(",")));
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).trim());
        }
        return new HashSet<>(list);
    }

    public void addToWhiteList(String number) {
        HashSet<String> list = getWhiteList();
        list.add(number);
        savePropertiesData("whitelist", String.join(",", list));
    }
    public void addToAdminList(String number) {
        HashSet<String> list = getWhiteList();
        list.add(number);
        savePropertiesData("adminlist", String.join(",", list));
    }

    public void addToBlackList(String number) {
        HashSet<String> list = getBlackList();
        list.add(number);
        savePropertiesData("blacklist", String.join(",", list));
    }

    public void removeFromWhiteList(String number) {
        HashSet<String> list = getWhiteList();
        list.remove(number);
        savePropertiesData("whitelist", String.join(",", list));
    }

    @Override
    public void removeFromAdminList(final String number) {

    }

    public void removeAdminList(String number) {
        HashSet<String> list = getAdminList();
        list.remove(number);
        savePropertiesData("adminlist", String.join(",", list));
    }
    public void removeFromBlackList(String number) {
        HashSet<String> list = getBlackList();
        list.remove(number);
        savePropertiesData("blacklist", String.join(",", list));
    }

    public boolean isWhiteListed(String number) {
        if (number.equals(ConfigSettings.INSTANCE.getOwner())) {
            return true;
        }
        return getWhiteList().contains(number);
    }
    public boolean isAdminListed(String number) {
        if (number.equals(ConfigSettings.INSTANCE.getOwner())) {
            return true;
        }
        return getAdminList().contains(number);
    }
    public boolean isBlackListed(String number) {
        return getBlackList().contains(number);
    }

    private void savePropertiesData(String propertiesName, String propertiesValue) {
        properties.stringPropertyNames();
        properties.setProperty(propertiesName, propertiesValue);

        try {
            FileOutputStream fos = new FileOutputStream(configName);
            properties.store(fos, propertiesName);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}