package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.IconsProperties;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class ButtonToPanel {

    private HashMap<String, String> icons = new IconsProperties().getIconList();
    private String commandType = null;
    private NumberType numberType = null;

    public ButtonToPanel(final String commandType, final NumberType numberType) {
        this.commandType = commandType;
        this.numberType = numberType;
    }

    JSONObject getButtonSimple(String key) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);

        jsonButton.put("structure", structure);
        return jsonButton;
    }

    JSONObject getButtonTypeText(String key, String vocabulary) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        JSONObject typeText = getDataType("text_field", vocabulary, "text");

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(typeText);

        jsonButton.put("structure", structure);
        return jsonButton;
    }

    JSONObject getButtonTypeChoose(final String key,
                                   final List<String> choose,
                                   final String vocabulary,
                                   final String paramName) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        JSONObject typeText = getDataTypeChoose(paramName, vocabulary, choose);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(typeText);

        jsonButton.put("structure", structure);

        return jsonButton;
    }

    JSONObject getButtonChoose(final String key,
                               final JSONObject choose) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(choose);

        jsonButton.put("structure", structure);

        return jsonButton;
    }

    JSONObject getButtonWithParam(final String key,
                                  final JSONObject first,
                                  final JSONObject second,
                                  final JSONObject third,
                                  final JSONObject fourth) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(first);
        structure.add(second);
        structure.add(third);
        structure.add(fourth);

        jsonButton.put("structure", structure);

        return jsonButton;
    }

    JSONObject getButtonWithParam(final String key,
                                  final JSONObject first,
                                  final JSONObject second,
                                  final JSONObject third) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

//        JSONObject typeText = getDataTypeChoose(paramName, vocabulary, choose);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(first);
        structure.add(second);
        structure.add(third);

        jsonButton.put("structure", structure);

        return jsonButton;
    }

    JSONObject getButtonWithParam(final String key,
                                  final JSONObject first,
                                  final JSONObject second) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

//        JSONObject typeText = getDataTypeChoose(paramName, vocabulary, choose);

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(first);
        structure.add(second);

        jsonButton.put("structure", structure);

        return jsonButton;
    }

    JSONObject getButtonInfoMessage(final String key,
                                    final String vocabulary) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonInfoMessage = getDataType(key, vocabulary, "info_message");

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonInfoMessage);

        jsonButton.put("structure", structure);
        return jsonButton;
    }

    JSONObject getButtonDoubleTypeText(final String key,
                                       final String vocabulary,
                                       final String vocabulary2) {
        JSONObject jsonButton = createButton(key);

        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);

        JSONObject buttonCommand = getParamName("command", key);

        JSONObject typeText = getDataType("text_field", vocabulary, "text");
        JSONObject typeText2 = getDataType("additional_text_field", vocabulary2, "text");

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(buttonCommand);
        structure.add(typeText);
        structure.add(typeText2);

        jsonButton.put("structure", structure);
        return jsonButton;
    }

    JSONObject getDataTypeChoose(final String text_field,
                                 final String select,
                                 final List<String> choose) {
        JSONObject typeChoose = new JSONObject();
        typeChoose.put("name", text_field);

        JSONObject addDatatype = new JSONObject();
        addDatatype.put("choose", choose);

        typeChoose.put("param", addDatatype);
        typeChoose.put("vocabulary", select);
        return typeChoose;
    }

    public JSONObject getButtonAlertMessage(final String key,
                                            final String vocabulary) {
        JSONObject jsonButton = createButton(key);
        JSONObject jsonCommandTyp = getParamName("commandTyp", commandType);
        JSONObject jsonCommand = getParamName("command", key);
        JSONObject jsonInfoMessage = getDataType(key, vocabulary, "alert_message");

        ArrayList<JSONObject> structure = new ArrayList<>();
        structure.add(jsonCommandTyp);
        structure.add(jsonCommand);
        structure.add(jsonInfoMessage);

        jsonButton.put("structure", structure);
        return jsonButton;
    }

    private JSONObject createButton(final String key) {
        JSONObject button = new JSONObject();
        button.put("version", 1);
        button.put("vocabulary", key);
        button.put("icon", (icons.containsKey(key) ? key : "default"));
        return button;
    }

    private JSONObject getParamName(final String name,
                                    final String param) {
        JSONObject command = new JSONObject();
        command.put("name", name);
        command.put("param", param);
        return command;
    }

    JSONObject getDataType(String name, String vocabulary, String dataType) {
        JSONObject typeText = new JSONObject();
        typeText.put("name", name);

        JSONObject addDatatype = new JSONObject();
        addDatatype.put("data_type", dataType);

        typeText.put("param", addDatatype);
        typeText.put("vocabulary", vocabulary);
        return typeText;
    }

    JSONObject getDataTypeMinValue(String name, String vocabulary, String dataType, long value) {
        JSONObject typeText = new JSONObject();
        typeText.put("name", name);

        JSONObject addDatatype = new JSONObject();
        addDatatype.put("data_type", dataType);
        addDatatype.put("min_value", value);

        typeText.put("param", addDatatype);
        typeText.put("vocabulary", vocabulary);
        return typeText;
    }

    JSONObject getDataTypeMinMaxValue(String name, String vocabulary, String dataType, long min, long max) {
        JSONObject typeText = new JSONObject();
        typeText.put("name", name);

        JSONObject addDatatype = new JSONObject();
        addDatatype.put("data_type", dataType);
        addDatatype.put("min_value", min);
        addDatatype.put("max_value", max);

        typeText.put("param", addDatatype);
        typeText.put("vocabulary", vocabulary);
        return typeText;
    }
//    private JSONObject getDataTypeText(String name, String vocabulary) {
//        JSONObject typeText = new JSONObject();
//        typeText.put("name", name);
//
//        JSONObject addDatatype = new JSONObject();
//        addDatatype.put("data_type", "text");
//
//        typeText.put("param", addDatatype);
//        typeText.put("vocabulary", vocabulary);
//        return typeText;
//    }
//    private JSONObject getDataTypeInfoMessage(String name, String vocabulary) {
//
//        JSONObject jsonInfoMessage = new JSONObject();
//
//        jsonInfoMessage.put("name", name);
//        JSONObject durationParam = new JSONObject();
//        durationParam.put("data_type", "info_message");
//
//        jsonInfoMessage.put("param", durationParam);
//        jsonInfoMessage.put("vocabulary", vocabulary);
//        return jsonInfoMessage;
//    }

    JSONObject buttonExit(final String key) {
        JSONObject exit = new JSONObject();

        exit.put("vocabulary", key);
        exit.put("version", 1);
        exit.put("openPanel", key);
        exit.put("icon", key);

        return exit;
    }

}
