package com.dstarlab.bot.properties.config;

public interface ConfigInterface {
    public String getNumber();

    public String getOwner();

    public String getVerify();
}