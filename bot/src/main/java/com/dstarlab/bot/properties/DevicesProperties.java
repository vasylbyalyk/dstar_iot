package com.dstarlab.bot.properties;

import com.dstarlab.bot.Main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class DevicesProperties {

    private Properties properties;

    public DevicesProperties() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(Main.getUserDirPath() + "devices.properties"));
        } catch (IOException e) {
            System.out.println("not Properties");
        }
    }

    public ArrayList<String> getDeviceName() {
        return new ArrayList<>(Arrays.asList(properties.getProperty("devices").trim().split(",")));
    }
    public String getDeviceScript(String deviceName) {
        return  properties.getProperty("script_" + deviceName.trim());
    }
    public ArrayList<String> getDeviceCommand(String deviceName) {
        ArrayList<String> command = new ArrayList<>();
        for (String str : new ArrayList<>(Arrays.asList(properties.getProperty("command_" + deviceName.trim()).split(","))))  {
            command.add("\"" + str.trim() + "\"");
        }
//        return command;
        return new ArrayList<>(Arrays.asList(properties.getProperty("command_" + deviceName.trim()).split(",")));
    }

}
