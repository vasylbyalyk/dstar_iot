package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.IconsProperties;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class BlankPanel implements PanelIot {

    private JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject devicePanelJson = jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();
    private HashMap<String, String> icons = new IconsProperties().getIconList();
    private ButtonToPanel buttonPanel = null;

    @Override
    public String sendPanelIot(NumberType numberType) {
        buttonPanel = new ButtonToPanel("Admin", numberType);
        buildPanel();
        devicePanelJson.put("blank_panel", commandArray);

        JSONObject panel = new JSONObject();
        panel.put("panels", devicePanelJson);

        System.out.println("------BlankPanel: " + panel.toString());

        return panel.toString();

    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        if (numberType == NumberType.BLACKLIST)
            buildPanel();

        return commandArray;
    }

    @Override
    public void buildPanel() {
        JSONObject blankPanel = buttonPanel.getButtonInfoMessage("system_off", "system_off_text");
        commandArray.add(commandArray.size(), blankPanel);
    }

}
