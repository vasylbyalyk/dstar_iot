package com.dstarlab.bot.properties.application;

import java.util.HashSet;

public interface ApplicationInterface {

    boolean isPublic();
    void setPublic(boolean isPublic);

    void setHiText(String hiText);
    String getHiText();

    HashSet<String> getWhiteList();
    HashSet<String> getAdminList();
    HashSet<String> getBlackList();

    void addToWhiteList(String number);
    void addToAdminList(String number);
    void addToBlackList(String number);

    void removeFromWhiteList(String number);
    void removeFromAdminList(String number);
    void removeFromBlackList(String number);

    boolean isWhiteListed(String number);
    boolean isAdminListed(String number);
    boolean isBlackListed(String number);

    int getNumberOfFilesInMessage();
}