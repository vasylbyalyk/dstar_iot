package com.dstarlab.bot.properties.config;

import com.dstarlab.bot.Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public enum ConfigFile implements ConfigInterface {
    INSTANCE;

    private final Properties properties;
    String configName = Main.getUserDirPath() + "config.properties";

    ConfigFile() {
        if (!new File(configName).exists()) {
            detectFileProperty();
        }
        properties = new Properties();

        try {
            properties.load(new FileInputStream(configName));
        } catch (IOException e) {
            System.out.println("not ConfigProperties");
        }
    }

    private void detectFileProperty() {
        String volumePath = "/mnt/DATA/config.properties";

        if (!(new File(volumePath).exists())){
            volumePath = Main.getUserDirPath() + "config.properties";
        }

        try {

            Properties propertiesVolume = new Properties();
            propertiesVolume.load(new FileInputStream(volumePath));
            FileOutputStream fos = new FileOutputStream(configName);
            propertiesVolume.store(fos, "new file");
            fos.close();
            if (new File(volumePath).delete()) {
                System.out.println("File " + volumePath + " was deleted");
            } else
                System.out.println("File " + volumePath + " was not deleted");

        } catch (IOException e) {
            System.out.println("not Properties");
        }

    }

    public String getNumber() {
        return properties.getProperty("number");
    }

    public String getOwner() {
        return properties.getProperty("owner");
    }

    public String getVerify() {
        return properties.getProperty("verify");
    }

}