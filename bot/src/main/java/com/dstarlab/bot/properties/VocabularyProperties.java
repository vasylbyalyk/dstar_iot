package com.dstarlab.bot.properties;

public enum VocabularyProperties {
    INSTANCE;

    private final String vocabulary;
    String configName = "vocabulary.txt";


    VocabularyProperties() {
         vocabulary = new ProtocolRead().getProtocol(configName);
     }

    public String getVocabulary() {
        return vocabulary;
    }

}
