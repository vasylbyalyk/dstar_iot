package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.IconsProperties;
import com.dstarlab.bot.properties.VocabularyProperties;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PermissionPanel implements PanelIot {

    private JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject devicePanelJson = jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();
    private HashMap<String, String> icons = new IconsProperties().getIconList();
    @Override
    public String sendPanelIot(NumberType numberType) {

        buildPanel();
        devicePanelJson.put("permission_panel", commandArray);

        JSONObject panel = new JSONObject();
        panel.put("panels", devicePanelJson);
        panel.put("vocabulary", new JSONObject(VocabularyProperties.INSTANCE.getVocabulary()));
        System.out.println("------PermissionPanel: " + panel.toString());

        return panel.toString();

    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        if (numberType == NumberType.BLACKLIST)
            buildPanel();

        return commandArray;
    }

    @Override
    public void buildPanel() {

        JSONObject panelNoCamera = new JSONObject();

        panelNoCamera.put("version", 1);
        panelNoCamera.put("vocabulary", "not_permission");

        JSONObject duration = new JSONObject();
        duration.put("name", "not_permission");
        JSONObject durationParam = new JSONObject();
        durationParam.put("data_type", "info_message");

        duration.put("param", durationParam);
        duration.put("icon", (icons.containsKey("not_permission") ? "not_permission" : "default"));
        duration.put("vocabulary", "not_permission_text");

        ArrayList<JSONObject> structure = new ArrayList<>();

        structure.add(duration);

        panelNoCamera.put("structure", structure);
        commandArray.add(commandArray.size(), panelNoCamera);
    }

}
