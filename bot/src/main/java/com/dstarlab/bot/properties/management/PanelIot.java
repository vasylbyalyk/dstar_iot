package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;

import org.json.JSONObject;

import java.util.ArrayList;

public interface PanelIot {
    String sendPanelIot(NumberType numberType);
    void buildPanel();
    ArrayList<JSONObject> getPanelCommand(NumberType numberType);

}
