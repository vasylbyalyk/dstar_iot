package com.dstarlab.bot.properties;

public class ConfigStrings {

    public static final String PATH = "/mnt/DATA/usb_cameras/camera%s/videos/"; // release
    public static final String SOURCE_PATH = "/mnt/DATA/usb_cameras"; // release
    public static final int VIDEO_COUNT = 6; // 360 number of videos in one folder
    public static final double SPACE_THRESHOLD = 80;

    public static final String PATH_PHOTO = "/mnt/DATA/usb_cameras/camera%s/images/"; // release

    public static final String COMMAND_PHOTO = "photo_once";
    public static final String COMMAND_PHOTO_EVERY = "photo_every";
    public static final String COMMAND_PHOTO_ALL = "photo_all";
    public static final String COMMAND_PHOTO_SCHEDULE = "photo_schedule";
    public static final String COMMAND_VIDEO = "video_once";
    public static final String COMMAND_VIDEO_EVERY = "video_every";
    public static final String COMMAND_VIDEO_SCHEDULE = "video_schedule";
    public static final String COMMAND_VIDEO_RECORD = "video_record";

    public static final String COMMAND_TEXT_PHOTO = "Photo";
    public static final String COMMAND_TEXT_PHOTO_EVERY = "Photo";
    public static final String COMMAND_TEXT_PHOTO_ALL = "Photos from all cameras";
    public static final String COMMAND_TEXT_PHOTO_SCHEDULE = "Photo at a specific time";
    public static final String COMMAND_TEXT_VIDEO = "Video";

    public static final String THREAD_NAME_STOP = "thead-stop-";

    public static final String ADMIN_PANEL_REBOOT = "reboot";
    public static final String ADMIN_PANEL_WHITE_LIST = "white_list";
    public static final String ADMIN_PANEL_WHITE_LIST_ADD = "add_White_list";
    public static final String ADMIN_PANEL_WHITE_LIST_DELL = "dell_white_list";
    public static final String ADMIN_PANEL_ADMIN_LIST = "admin_list";
    public static final String ADMIN_PANEL_ADMIN_LIST_ADD = "add_admin_list";
    public static final String ADMIN_PANEL_ADMIN_LIST_DELL = "dell_admin_list";
    public static final String ADMIN_PANEL_IS_PUBLIC = "is_public";
    public static final String ADMIN_PANEL_IS_PRIVATE = "is_private";
    public static final String ADMIN_PANEL_IS_PUBLIC_BOT = "is_public_bot";
    public static final String ADMIN_PANEL_IS_PRIVATE_BOT = "is_private_bot";

    public static final String ADMIN_PANEL_WIFI = "wifi";
    public static final String ADMIN_PANEL_CLEAN_SPACE_BUTTON = "clean_space_button";
    public static final String ADMIN_PANEL_TIMEZONE = "timezone";

    public static final String IOT_IS_PRIVATE_TEXT = "You don't have permission for sending commands to the Bot. Please, contact with the administrator";

    //
    public static final String PATTERN_MESSAGE = "To: %s\nData: %s";
    public static final String PATTERN_DATA = "MMM dd HH:mm:ss yyyy";
    public static final String PATTERN_TIME = "HH:mm dd/MM/yyyy";
    //
    public static final String WAIT = "Please wait %s s";

    // ERORRS
    private static final String ERROR = "ERROR-%s";

    public static final String ERROR_INVALID_TIME = String.format(ERROR, "InvalidTime");
    public static final String ERROR_INVALID_COMMAND = String.format(ERROR, "InvalidCommand");

    public static final String ERROR_THE_CAMERA_IS_BUSY =
            String.format(ERROR, "TheCameraIsBusy"); // камера занята

    public static final String TEXT_HELLO = "HELLO";
    public static final String TEXT_HI = "HI";
    public static final String TEXT_VIDEO = "VIDEO";
    public static final String TEXT_UPDATE = "UPDATE12";

    public static final String TYPING_STARTED = "STARTED";
    public static final String TYPING_STOPPED = "STOPPED";

    public static final String OWNER_COMMAND = "hi boss\n-reboot\n-error\n-update";
    public static final String OWNER_COMMAND_REBOOT = "reboot";
    public static final String OWNER_COMMAND_ERROR = "error";
    public static final String OWNER_COMMAND_UPDATE = "update";

    // raspberry critical variables
    public static final int PI_CRITICAL_TEMPERATURE_CPU = 70;
    public static final int PI_CRITICAL_FREE_DISK_SPACE = 4;
}
