package com.dstarlab.bot.properties;

import com.dstarlab.bot.Main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

class ProtocolRead {

    String getProtocol(String configName) {
        StringBuilder temp = new StringBuilder();
        try {
//            String configPath = "/home/vasyl/dStar/iot-raspberry-pi/pi/resources/";
            FileInputStream inputStreamReader = new FileInputStream(Main.getUserDirPath() + configName);
            BufferedReader reader = new BufferedReader(new InputStreamReader( inputStreamReader, StandardCharsets.UTF_8));
            String line = reader.readLine();
            while (line != null) {
                temp.append(line);
                temp.append("\n");
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp.toString();
    }

}
