package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.IconsProperties;
import com.dstarlab.bot.properties.application.ApplicationSettings;
import com.dstarlab.bot.properties.config.ConfigSettings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainPanel implements PanelIot {

    private JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject mainPanelJson = jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();
    private HashMap<String, String> icons = new IconsProperties().getIconList();

    @Override
    public String sendPanelIot(NumberType numberType) {
        if (numberType == NumberType.ADMIN) {
            buildPanelForAdmin();
        } else if (numberType == NumberType.WHITELIST) {
            buildPanel();
        } else {
             return new CameraPanel().sendPanelIot(numberType);
        }

        System.out.println("------numberType: " + numberType);

        JSONObject dStarCommands = new JSONObject();
        dStarCommands.put("main_panel", commandArray);

        return dStarCommands.toString();
    }

    @Override
    public void buildPanel() {
        JSONObject panelJsonShowCamera = new JSONObject();

        panelJsonShowCamera.put("vocabulary", "camera_panel");
        panelJsonShowCamera.put("version", 1);
        panelJsonShowCamera.put("openPanel", "camera_panel");
        panelJsonShowCamera.put("icon", (icons.containsKey("camera_panel") ? "camera_panel" : "default"));
        
        commandArray.add(commandArray.size(), panelJsonShowCamera);
//        commandArray.add(commandArray.size(), panelJsonShowDevice);

    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        if (numberType == NumberType.ADMIN)
            buildPanelForAdmin();
         
        return commandArray;
    }

    private void buildPanelForAdmin() {

        JSONObject panelJsonShowAdmin = new JSONObject();
        panelJsonShowAdmin.put("vocabulary", "admin_panel");
        panelJsonShowAdmin.put("version", 1);
        panelJsonShowAdmin.put("openPanel", "admin_panel");
        panelJsonShowAdmin.put("icon", (icons.containsKey("admin_panel") ? "admin_panel" : "default"));

        buildPanel();

        commandArray.add(commandArray.size(), panelJsonShowAdmin);
    }
}
