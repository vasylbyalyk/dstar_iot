package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.application.ApplicationSettings;
import com.dstarlab.bot.properties.config.ConfigSettings;
import com.dstarlab.bot.properties.DevicesProperties;
import org.json.JSONObject;

import java.util.ArrayList;

public class DevicePanel implements PanelIot {
    private DevicesProperties devicesProperties = new DevicesProperties();
    private JsonBuilder jsonBuilder = new JsonBuilder();
    
    private JSONObject devicePanelJson =  jsonBuilder.createJson();
    private ArrayList<JSONObject> commandArray = new ArrayList<>();

    @Override
    public String sendPanelIot(NumberType numberType) {
        buildPanel();
        if (numberType == NumberType.ADMIN || numberType == NumberType.WHITELIST) {
            jsonBuilder.buildPanelExit(commandArray);
        }

        devicePanelJson =  jsonBuilder.addDStarCommandsToJson(commandArray);

        return devicePanelJson.toString();
    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        buildPanel();
        return commandArray;
    }

    @Override
    public void buildPanel(){
        ArrayList<String> device =  devicesProperties.getDeviceName();
        if(device.size() > 0) {
            for (String deviceName : device) {
                JSONObject panelJsonDiscoverDevice = new JSONObject();
                panelJsonDiscoverDevice.put("version", 1);
                panelJsonDiscoverDevice.put("vocabulary", deviceName);

                JSONObject name = new JSONObject();
                name.put("name", "name");
                name.put("param", "IotCommand");

                JSONObject commandTyp = new JSONObject();
                commandTyp.put("name", "commandTyp");
                commandTyp.put("param", "Device");

                JSONObject command = new JSONObject();
                command.put("name", "command");
                command.put("param", "commandDevice");

                JSONObject deviceJson = new JSONObject();
                deviceJson.put("name", "device");
                deviceJson.put("param", deviceName);

                JSONObject device_command = new JSONObject();
                device_command.put("name", "device_command");
                JSONObject choose = new JSONObject();
               choose.put("choose", devicesProperties.getDeviceCommand(deviceName));
                
                device_command.put("param", choose);
                device_command.put("vocabulary", "Select command");

                ArrayList<JSONObject> structure =  new ArrayList<>();
                structure.add(name);
                structure.add(commandTyp);
                structure.add(command);
                structure.add(deviceJson);
                structure.add(device_command);

                panelJsonDiscoverDevice.put("structure", structure);

                commandArray.add(commandArray.size(), panelJsonDiscoverDevice);
            }
        }

        JSONObject panelJsonDiscoverDevice = new JSONObject(

                " {\n" +
                        "          \"version\": 1,\n" +
                        "          \"vocabulary\": \"Discover device\",\n" +
                        "          \"structure\": [\n" +
                        "            {\n" +
                        "              \"name\": \"commandTyp\",\n" +
                        "              \"param\": \"Device\"\n" +
                        "            },\n" +
                        "            {\n" +
                        "              \"name\": \"command\",\n" +
                        "              \"param\": \"discoverDevice\"\n" +
                        "            }\n" +
                        "          ]\n" +
                        "        },\n" +
                        " {\n"
        );
        commandArray.add(commandArray.size(),panelJsonDiscoverDevice);
        commandArray.add(commandArray.size(), jsonBuilder.buttonExit());
    }
}
