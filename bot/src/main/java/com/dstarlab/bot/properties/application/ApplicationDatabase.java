package com.dstarlab.bot.properties.application;

import com.dstarlab.bot.database.DefaultBotSettings;
import com.dstarlab.bot.database.managers.BotManager;
import com.dstarlab.bot.database.managers.NumbersManager;

public class ApplicationDatabase extends NumbersManager implements ApplicationInterface {

    public ApplicationDatabase() {
        super();
    }

    @Override
    public boolean isPublic() {
        return BotManager.getInstance().isPublic();
    }

    @Override
    public void setPublic(boolean isPublic) {
        BotManager.getInstance().setPublic(isPublic);
    }

    @Override
    public void setHiText(String hiText) {
        BotManager.getInstance().setHiText(hiText);
    }

    @Override
    public String getHiText() {
        return BotManager.getInstance().getHiText();
    }

    @Override
    public int getNumberOfFilesInMessage() {
        return DefaultBotSettings.getDefaultNumberFilesInMessage();
    }

}