package com.dstarlab.bot.properties.management;

import com.dstarlab.bot.database.managers.CameraManager;
import com.dstarlab.bot.database.managers.NumberType;
import com.dstarlab.bot.properties.IconsProperties;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_ALL;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_PHOTO_SCHEDULE;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_EVERY;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_RECORD;
import static com.dstarlab.bot.properties.ConfigStrings.COMMAND_VIDEO_SCHEDULE;

public class CameraPanel implements PanelIot {

    private final JsonBuilder jsonBuilder = new JsonBuilder();
    private JSONObject cameraPanelJson = jsonBuilder.createJson();
    private final ArrayList<JSONObject> commandArray = new ArrayList<>();
    private HashMap<String, String> icons = new IconsProperties().getIconList();
    private ButtonToPanel buttonPanel = null;

    @Override
    public String sendPanelIot(NumberType numberType) {
        buttonPanel = new ButtonToPanel("Camera", numberType);
        buildPanel();
        if (numberType == NumberType.ADMIN || numberType == NumberType.WHITELIST) {
            jsonBuilder.buildPanelExit(commandArray);
        }

        cameraPanelJson.put("camera_panel", commandArray);

        return cameraPanelJson.toString();
    }

    @Override
    public ArrayList<JSONObject> getPanelCommand(NumberType numberType) {
        buttonPanel = new ButtonToPanel("Camera", numberType);
        if (numberType != NumberType.BLACKLIST)
            buildPanel();
        if (numberType == NumberType.ADMIN)
            commandArray.add(commandArray.size(), jsonBuilder.buttonExit());

        if (numberType == NumberType.ADMIN)
            commandArray.add(commandArray.size(), jsonBuilder.buttonExit());
        
        return commandArray;
    }

    public void buildPanel() {
                                                                                      
        CameraManager cameraManager;
        cameraManager = CameraManager.INSTANCE;
        int isCamera = cameraManager.getActiveCameras().size();
        System.out.println("------isCamera: " + cameraManager.getActiveCameras());
        if (isCamera == 0) {
            buildNoCamera();
            return;
        }



        JSONObject cameraList = new JSONObject();

        if (isCamera == 0)
            cameraList.put("choose", new int[]{});
        else
            cameraList.put("choose", cameraManager.getActiveIndexes());


        if (isCamera > 1) {
            JSONObject getPhotoAll = buttonPanel.getButtonSimple(COMMAND_PHOTO_ALL);
            commandArray.add(commandArray.size(), getPhotoAll);
        }


        List<String> listCamera = new ArrayList<>();
        cameraManager.getActiveIndexes().forEach(cam -> listCamera.add(cam.toString()));
        JSONObject cameraChoose = buttonPanel.getDataTypeChoose("cameraId","cameraId", listCamera);


        JSONObject getPhotoOnce = buttonPanel.getButtonChoose(COMMAND_PHOTO,
                                                                cameraChoose);
        commandArray.add(commandArray.size(), getPhotoOnce);


        JSONObject timestampStart = buttonPanel.getDataType("time_start", "timestamp_start", "timestamp");
        JSONObject timestampEnd = buttonPanel.getDataType("time_end", "timestamp_end", "timestamp");
        JSONObject timerTyp = buttonPanel.getDataTypeMinValue("timer", "timer", "timer", 60000);

        JSONObject getPhotoEvery = buttonPanel.getButtonWithParam(COMMAND_PHOTO_EVERY,
                                                                        cameraChoose,
                                                                        timestampStart,
                                                                        timestampEnd,
                                                                        timerTyp);
        commandArray.add(commandArray.size(), getPhotoEvery);

        JSONObject getPhotoSchedule = buttonPanel.getButtonWithParam(COMMAND_PHOTO_SCHEDULE,
                cameraChoose,
                timestampStart);
        commandArray.add(commandArray.size(), getPhotoSchedule);
        

        JSONObject getVideoOnce = buttonPanel.getButtonChoose(COMMAND_VIDEO,
                                                                cameraChoose);
        commandArray.add(commandArray.size(), getVideoOnce);

        JSONObject getVideoEvery = buttonPanel.getButtonWithParam(COMMAND_VIDEO_EVERY,
                                                                    cameraChoose,
                                                                    timestampStart,
                                                                    timestampEnd,
                                                                    timerTyp);
        commandArray.add(commandArray.size(), getVideoEvery);

        JSONObject durationTyp = buttonPanel.getDataTypeChoose("duration",
                                                                "time_duration",
                                                                Arrays.asList("1", "2", "3", "4", "5"));

        JSONObject getVideoSchedule = buttonPanel.getButtonWithParam(COMMAND_VIDEO_SCHEDULE,
                                                                        cameraChoose,
                                                                        timestampStart,
                                                                        durationTyp);
        commandArray.add(commandArray.size(), getVideoSchedule);

//        List<String> listCameraVideoRecord = new ArrayList<>();
//        cameraManager.getIndexes().forEach(cam -> listCamera.add(cam.toString()));
//        JSONObject cameraChooseVideoRecord = buttonPanel.getDataTypeChoose("cameraId","cameraId", listCameraVideoRecord);

        JSONObject getVideoRecord = buttonPanel.getButtonWithParam(COMMAND_VIDEO_RECORD,
                                                                    cameraChoose,
                                                                    timestampStart,
                                                                    durationTyp);
        commandArray.add(commandArray.size(), getVideoRecord);

    }

    private void buildNoCamera() {

        JSONObject panelNoCamera = buttonPanel.getButtonInfoMessage("no_camera", "no_camera_text");
        commandArray.add(commandArray.size(), panelNoCamera);

    }
}
