package com.dstarlab.bot;

import com.dstarlab.bot.update.FtpClient;
import com.dstarlab.bot.update.UpdateEventListener;
import com.fasterxml.jackson.core.Version;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

//    public void temp() {
//        String fileName = "/sys/class/thermal/thermal_zone0/temp";
//        String line = null;
//
//        try {
//            FileReader fileReader = new FileReader(fileName);
//
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//
//            while((line = bufferedReader.readLine()) != null) {
//                float tempC = (Integer.parseInt(line) / 1000);
//                float tempF = ((tempC / 5) * 9) + 32;
//                System.out.println("Temp C: " + tempC + " Temp F: " + tempF);
//            }
//
//            bufferedReader.close();
//        }
//        catch(FileNotFoundException ex) {
//            System.out.println("Unable to open file '" + fileName + "'");
//        }
//        catch(IOException ex) {
//            System.out.println("Error reading file '" + fileName + "'");
//        }
//    }
//

    public static String getDataPath() {
        String dataPath = getDataHomeDir();

        // String dataPath = "/home/pi/iot-raspberry-pi/pi/signal-cli";
        if (new File(dataPath).exists()) {
            return dataPath;
        }
        String legacySettingsPath = getDataHomeDir() + "/.config/signal";
        // String legacySettingsPath = "/home/pi/iot-raspberry-pi/pi/.config/signal";
        if (new File(legacySettingsPath).exists()) {
            return legacySettingsPath;
        }

        legacySettingsPath = getDataHomeDir() + "/.config/textsecure";
        // legacySettingsPath = "/home/pi/iot-raspberry-pi/pi/.config/textsecure";
        if (new File(legacySettingsPath).exists()) {
            return legacySettingsPath;
        }

        return dataPath;
    }

    public static String getDataHomeDir() {

        Path path = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();

        return path + "/resources";
    }

    public static String getUserDirPath() {
        Path path = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();
        return path + "/resources/";
    }
}
