package com.dstarlab.bot.database.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Camera")
public class Camera {

    @DatabaseField(columnName = "camera_index")
    private int cameraIndex;
    @DatabaseField(columnName = "video_folder")
    private String videoFolder;
    @DatabaseField(id = true, columnName = "usb_port")
    private String usbPort;
    @DatabaseField(columnName = "vendor")
    private String vendor;
    @DatabaseField(columnName = "is_active")
    private boolean active = false;
    @DatabaseField(columnName = "is_ram")
    private boolean ram = false;

    public Camera() {
    }

    public Camera(final int cameraIndex) {
        this.cameraIndex = cameraIndex;
    }

    public Camera(final int cameraIndex, final String videoFolder, final String usbPort, final boolean active, final String vendor, final boolean ram) {
        this.cameraIndex = cameraIndex;
        this.videoFolder = videoFolder;
        this.usbPort = usbPort;
        this.active = active;
        this.vendor = vendor;
        this.ram = ram;
    }

    public int getCameraIndex() {
        return cameraIndex;
    }

    public void setCameraIndex(final int cameraIndex) {
        this.cameraIndex = cameraIndex;
    }

    public String getVideoFolder() {
        return videoFolder;
    }

    public void setVideoFolder(final String videoFolder) {
        this.videoFolder = videoFolder;
    }

    public String getUsbPort() {
        return usbPort;
    }

    public void setUsbPort(final String usbPort) {
        this.usbPort = usbPort;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(final String vendor) {
        this.vendor = vendor;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public boolean isRam() {
        return ram;
    }

    public void setRam(final boolean ram) {
        this.ram = ram;
    }

    @Override
    public String toString() {
        return cameraIndex + " - Camera[Index: " + cameraIndex
                + ", Folder: " + videoFolder
                + ", Usb: " + usbPort
                + ", Active: " + active
                + ", Vendor: " + vendor
                + ", Ram: " + ram + "]";
    }

    public String superString() {
        return super.toString();
    }
}