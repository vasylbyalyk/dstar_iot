package com.dstarlab.bot.database;

import com.dstarlab.bot.database.managers.BotManager;

public class DefaultBotSettings {



    private static String defaultNumber = "+807807200000000";
    private static String defaultOwner = "+380000000000";
    private static String defaultVerificationKey = "000000";
    private static int defaultPrivacySetting = 0;
    private static String defaultHiText = "Welcome to IoT bot (Command Controller) for remote camera control."
    + " You can use me as a public bot (available for anyone to be used),"
    + " or a private bot (available only for the recipients you choose)."
    + " For more information you are Welcome to visit dstarlab.com/dstar-iot";
    private static boolean defaultIsPublic = false;
    private static boolean defaultIsRam = false;

    private static int defaultNumberFilesInMessage = 2;

    private static BotManager defaultBot = new BotManager(defaultNumber, defaultOwner, defaultVerificationKey, defaultPrivacySetting,
            defaultHiText, defaultIsPublic);

    public String getDefaultNumber() {
        return DefaultBotSettings.defaultNumber;
    }

    public String getDefaultOwner() {
        return DefaultBotSettings.defaultOwner;
    }

    public String getDefaultVerificationKey() {
        return DefaultBotSettings.defaultVerificationKey;
    }

    public int getDefaultPrivacySettings() {
        return DefaultBotSettings.defaultPrivacySetting;
    }

    public String getDefaultHiText() {
        return DefaultBotSettings.defaultHiText;
    }

    public boolean getDefaultIsPublic() {
        return DefaultBotSettings.defaultIsPublic;
    }

    public boolean getDefaultIsRam() {
        return DefaultBotSettings.defaultIsRam;
    }

    public static int getDefaultNumberFilesInMessage() {
        return DefaultBotSettings.defaultNumberFilesInMessage;
    }

    public static BotManager getDefaultBot() {
        return defaultBot;
    }

    @Override
    public String toString() {
        return "DefaultBotSettings{" + getDefaultNumber() +
                getDefaultOwner() +
                getDefaultVerificationKey() +
                getDefaultPrivacySettings() + "}";
    }
}