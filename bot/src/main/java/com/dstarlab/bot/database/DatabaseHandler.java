package com.dstarlab.bot.database;

import com.dstarlab.bot.Main;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.sql.SQLException;

public enum DatabaseHandler {

    INSTANCE;

    private static final String CONNECTION_STRING = "jdbc:sqlite:" + Main.getDataPath() + "/dstar.db";

    private JdbcPooledConnectionSource connectionSource;

    public JdbcPooledConnectionSource getConnection() throws SQLException {
        if (this.connectionSource == null) {
            this.connectionSource = new JdbcPooledConnectionSource(CONNECTION_STRING);
        }
        this.connectionSource.setMaxConnectionsFree(1);
        return this.connectionSource;
    }
}