package com.dstarlab.bot.database.entities;

import com.dstarlab.bot.database.managers.NumberType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "NumberList")
public class PhoneNumber {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField()
    private String number;
    @DatabaseField(columnName = "number_type")
    private NumberType numberType;

    public PhoneNumber() {

    }

    public PhoneNumber(String number, NumberType numberType) {
        this.number = number;
        this.numberType = numberType;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return this.numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }
}