package com.dstarlab.bot.database.managers;

import com.dstarlab.bot.database.DatabaseHandler;
import com.dstarlab.bot.database.entities.Icon;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;

public class IconManager {

    private Dao<Icon, Integer> numbersDao;
    private DeleteBuilder<Icon, Integer> deleteBuilder;

    public IconManager() {
        try {
            final JdbcPooledConnectionSource connectionSource = DatabaseHandler.INSTANCE.getConnection();
            TableUtils.createTableIfNotExists(connectionSource, Icon.class);
            numbersDao = DaoManager.createDao(connectionSource, Icon.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        deleteBuilder = numbersDao.deleteBuilder();
    }

    public void addIcon(String key, String icon) {
        try {
            if (!getIconList().containsKey(key))
                numbersDao.create(new Icon(key, icon));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public void removeIcon(String key) {
        removeFromDatabase(key);
    }


    private void removeFromDatabase(String key) {
        try {
            deleteBuilder.where().eq("key", key);
            deleteBuilder.delete();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

     public HashMap<String, String> getIconList() {
        HashMap<String, String> iconList = new HashMap<>();
        for (Icon icon : numbersDao) {

                iconList.put(icon.getKey(),icon.getIconB64());
        }
        return iconList;
    }

}