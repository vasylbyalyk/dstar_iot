package com.dstarlab.bot.database.managers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import com.dstarlab.bot.database.DatabaseHandler;
import com.dstarlab.bot.database.entities.PhoneNumber;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

public class NumbersManager {

    private Dao<PhoneNumber, Integer> numbersDao;
    DeleteBuilder<PhoneNumber, Integer> deleteBuilder;

    public NumbersManager() {
        try {
            final JdbcPooledConnectionSource connectionSource = DatabaseHandler.INSTANCE.getConnection();
            TableUtils.createTableIfNotExists(connectionSource, PhoneNumber.class);
            numbersDao = DaoManager.createDao(connectionSource, PhoneNumber.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        deleteBuilder = numbersDao.deleteBuilder();
    }

    public void addToWhiteList(String number) {
        try {
            if (!getWhiteList().contains(number))
                numbersDao.create(new PhoneNumber(number, NumberType.WHITELIST));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addToAdminList(String number) {
        try {
            if (!getAdminList().contains(number))
                numbersDao.create(new PhoneNumber(number, NumberType.ADMIN));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addToBlackList(String number) {
        try {
            if (!getBlackList().contains(number))
            numbersDao.create(new PhoneNumber(number, NumberType.BLACKLIST));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFromWhiteList(String number) {
        removeFromDatabase(number);
    }
    public void removeFromAdminList(String number) {
        removeFromDatabase(number);
    }

    public void removeFromBlackList(String number) {
        removeFromDatabase(number);
    }

    private void removeFromDatabase(String number) {
        try {
            deleteBuilder.where().eq("number", number);
            deleteBuilder.delete();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    public HashSet<String> getWhiteList() {
        HashSet<String> whiteListNumbers = new HashSet<>();
        for (PhoneNumber number : numbersDao) {
            if (number.getNumberType().equals(NumberType.WHITELIST))
                whiteListNumbers.add(number.getNumber());
        }
        return whiteListNumbers;
    }
    public HashSet<String> getAdminList() {
        HashSet<String> whiteAdminNumbers = new HashSet<>();
        for (PhoneNumber number : numbersDao) {
            if (number.getNumberType().equals(NumberType.ADMIN))
                whiteAdminNumbers.add(number.getNumber());
        }
        return whiteAdminNumbers;
    }
    public HashSet<String> getBlackList() {
        HashSet<String> blackListNumbers = new HashSet<>();
        for (PhoneNumber number : numbersDao) {
            if (number.getNumberType().equals(NumberType.BLACKLIST))
                blackListNumbers.add(number.getNumber());
        }
        return blackListNumbers;
    }

    public boolean isWhiteListed(String number) {
        if (number.equals(BotManager.getInstance().getOwner())) {
            return true;
        }
        return getWhiteList().contains(number);
    }
    public boolean isAdminListed(String number) {
        if (number.equals(BotManager.getInstance().getOwner())) {
            return true;
        }
        return getAdminList().contains(number);
    }
    public boolean isBlackListed(String number) {
        return getBlackList().contains(number);
    }
}