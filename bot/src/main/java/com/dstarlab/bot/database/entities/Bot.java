package com.dstarlab.bot.database.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Bot")
public class Bot {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String number;
    @DatabaseField
    public String owner;
    @DatabaseField(columnName = "verification_key")
    public String verificationKey;
    @DatabaseField(columnName = "privacy_settings")
    public int privacySettings;
    @DatabaseField(columnName = "hi_text")
    public String hiText;
    @DatabaseField(columnName = "is_public")
    public boolean isPublic;

    public Bot() {

    }

    public Bot(final int id, final String number, final String owner, final String verificationKey, final int privacySettings, final String hiText, final boolean isPublic) {
        this.id = id;
        this.number = number;
        this.owner = owner;
        this.verificationKey = verificationKey;
        this.privacySettings = privacySettings;
        this.hiText = hiText;
        this.isPublic = isPublic;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getVerificationKey() {
        return this.verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public int getPrivacySettings() {
        return this.privacySettings;
    }

    public void setPrivacySettings(int privacySettings) {
        this.privacySettings = privacySettings;
    }

    public String getHiText() {
        return this.hiText;
    }

    public void setHiText(String hiText) {
        this.hiText = hiText;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }
}