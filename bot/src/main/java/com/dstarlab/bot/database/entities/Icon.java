package com.dstarlab.bot.database.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "IconList")
public class Icon {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "key")
    private String key;
    @DatabaseField(columnName = "icon")
    private String iconB64;

    public Icon() {

    }

    public Icon(String key, String iconB64) {
        this.key = key;
        this.iconB64 = iconB64;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getIconB64() {
        return this.iconB64;
    }

    public void setIconB64(String iconB64) {
        this.iconB64 = iconB64;
    }

}