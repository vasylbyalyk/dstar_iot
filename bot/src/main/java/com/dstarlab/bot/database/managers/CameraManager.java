package com.dstarlab.bot.database.managers;

import com.dstarlab.bot.database.DatabaseHandler;
import com.dstarlab.bot.database.entities.Camera;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum CameraManager {

    INSTANCE;

    private Dao<Camera, Integer> cameraDao;
    private UpdateBuilder<Camera, Integer> updateBuilder;

    CameraManager() {
        try {
            final JdbcPooledConnectionSource connectionSource = DatabaseHandler.INSTANCE.getConnection();
            TableUtils.createTableIfNotExists(connectionSource, Camera.class);
            cameraDao = DaoManager.createDao(connectionSource, Camera.class);
            updateBuilder = cameraDao.updateBuilder();
            setAllActive(false);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void registerObserver(Dao.DaoObserver daoObserver) {
        cameraDao.registerObserver(daoObserver);
    }

    public ArrayList<Camera> getCameras() {
        ArrayList<Camera> cameras = new ArrayList<>();
        cameraDao.iterator().forEachRemaining(cameras::add);
        return cameras;
    }

    public List<Camera> getActiveCameras() {
        try {
            QueryBuilder<Camera, Integer> queryBuilder =
                    cameraDao.queryBuilder();
            queryBuilder.where().eq("is_active", true);
            PreparedQuery<Camera> preparedQuery = queryBuilder.prepare();
            return cameraDao.query(preparedQuery);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return Collections.emptyList();
    }

    public ArrayList<Integer> getIndexes() {
        ArrayList<Integer> indexes = new ArrayList<>();
        cameraDao.forEach(camera -> {
            indexes.add(camera.getCameraIndex());
        });
        return indexes;
    }

    public void setActive(Camera camera, boolean isActive) {
        try {
            updateBuilder.where().eq("camera_index", camera.getCameraIndex());
            updateBuilder.updateColumnValue("is_active", isActive);
            updateBuilder.update();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void setAllActive(boolean isActive) {
        try {
            updateBuilder.updateColumnValue("is_active", isActive);
            updateBuilder.update();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Camera getByIndex(int cameraId) throws SQLException {
        return cameraDao.queryForId(cameraId);
    }

    public ArrayList<Integer> getActiveIndexes() {
        ArrayList<Integer> indexes = new ArrayList<>();
        List<Camera> activeCameras = getActiveCameras();
        for (Camera camera : activeCameras) {
            indexes.add(camera.getCameraIndex());
        }
        System.out.println("Active indexes: " + indexes);
        
        return indexes;
//        return new ArrayList<>(Arrays.asList(1,2,3));
    }

    public int getMaxIndex() {
        try {
            return (int) cameraDao.queryRawValue("select MAX(camera_index) from Camera");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public void updateVideoFolder(Camera camera) {
        try {
            updateBuilder.where().eq("usb_port", camera.getUsbPort());
            updateBuilder.updateColumnValue("video_folder", camera.getVideoFolder());
            updateBuilder.update();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public boolean isRam(int camera_index) {
        try {
            return cameraDao.queryForId(camera_index).isRam();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<String> getUsbPorts() {
        ArrayList<String> usbPorts = new ArrayList<>();
        for (Camera camera : getCameras()) {
            usbPorts.add(camera.getUsbPort());
        }
        return usbPorts;
    }

    public void setRam(boolean isRam, int camera_index) {
        try {
            updateBuilder.where().eq("camera_index", camera_index);
            updateBuilder.updateColumnValue("is_ram", isRam);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Camera getByUsbPort(String usbPort) throws SQLException {
        List<Camera> matchingResults = cameraDao.queryForEq("usb_port", usbPort);
        return matchingResults.get(0);
    }

    public void insert(Camera camera) {
        try {
            cameraDao.create(camera);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}