package com.dstarlab.bot.database.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

import com.dstarlab.bot.camera.Recorder;
import com.dstarlab.bot.database.DatabaseHandler;
import com.dstarlab.bot.database.DefaultBotSettings;
import com.dstarlab.bot.Main;
import com.dstarlab.bot.database.entities.Bot;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.table.TableUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BotManager extends Bot {

    private UpdateBuilder<Bot, Integer> updateBuilder;
    private Dao<Bot, Integer> BotDao;
    private JdbcPooledConnectionSource connectionSource;
    private static BotManager instance = null;

    private static final Logger logger = LoggerFactory.getLogger(BotManager.class);

    public static synchronized BotManager getInstance() {
        if (instance == null)
            try {
                instance = new BotManager();
            } catch (SQLException e) {
                System.out.println(Arrays.toString(e.getStackTrace()));
            }
        return instance;
    }

    private BotManager() throws SQLException {
        connectionSource = DatabaseHandler.INSTANCE.getConnection();
        TableUtils.createTableIfNotExists(connectionSource, Bot.class);
        BotDao = DaoManager.createDao(connectionSource, Bot.class);
        updateBuilder = BotDao.updateBuilder();

        if (BotDao.countOf() == 0) {
            InnerConfigProperties config = new InnerConfigProperties();
            BotDao.create(DefaultBotSettings.getDefaultBot());
            UpdateBuilder<Bot, Integer> updateBuilder = BotDao.updateBuilder();
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("number", config.getConfigNumber());
            updateBuilder.updateColumnValue("owner", config.getConfigOwner());
            updateBuilder.updateColumnValue("verification_key", config.getConfigVerificationKey());
            updateBuilder.update();
        } else if (getNumber().equals(DefaultBotSettings.getDefaultBot().number)) {
            System.out.println("error reading file");
        } else if (getNumber() == null) {
            DeleteBuilder<Bot, Integer> deleteBuilder = BotDao.deleteBuilder();
            deleteBuilder.where().eq("id", 1);
            deleteBuilder.delete();
        }
    }

    private static class InnerConfigProperties {

        private String number;
        private String owner;
        private String verificationKey;

        InnerConfigProperties() {
            logger.info("InnerConfigProperties");
            setConfigProperties();
        }

        void setConfigNumber(String number) {
            this.number = number;
        }

        void setConfigOwner(String owner) {
            this.owner = owner;
        }

        void setConfigVerificationKey(String verificationKey) {
            this.verificationKey = verificationKey;
        }

        String getConfigNumber() {
            return this.number;
        }

        String getConfigOwner() {
            return this.owner;
        }

        String getConfigVerificationKey() {
            return this.verificationKey;
        }

        private void deleteConfigFile(String volumePath) {
            if (new File(volumePath).delete()) {
                System.out.println("File " + volumePath + " was deleted");
            } else
                System.out.println("File " + volumePath + " was not deleted");
        }

        private void setConfigProperties() {
            logger.info("setConfigProperties");
            logger.info("~~~~~~" + Main.getUserDirPath());
            String volumePath = "/mnt/DATA/config.properties";
            if (!(new File(volumePath).exists())) {
                volumePath = Main.getUserDirPath() + "config.properties";
            }

            logger.info("volumePath: " + volumePath);

            Properties propertiesVolume = new Properties();
            try {
                propertiesVolume.load(new FileInputStream(volumePath));
            } catch (FileNotFoundException e) {
                System.out.println("No such file: " + volumePath);
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println("Error while reading: " + volumePath);
                e.printStackTrace();
            }

            setConfigNumber(propertiesVolume.getProperty("number"));
            setConfigOwner(propertiesVolume.getProperty("owner"));
            setConfigVerificationKey(propertiesVolume.getProperty("verify"));

            deleteConfigFile(volumePath);
        }
    }

    public BotManager(String number, String owner, String verificationKey, int privacy_settings, String hiText,
                      boolean isPublic) {
        this.number = number;
        this.owner = owner;
        this.verificationKey = verificationKey;
        this.privacySettings = privacy_settings;
        this.hiText = hiText;
        this.isPublic = isPublic;
    }

    @Override
    public int getId() {
        try {
            return BotDao.queryForId(1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().id;
    }

    public void setId(int id) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("id", id);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getNumber() {
        try {
            return BotDao.queryForId(1).getNumber();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().number;
    }

    public void setNumber(String number) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("number", number);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getOwner() {
        try {
            return BotDao.queryForId(1).getOwner();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().owner;
    }

    public void setOwner(String owner) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("owner", owner);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getVerificationKey() {
        try {
            return BotDao.queryForId(1).getVerificationKey();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("verification_key", owner);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getPrivacySettings() {
        try {
            return BotDao.queryForId(1).getPrivacySettings();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().privacySettings;
    }

    public void setPrivacySettings(int privacySettings) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("privacy_settings", privacySettings);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getHiText() {
        try {
            return BotDao.queryForId(1).getHiText();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().hiText;
    }

    public void setHiText(String hiText) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("hi_text", hiText);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isPublic() {
        try {
            return BotDao.queryForId(1).isPublic();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return DefaultBotSettings.getDefaultBot().isPublic;
    }

    public void setPublic(boolean isPublic) {
        try {
            updateBuilder.where().eq("id", 1);
            updateBuilder.updateColumnValue("is_public", isPublic);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}