package com.dstarlab.bot.database.managers;

public enum NumberType {
    USER_LIST,
    WHITELIST, 
    BLACKLIST,
    ADMIN
}