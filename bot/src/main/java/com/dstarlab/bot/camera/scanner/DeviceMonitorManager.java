package com.dstarlab.bot.camera.scanner;

import com.dstarlab.bot.camera.Recorder;
import com.dstarlab.bot.camera.defaultsettings.SupportedCameraList;
import com.dstarlab.bot.controller.system.SystemArchitectureController;
import com.dstarlab.bot.database.entities.Camera;
import com.dstarlab.bot.database.managers.CameraManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.dstarlab.bot.properties.ConfigStrings.PATH;
import static com.dstarlab.bot.properties.ConfigStrings.PATH_PHOTO;

public class DeviceMonitorManager {

    private static final Logger logger = LoggerFactory.getLogger(DeviceMonitorManager.class);

    private final ExecutorService executorService;

    public DeviceMonitorManager() {
        int numberOfCameras;
        switch (SystemArchitectureController.INSTANCE.getOS()) {
            case RPI3:
                numberOfCameras = 3;
                break;
            case RPI4:
                numberOfCameras = 2;
                break;
            default:
                numberOfCameras = 0;
        }
        logger.info("Number of simultaneously streams: " + numberOfCameras
                + " for System: " + SystemArchitectureController.INSTANCE.getOS());
        this.executorService = Executors.newFixedThreadPool(numberOfCameras);
    }

    public void update(final Camera camera) {
        found:
        {
            for (SupportedCameraList defaultCamera : SupportedCameraList.values()) {
                if (defaultCamera.cameraSettings.vendor.equals(camera.getVendor())) {
                    executorService.submit(() -> {
                        recorderTask(camera);
                    });
                    break found;
                }
            }
            executorService.submit(() ->
                    recorderTask(camera)
            );
        }
    }

    private void recorderTask(Camera camera) {
        final CameraManager cameraManager = CameraManager.INSTANCE;

        if (cameraManager.getUsbPorts().contains(camera.getUsbPort())) {
            logger.info("Registered in DB, but inactive, updating DB: " + camera);
            cameraManager.updateVideoFolder(camera);
        } else {
            camera.setCameraIndex(cameraManager.getMaxIndex() + 1);
            logger.info("Not in DB, inserting: " + camera);
            cameraManager.insert(camera);
        }

        Camera newCamera = null;
        try {
            newCamera = cameraManager.getByUsbPort(camera.getUsbPort());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        cameraManager.setActive(Objects.requireNonNull(newCamera), true);
        newCamera.setActive(true);

        try {
            Files.createDirectories(Paths.get(String.format(PATH, camera.getCameraIndex())));
            Files.createDirectories(Paths.get(String.format(PATH_PHOTO, camera.getCameraIndex())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Running new process for: " + newCamera);
        Recorder.play(newCamera, SupportedCameraList.getSettings(camera.getVendor()));

        CameraManager.INSTANCE.setActive(camera, false);
        logger.info("CALLBACK: " + camera);
    }
}
