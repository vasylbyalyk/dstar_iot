package com.dstarlab.bot.camera;

import com.sun.jna.Pointer;

import org.freedesktop.gstreamer.lowlevel.GstNative;

public interface BaseParse extends com.sun.jna.Library {

    BaseParse BaseParse = GstNative.load("gstapp", BaseParse.class);

    void gst_base_parse_set_pts_interpolation(Pointer parse, boolean pts_interpolate);

    void gst_base_parse_set_infer_ts(Pointer parse, boolean pts_interpolate);
}

