package com.dstarlab.bot.camera.defaultsettings;

public enum SupportedCameraList {

    LogitechC920(new CameraSettings(
            "Logitech C920",
            1280,
            720,
            5,
            "046d:082d",
            "image/jpeg",
            "v4l2jpegdec",
            "omxh264enc"
    )),

    LogitechC270(new CameraSettings(
            "Logitech C270",
            1280,
            720,
            5,
            "046d:0825",
            "image/jpeg",
            "jpegdec",
            "omxh264enc"
    )),

    LogitechC120(new CameraSettings(
            "Logitech C120",
            640,
            480,
            5,
            "046d:0824",
            "image/jpeg",
            "jpegdec",
            "omxh264enc"
    )),

    CanyonCNS_CWC5(new CameraSettings(
            "Canyon CNS-CWC5",
            1280,
            720,
            5,
            "1bcf:2284",
            "image/jpeg",
            "jpegdec",
            "omxh264enc"
    )),

    A4TechPK_910P(new CameraSettings(
            "A4Tech PK-910P",
            640,
            480,
            30,
            "09da:2690",
            "image/jpeg",
            "v4l2jpegdec",
            "omxh264enc"
    )),

    DefenderC110(new CameraSettings(
            "Defender C110",
            640,
            480,
            15,
            "1908:2310",
            "video/x-raw",
            "",
            "omxh264enc"
    )),

    DummyCamera(new CameraSettings(
            "Unknown Camera",
            640,
            480,
            5,
            "0000:0000",
            "image/jpeg",
            "jpegdec",
            "omxh264enc"
    ));

    public final CameraSettings cameraSettings;

    SupportedCameraList(CameraSettings cameraSettings) {
        this.cameraSettings = cameraSettings;
    }

    public static CameraSettings getSettings(String vendor) {
        for (SupportedCameraList supportedCamera : SupportedCameraList.values()) {
            if (supportedCamera.cameraSettings.vendor.equals(vendor)) {
                return supportedCamera.cameraSettings;
            }
        }
        return DummyCamera.cameraSettings;
    }
}
