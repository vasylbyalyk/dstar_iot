package com.dstarlab.bot.camera.defaultsettings;

public class CameraSettings {

    public String name;
    public int width;
    public int height;
    public int frameRate;
    public final String vendor;
    public final String format;
    public final String decoder;
    public final String encoder;

    public CameraSettings(final String name, final int width, final int height, final int frameRate, final String vendor, final String format, final String decodingCaps, final String encoder) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.frameRate = frameRate;
        this.vendor = vendor;
        this.format = format;
        this.decoder = decodingCaps;
        this.encoder = encoder;
    }

    @Override
    public String toString() {
        return "CameraSettings{" +
                "name='" + name + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", frameRate=" + frameRate +
                ", vendor='" + vendor + '\'' +
                ", format='" + format + '\'' +
                ", decoder='" + decoder + '\'' +
                ", decoder caps='" + getDecoderCaps() + '\'' +
                ", encoder='" + encoder + '\'' +
                ", encoder caps: " + getEncoderCaps() +
                '}';
    }

    public String getDecoderCaps() {
        return format + ",width=" + width + ",height=" + height + ",framerate=" + frameRate + "/1";
    }

    public String getEncoderCaps() {
        if (encoder.contains("omx")) {
            return "target-bitrate=500000 control-rate=variable qos=true interval-intraframes=" + frameRate;
        } else if (encoder.contains("v4l2")) {
            return "extra-controls=\"controls,h264_profile=0,video_bitrate=500000,h264_i_frame_period=" + frameRate + ";\"";
        } else if (encoder.contains("x264")) {
            return "tune=zerolatency key-int-max=" + frameRate;
        }
        return "";
    }
}