package com.dstarlab.bot.camera;

import org.freedesktop.gstreamer.Bin;
import org.freedesktop.gstreamer.Bus;
import org.freedesktop.gstreamer.Element;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Pipeline;
import org.freedesktop.gstreamer.lowlevel.MainLoop;
import org.slf4j.Logger;

import java.util.Observable;
import java.util.Observer;

import static com.dstarlab.bot.camera.BaseParse.BaseParse;

public class GStreamerUtil {

    public static Pipeline buildPipeline(String description) {
        Bin video = Gst.parseBinFromDescription(description, false);
        final Pipeline pipe = new Pipeline();
        pipe.add(video);
        return pipe;
    }

    static void setPTSInter(Bin bin) {
        for (Element element : bin.getElements()) {
            if (element.getName().contains("parse")) {
                BaseParse.gst_base_parse_set_infer_ts(element.getNativeAddress(), true);
                BaseParse.gst_base_parse_set_pts_interpolation(element.getNativeAddress(), true);
            }
        }
    }

    public static Bus setLoggingPipe(Pipeline pipe,
                                     Logger logger,
                                     String threadName,
                                     boolean isEOS,
                                     MainLoop loop,
                                     Observer observer
    ) {

        Bus bus = pipe.getBus();

        bus.connect((Bus.ERROR) (source, code, message) -> {
            Thread.currentThread().setName(threadName);
            logger.error("ERROR from source: '{}', with code: {}, and message '{}'", source, code, message);
            observer.update(new Observable(), message);
        });

        // STATE CHANGING
        bus.connect((source, old, current, pending) -> {
            if (source instanceof Pipeline) {
                Thread.currentThread().setName(threadName);
                logger.info("Pipe STATE changed from {} to new {}", old, current);
            }
        });

        bus.connect((Bus.WARNING) (source, code, message) -> {
            Thread.currentThread().setName(threadName);
            logger.error("WARNING from source: '{}', with code: {}, and message '{}'", source, code, message);
        });

        bus.connect((Bus.INFO) (source, code, message) -> {
            Thread.currentThread().setName(threadName);
            logger.info("INFO from source: '{}', with code: {}, and message '{}'", source, code, message);
        });

        bus.connect((Bus.BUFFERING) (source, percent) -> {
            logger.info(String.valueOf(source));
            logger.info(String.valueOf(percent));
        });

        if (isEOS)
            bus.connect((Bus.EOS) (source) -> {
                pipe.stop();
                loop.quit();
            });
        return bus;
    }
}
