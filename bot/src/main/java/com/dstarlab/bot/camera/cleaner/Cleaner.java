package com.dstarlab.bot.camera.cleaner;

import com.dstarlab.bot.controller.media.VideoFilesController;
import com.dstarlab.bot.database.managers.CameraManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.dstarlab.bot.properties.ConfigStrings.SOURCE_PATH;
import static com.dstarlab.bot.properties.ConfigStrings.SPACE_THRESHOLD;
import static com.dstarlab.bot.properties.ConfigStrings.VIDEO_COUNT;

public class Cleaner {

    private static final Logger logger = LoggerFactory.getLogger(Cleaner.class);
    private final CameraManager cameraManager;

    public Cleaner(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    public void startCleaning() {
        firstCleaning();
        while (true) {
            if (getUsedSpace() > SPACE_THRESHOLD) {
                for (int index : cameraManager.getIndexes()) {
                    Thread thread = new Thread(() -> {
                        List<File> files;
                        VideoFilesController videoFolder = new VideoFilesController(index);
                        String pathFor = videoFolder.getFirstFolder(videoFolder.getPathCameraId());
                        files = videoFolder.getListFileFromFolder(pathFor);
                        if (files.size() == 0) {
                            logger.info("Folder is empty");
                            videoFolder.deleteVideo(pathFor);
                        } else {
                            int countDelete = Math.min(files.size(), VIDEO_COUNT);
                            for (int i = 0; i < countDelete; i++) {
//                                logger.info(String.valueOf(files.get(i).toPath()));
                                videoFolder.deleteVideo(files.get(i).toPath().toString());
                            }
                        }
                    });
                    thread.start();
                }
            }
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public double getUsedSpace() {
        double total = new File(SOURCE_PATH).getTotalSpace() / Math.pow(10, 9);
        double free = new File(SOURCE_PATH).getUsableSpace() / Math.pow(10, 9);
        double used = 100 - (free / total * 100);

        logger.info("Used: " + used + "%");
        return used;
    }

    public void allCleaning() {
        for (int index : cameraManager.getIndexes()) {

                VideoFilesController videoFolder = new VideoFilesController(index);
                List<File> videoListFiles = new ArrayList<>();
                try {
                  videoListFiles =
                      Files.walk(Paths.get(videoFolder.getPathCameraId()))
                          //                          .filter(Files::isRegularFile)
                          .map(path -> new File(path.toString()))
                          .sorted(Comparator.reverseOrder())
                              .collect(Collectors.toList());
                } catch (IOException e) {
                  e.printStackTrace();
                }
                for (File f : videoListFiles) {
                  System.out.println(f.getPath() + ": " + f.delete());
                }
        }
    }

    private void firstCleaning() {
        VideoFilesController folderController = new VideoFilesController();

        for (File folderIn : folderController.findFolder(SOURCE_PATH)) {
            for (File file : folderController.getListFileFromFolder(folderIn.getPath() + "/videos")) {
                folderController.deleteVideo(file.toPath().toString());
            }

        }
    }
}
