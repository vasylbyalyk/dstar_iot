package com.dstarlab.bot.camera.scanner;

import com.dstarlab.bot.camera.cleaner.Cleaner;
import com.dstarlab.bot.database.entities.Camera;
import com.dstarlab.bot.database.managers.CameraManager;

import org.freedesktop.gstreamer.Gst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;

public class DeviceMonitorScanner {

    private static final Logger logger = LoggerFactory.getLogger(DeviceMonitorScanner.class);

    private final CameraManager cameraManager;
    DeviceMonitorManager deviceMonitorManager;

    public DeviceMonitorScanner(DeviceMonitorManager deviceMonitorManager) {
        Gst.init();
        this.cameraManager = CameraManager.INSTANCE;
        this.deviceMonitorManager = deviceMonitorManager;
    }

    public void scan() {
        logger.info("Cleaner initialization");
        Cleaner cleaner = new Cleaner(cameraManager);
        Executors.newSingleThreadExecutor().submit(cleaner::startCleaning);

        Executors.newSingleThreadExecutor().submit(() -> {
            logger.info("Running camera scan!");
            try {
                Process process = Runtime.getRuntime().exec("gst-device-monitor-1.0 Video/Source -f");
                InputStream output = process.getInputStream();
                Thread out = new Thread(new OutErrReader(output, cameraManager));
                out.start();
                process.waitFor();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    class OutErrReader implements Runnable {

        InputStream inputStream;
        CameraManager cameraManager;

        public OutErrReader(InputStream is, CameraManager cameraManager) {
            this.inputStream = is;
            this.cameraManager = cameraManager;
        }

        public void run() {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            try {
                Camera camera = new Camera(1);

                String vendorID = "";
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    if (line.contains("v4l2.device.bus_info")) {
                        camera.setUsbPort(line.split("=")[1].trim());
                    } else if (line.contains("device.path")) {
                        camera.setVideoFolder(line.split("=")[1].trim());
                    } else if (line.contains("device.vendor.id")) {
                        vendorID = line.split("=")[1].trim();
                    } else if (line.contains("device.product.id")) {
                        String productID = line.split("=")[1].trim();
                        camera.setVendor(vendorID + ":" + productID);
                    } else if (line.contains("gst-launch-1.0")) {


                        if (camera.getUsbPort().equals("platform:bcm2835-isp")) {
                            continue;
                        }

                        // TODO WORKAROUND
                        Thread.sleep(1000);

                        deviceMonitorManager.update(camera);
                        camera = new Camera();
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}