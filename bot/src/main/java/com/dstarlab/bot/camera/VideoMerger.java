package com.dstarlab.bot.camera;

import org.freedesktop.gstreamer.Bin;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Pipeline;
import org.freedesktop.gstreamer.lowlevel.MainLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.dstarlab.bot.properties.ConfigStrings.PATH;

public class VideoMerger {

    private static final Logger logger = LoggerFactory.getLogger(VideoMerger.class);

    public static String merge(ArrayList<String> files, int cameraId) {
        Gst.init();

        final Pipeline pipe = new Pipeline();
        MainLoop loop = new MainLoop();

        String OUT_PATH = String.format(PATH, cameraId) + "/"
                + "out_" + System.currentTimeMillis() + ".mp4";

        String pipeline = buildPipeline(OUT_PATH, files);


        Bin video = Gst.parseBinFromDescription(pipeline, false);

        GStreamerUtil.setPTSInter(video);

        pipe.add(video);

        GStreamerUtil.setLoggingPipe(pipe, logger, VideoMerger.class.getName(), true, loop, null);
        pipe.play();
        loop.run();

        return OUT_PATH;
    }

    private static String buildPipeline(String outPath, ArrayList<String> files) {
        StringBuilder VIDEO_BIN_DESCRIPTION = new StringBuilder(
                "concat name=c ! queue ! h264parse ! mp4mux ! filesink location="
        );
        VIDEO_BIN_DESCRIPTION
                .append(outPath)
                .append(" ");

        int count = 0;
        for (String str : files) {
            if (count != 0) VIDEO_BIN_DESCRIPTION.append("queue ! c. ");
            VIDEO_BIN_DESCRIPTION
                    .append("filesrc location=")
                    .append(str)
                    .append(" ! qtdemux ! h264parse name=parse")
                    .append(count)
                    .append(" ! ");
            count++;
        }

        VIDEO_BIN_DESCRIPTION.append(" c.");

        return VIDEO_BIN_DESCRIPTION.toString();
    }
}