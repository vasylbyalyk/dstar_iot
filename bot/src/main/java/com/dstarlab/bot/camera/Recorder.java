package com.dstarlab.bot.camera;

import com.dstarlab.bot.camera.defaultsettings.CameraSettings;
import com.dstarlab.bot.controller.bash.BashConsoleController;
import com.dstarlab.bot.controller.system.SystemUtils;
import com.dstarlab.bot.database.entities.Camera;

import org.freedesktop.gstreamer.Bin;
import org.freedesktop.gstreamer.Bus;
import org.freedesktop.gstreamer.Closure;
import org.freedesktop.gstreamer.Element;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.Pipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.dstarlab.bot.properties.ConfigStrings.PATH;

public class Recorder {

    private static final Logger logger = LoggerFactory.getLogger(Recorder.class);

    public static void play(Camera camera, CameraSettings supportedCameraSettings) {

        String VIDEO_BIN_DESCRIPTION =
                "v4l2src device=" + camera.getVideoFolder()
                        + " io-mode=2 num-buffers=-1 do-timestamp=true"
                        + " ! queue ! " + supportedCameraSettings.getDecoderCaps();

        if (supportedCameraSettings.getDecoderCaps().contains("jpeg")) {
            VIDEO_BIN_DESCRIPTION += " ! queue ! jpegparse ! tee name=t ! queue ! " + supportedCameraSettings.decoder;
        } else if (supportedCameraSettings.getDecoderCaps().contains("h264")) {
            VIDEO_BIN_DESCRIPTION += " ! h264parse ! queue ! decodebin ";
        }
        else {
            VIDEO_BIN_DESCRIPTION += " ! tee name=t";
        }

        VIDEO_BIN_DESCRIPTION += " ! queue ! clockoverlay text=\"Camera " + camera.getCameraIndex() + ":\""
                + " ! videoconvert ! videorate ! videoscale"
                + " ! queue ! " + supportedCameraSettings.encoder
                + " " + supportedCameraSettings.getEncoderCaps()
                + " ! video/x-h264,profile=baseline"
                + " ! queue ! h264parse config-interval=1 name=parse"
                + " ! queue ! splitmuxsink"
                + " name=splitmuxsink" + camera.getCameraIndex()
                + " muxer=\"mp4mux name=muxxxer\" send-keyframe-requests=true max-size-time=10000000000";
        if (supportedCameraSettings.getDecoderCaps().contains("raw")) {
            VIDEO_BIN_DESCRIPTION += " t. ! queue ! jpegenc ! queue ! rtpjpegpay ! queue ! udpsink host=127.0.0.1 port=500" + camera.getCameraIndex();
        } else {
            VIDEO_BIN_DESCRIPTION += " t. ! queue ! rtpjpegpay ! queue ! udpsink host=127.0.0.1 port=500" + camera.getCameraIndex();
        }

        String threadName = "Camera " + camera.getCameraIndex() + " | " + camera.getVideoFolder();
        Thread.currentThread().setName(threadName);

        logger.info(VIDEO_BIN_DESCRIPTION);

        Bin video = null;
        try {
            // FIXME gst library crash is here
            video = Gst.parseBinFromDescription(VIDEO_BIN_DESCRIPTION, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Objects.isNull(video)) {
            logger.info("NULLLLLLLL");
        }
        GStreamerUtil.setPTSInter(video);
        final Pipeline pipe = new Pipeline();
        pipe.add(video);

        Element splitmuxsink = pipe.getElementByName("splitmuxsink" + camera.getCameraIndex());

        splitmuxsink.connect("format-location", new Closure() {
            public String invoke() {
//                logger.info(VIDEO_BIN_DESCRIPTION);

                Thread.currentThread().setName(threadName);
//                logger.info("Camera started new video.");
                camera.setActive(true);

                return getFileName(String.format(PATH, camera.getCameraIndex())
                        + System.currentTimeMillis() / 1000L
                        + ".mp4");
            }
        });

        // TODO migrate to 1.2 and fix core dumping after 1-2 minutes
//    interface FormatLocationCallback extends GstAPI.GstCallback {
//        String callback(Element splitmux, int fragmentId);
//    }
//        FormatLocationCallback formatLocationCallback = (splitmux, fragmentId) -> {
//            System.out.println("NEW VID FROM CAMERA " + camera.getCameraIndex());
//            return String.format(PATH, camera.getCameraIndex())
//                    + System.currentTimeMillis() / 1000L
//                    + ".mp4";
//        };
//
//        splitmuxsink.connect("format-location",
//                FormatLocationCallback.class,
//                formatLocationCallback,
//                formatLocationCallback
//        );

        Bus bus = GStreamerUtil.setLoggingPipe(pipe, logger, threadName, false, null,
                (observable, o) -> {
                    logger.info("Camera " + camera.getCameraIndex() + " has been BROKEN.");
                    camera.setActive(false);
                });

        logger.info("Set to PLAYING: " + camera);

        camera.setActive(true);
        pipe.play();
        logger.info("Waiting for camera to be disconnected: " + camera);
        isConnected(camera);
        logger.info("Camera " + camera.getCameraIndex() + " has been disconnected.");
        pipe.stop();
        pipe.dispose();
        bus.dispose();
        Gst.quit();
    }

    private static String getFileName(String name){
        File filePath = new File(name);
        String fileName = filePath.getName();
        String fileDate = name.substring(name.lastIndexOf("/") + 1, name.lastIndexOf("."));
        String fileFolder = name.substring(0, name.lastIndexOf("/") + 1);;

        String newDirectory = fileFolder + SystemUtils.pathToVideo(Long.parseLong(fileDate) * 1000);

        File newDir = new File(newDirectory);
        if(!newDir.isDirectory()){
            try {
                Files.createDirectories(Paths.get(newDirectory));
            } catch (IOException e) {
                logger.error("Failed to create directory!" + e.getMessage());
            }
        }

        return newDirectory + fileName;
    }

    private static void isConnected(Camera camera) {
        while (Files.exists(Paths.get(camera.getVideoFolder())) && camera.isActive()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void replugUSB(Camera camera) throws InterruptedException, IOException {

        List<String> files = Files.list(Paths.get("/sys/bus/usb/drivers/usb"))
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());

        Pattern pattern = Pattern.compile("-(\\d\\.).*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(camera.getUsbPort());
        if (matcher.find()) {
            String usbPort = matcher.group(0).split("-")[1];
            System.out.println(usbPort);
            BashConsoleController bashConsoleController = new BashConsoleController();

            for (String file : files) {
                if (file.contains(usbPort)) {
                    bashConsoleController.run("echo " + file + " > /sys/bus/usb/drivers/usb/unbind");
                    Thread.sleep(1000);
                    bashConsoleController.run("echo " + file + " > /sys/bus/usb/drivers/usb/bind");
                    return;
                }
            }
        }
    }
}