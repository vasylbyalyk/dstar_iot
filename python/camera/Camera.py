from dataclasses import dataclass
import recording_tools.gstreamer as gstreamer


@dataclass
class CameraData:
    video_folder: str
    usb_port: str
    vendor: str = None
    camera_index: int = None
    is_active: bool = False
    is_ram: bool = False

    def start(self):
        if self.vendor == LogitechC270.vendor:
            gstreamer.record(self, LogitechC270)

    def stop(self):
        pass


class LogitechC270:
    """
    Works fine with external power, image/video quality is crisp.
    Caution: Pi may hang (at least not accessible remotely) when using the UVC video kernel module.
    Fix: load the module using the following magic incantation:
    modprobe uvcvideo nodrop=1 timeout=5000 quirks=0x80
    Without external power (connecting camera directly in RPi) RPi works intermittently.
    It works, say, 10 times and then refuses to work. Then begins to work again.
    """
    name: str = 'Logitech C270'
    width: int = 640
    height: int = 480
    frame_rate: int = 5
    format: str = 'MJPEG'
    vendor = '046d:0825'

    gst_caps: str = f'! image/jpeg,width={width},height={height},framerate={frame_rate} ! jpegparse'
    gst_decoding: str = '! jpegdec'


class UnknownCamera:
    width: int
    height: int
    frame_rate: int
    format: str
