"""
The background script that cleaning the videos/images to save space.

It gets number of cameras by counting created directories by camera_finder
Then starts looped thread for image cleaning :
The ImageCleanerThread deletes oldest images to keep number of photos
in directory around var PHOTO_TO_KEEP

VideoCleanerThread's start once in a minute, deleting last recorded
minute. After deleting oldest video (determine by timestamps) thread
killing himself.
"""

import os
import shutil
import threading
import time
from Database.CameraTable import CameraTable
from util import Logger

CAMERAS_DIRECTORY = '/mnt/DATA/usb_cameras/'
MINUTES_TO_DELETE = 1
SPACE_LIMIT = 5  # %
PHOTO_TO_KEEP = 3
VIDEO_LENGTH = 10

logger = Logger().get_logger()


class VideoCleanerThread(threading.Thread):
    def __init__(self, index):
        threading.Thread.__init__(self)
        self.index = index

    def run(self):
        logger.debug(f'Running VideoCleanerThread for camera №{self.index}')
        camera_table = CameraTable()
        if camera_table.is_ram(self.index):
            videos_directory = f'{CAMERAS_DIRECTORY}camera{self.index}/ram/videos/'
        else:
            videos_directory = f'{CAMERAS_DIRECTORY}camera{self.index}/videos/'
        if len(files_array := os.listdir(videos_directory)) > MINUTES_TO_DELETE * 60 / VIDEO_LENGTH:
            self.delete_video(videos_directory, files_array, MINUTES_TO_DELETE * 60 / VIDEO_LENGTH)

    def delete_video(self, videos_directory, files_array, number_to_delete):
        files_array.sort()
        files_to_remove = files_array[:int(number_to_delete)]
        logger.debug(f'Camera №{self.index} : Removing next files: {files_to_remove}')
        [os.remove(f'{videos_directory}{file_to_remove}') for file_to_remove in files_to_remove]


def get_number_of_cameras():
    return len([i for i in os.listdir(CAMERAS_DIRECTORY)
                if os.path.isdir(f'{CAMERAS_DIRECTORY}{i}')])


def main():
    while 1:
        number_of_cameras = get_number_of_cameras()
        logger.debug(f'Found {number_of_cameras=}')
        total, used, free = shutil.disk_usage(CAMERAS_DIRECTORY)
        space = used / total * 100
        logger.info(f"Space used: {space:.2f}%")
        if space >= SPACE_LIMIT:
            [VideoCleanerThread(i + 1).start() for i in range(0, number_of_cameras)]
        time.sleep(MINUTES_TO_DELETE * 60)


if __name__ == "__main__":
    # We will wait a bit to give camera finder time to find all cameras and create
    # folders for them.
    time.sleep(10)
    main()
