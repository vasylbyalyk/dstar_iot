import numpy as np
import cv2
import ffmpeg
import time
from datetime import datetime

CAPTURE_WIDTH: int = 1280
CAPTURE_HEIGHT: int = 720
VIDEO_RESOLUTION = (CAPTURE_WIDTH, CAPTURE_HEIGHT)
VIDEO_FRAMERATE: float = 10.0
VIDEO_EXTENSION: str = 'mp4'
VIDEO_DURATION: int = 10
IMAGE_FREQUENCY: int = 1

font = cv2.FONT_HERSHEY_SIMPLEX
fontPosition = (50, 50)
fontScale = 1
fontColor = (255, 255, 255)
lineType = 2


def save_video(file_name):
    process = (
        ffmpeg
        .input('pipe:',
               use_wallclock_as_timestamps='true',
               format='rawvideo',
               pix_fmt='bgr24',
               s=f'{CAPTURE_WIDTH}x{CAPTURE_HEIGHT}')
        .output(f'{file_name}.mp4',
                t=f'00:00:{VIDEO_DURATION}',
                video_bitrate='200000',
                vcodec='h264_omx',
                r=VIDEO_FRAMERATE)
        .overwrite_output()
        .run_async(pipe_stdin=True)
    )
    return process


def main():

    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, CAPTURE_WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, CAPTURE_HEIGHT)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
    cap.set(cv2.CAP_PROP_FPS, VIDEO_FRAMERATE)

    ret, frame = cap.read()
    start = time.time()

    # for i in range(5):
    process = save_video(str("name"))
    while cap.isOpened():

        if ret:
            if time.time() - start > (1 / VIDEO_FRAMERATE):
                ret, frame = cap.read()
                start = time.time()

            human_time: str = str(datetime.now()).split('.')[0]
            cv2.putText(
                frame, f'Camera 1: {human_time}', fontPosition, font, fontScale, fontColor, lineType)
            try:
                process.stdin.write(frame)
            except BrokenPipeError:
                print('broken pipe')
                break
            continue

        process.stdin.close()
        process.wait()
        break


if __name__ == '__main__':
    main()
