import subprocess
# from Camera import CameraData
import time


def record(camera_data):
    camera_id = camera_data.camera_index
    video_folder = camera_data.video_folder
    framerate = 5
    resolution = '1280x720'
    codec = 'h264_v4l2m2m'

    text = f'Camera {camera_id}' + '\: %{localtime\:%F %T}'
    fontfile = '/usr/share/fonts/cantarell/Cantarell-Bold.otf'
    fontcolor = 'white'
    fontsize = 45

    subprocess.Popen(['ffmpeg',
                      '-input_format', 'mjpeg',
                      '-framerate', f'{framerate}',
                      '-s', f'{resolution}',
                      '-f', 'v4l2',
                      '-i', f'{video_folder}',
                      '-pix_fmt', 'yuv420p',
                      '-map', '0',
                      '-f', 'segment',
                      '-g', f'{framerate * 2}',
                      '-strftime', '1',
                      '-reset_timestamps', '1',
                      '-c:v', f'{codec}',
                      '-b:v', '5M',
                      '-segment_time', '00:00:10',
                      '-vf', f"drawtext=fontfile={fontfile}:fontsize={fontsize}:fontcolor={fontcolor}:text='{text}'",
                      '%s.mp4'
                      ])

    time.sleep(20)

    subprocess.Popen(['killall', '-9', 'ffmpeg'])


# '-f', 'mpegts', 'udp://127.0.0.1:5050'
# '-profile:v', 'baseline',

if __name__ == '__main__':
    from collections import namedtuple

    Data = namedtuple('data', 'video_folder, usb_port, camera_index')
    Camera = namedtuple('camera', 'data')

    record(Data('/dev/video0', '1.3', 1))
