import ffmpeg

VIDEO_FRAMERATE = 15
CAMERAS_DIRECTORY: str = '/mnt/DATA/usb_cameras/'
# CAMERAS_DIRECTORY: str = '/home/root/'
CAPTURE_WIDTH: int = 1280
CAPTURE_HEIGHT: int = 720
CAPTURE_RESOLUTION = f'{CAPTURE_WIDTH}x{CAPTURE_HEIGHT}'
CODEC = 'h264_omx'
VIDEO_BITRATE = 1_000_000

VIDEO_DURATION = 10


def record(sdp_filename, camera_index):  # camera_data: CameraData):

    # camera_id = camera_data.camera_index
    # video_folder = camera_data.video_folder
    # camera_id = 1
    # video_folder = '/dev/video0'

    # input_stream = ffmpeg.input(
    #     f'{video_folder}',
    #     input_format='mjpeg',
    #     format='v4l2',
    #     framerate=VIDEO_FRAMERATE,
    #     s=f'{CAPTURE_WIDTH}x{CAPTURE_HEIGHT}'
    # )

    input_stream = ffmpeg.input(
        f'{sdp_filename}',
        hide_banner=None,
        # pix_fmt='yuv420p',
        # re=None,
        reorder_queue_size=0,
        nostdin=None,
        nostats=None,
        # rw_timeout=1,
        protocol_whitelist='file,rtp,udp',
        # use_wallclock_as_timestamps=1,
        # vsync=1,
        # copyts=None
        # rw_timeout=200,
        # input_format='mjpeg',
        # framerate=VIDEO_FRAMERATE,
        # s=f'{CAPTURE_WIDTH}x{CAPTURE_HEIGHT}'
    )

    video_output = input_stream.output(
        f'{CAMERAS_DIRECTORY}camera{camera_index}/videos/%s.mp4',
        map=0,
        r=VIDEO_FRAMERATE,
        an=None,
        # shortest=None,
        # stimeout=1,
        # timeout=1,
        # rw_timeout=1,
        # to 1 000 000 and more
        video_bitrate=VIDEO_BITRATE,
        s=f'{CAPTURE_WIDTH}x{CAPTURE_HEIGHT}',
        vcodec='h264_omx',
        f='segment',
        g=VIDEO_FRAMERATE * 2,
        segment_time=f'00:00:{VIDEO_DURATION}',
        reset_timestamps=1,
        strftime=1,
    )

    # photo_output = input_stream.output(
    #     f'{CAMERAS_DIRECTORY}camera{camera_index}/images/`date +%s`.jpeg',
    #     vframes=1
    # )
    # merged_output = ffmpeg.merge_outputs(video_output, photo_output)

    video_output = (
        video_output
        .global_args('-loglevel', 'fatal')
        .run(quiet=True)
    )

    print('ffmpeg ends')
    raise SystemExit
