import sys
import threading
import time
from datetime import datetime
from util import check_accessibility, Logger
import gi

gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')
from gi.repository import GObject, Gst, GLib, GstBase

CAMERAS_DIRECTORY: str = '/mnt/DATA/usb_cameras'

logger = Logger().get_logger()


def format_location_callback(splitmux, fragment_id, camera_index):
    logger.debug(f'Camera №{camera_index} started new video')
    return f'{CAMERAS_DIRECTORY}/camera{camera_index}/videos/{int(datetime.timestamp(datetime.now()))}.mp4'


def record(camera, def_camera_specs):
    Gst.init(sys.argv)
    Gst.init(None)
    Gst.debug_set_active(True)
    Gst.debug_set_colored(False)
    Gst.debug_set_default_threshold(3)

    logger.debug(f'Camera ID matches with {def_camera_specs}')
    width = def_camera_specs.width
    height = def_camera_specs.height
    frame_rate = def_camera_specs.frame_rate

    # for control in camera.controls:
    #     logger.debug(f'Camera №{camera.camera_index} - {control}')

    # v4l2h264enc output-io-mode=5 qos=true
    #   extra-controls="controls,h264_profile=0,video_bitrate=500000,h264_i_frame_period={framerate};"

    # ! omxh264enc
    #       qos=true entropy-mode=0 target-bitrate=250000 control-rate=2 interval-intraframes={framerate}

    command = \
        f"""v4l2src
            device="{camera.video_folder}" io-mode=2 num-buffers=-1 do-timestamp=true
        ! queue
        ! image/jpeg,width={width},height={height},framerate={frame_rate}/1
        ! jpegparse
        ! tee name=t
            ! queue ! jpegdec
            ! queue ! clockoverlay
                        text="Camera {camera.camera_index}: "
            ! video/x-raw
            ! videoconvert ! videoscale ! videorate
            ! queue ! v4l2h264enc output-io-mode=5 qos=true
                        extra-controls="controls,h264_profile=0,video_bitrate=500000,h264_i_frame_period={framerate};"
            ! video/x-h264,profile=baseline
            ! queue ! h264parse config-interval=1 name=parse
            ! queue
            ! splitmuxsink
                       muxer="mp4mux name=muxxxer" max-size-time=10000000000
        t.
            ! queue ! rtpjpegpay
            ! queue ! udpsink
                        host=127.0.0.1 port=500{camera.camera_index}"""
    #

    pipeline = Gst.parse_launch(command)

    # omxh264enc workaround
    it0 = pipeline.iterate_elements()
    while True:
        res0, e = it0.next()
        if e is None:
            break

        if e.name == "parse":
            GstBase.BaseParse.set_infer_ts(e, True)
            GstBase.BaseParse.set_pts_interpolation(e, True)

    # fixed reseved-moov-update-warning. 1000 - random number
    mp4muxer = pipeline.get_by_name('muxxxer')
    mp4muxer.set_property('reserved-moov-update-period', 1000)

    # callback for timestamp in filename
    src = pipeline.get_by_name('splitmuxsink0')
    src.connect("format-location", format_location_callback,
                camera.camera_index)

    logger.info(f'Set state to PLAY: {camera.camera_index}')
    pipeline.set_state(Gst.State.PLAYING)

    logger.info(f'Joins accessibility_thread: {camera.camera_index}')
    accessibility_thread = threading.Thread(
        target=check_accessibility, args=[camera])
    accessibility_thread.start()
    accessibility_thread.join()

    print(f'Set state to STOP: {camera.camera_index}')

    pipeline.send_event(Gst.Event.new_eos())
    time.sleep(1)  # allow for the eos to be propagated
    pipeline.set_state(Gst.State.NULL)

    print(f'DISCONNECTED: {camera.camera_index}')
    return camera
