#!/usr/bin/env python

import sys

import gi

gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst


def main():
    Gst.init(sys.argv)
    Gst.init(None)

    # Create transcoding pipeline
    pipeline = Gst.Pipeline()
    v4lsrc1 = Gst.ElementFactory.make('v4l2src', None)
    v4lsrc1.set_property("device", "/dev/video0")
    pipeline.add(v4lsrc1)

    camera1caps = Gst.Caps.from_string("image/jpeg,width=1280,height=720,framerate=15/1")
    camerafilter1 = Gst.ElementFactory.make("capsfilter", "filter1")
    camerafilter1.set_property("caps", camera1caps)
    pipeline.add(camerafilter1)

    print(pipeline)


if __name__ == '__main__':
    main()
