from threading import Thread

import cv2 as cv
from datetime import datetime
import time
from typing import Tuple
from Camera import Camera, CameraData
from Database.DatabaseManager import CameraTable
from multiprocessing import Process

CAMERAS_DIRECTORY: str = '/mnt/DATA/usb_cameras/'
CAPTURE_HEIGHT: int = 720
CAPTURE_WIDTH: int = 1280
VIDEO_RESOLUTION: Tuple[int, int] = (CAPTURE_WIDTH, CAPTURE_HEIGHT)
VIDEO_FRAMERATE: float = 5.0
VIDEO_EXTENSION: str = '.mp4'
VIDEO_DURATION: int = 10
IMAGE_FREQUENCY: int = 1
# FOURCC: int = -1
# FOURCC: int = cv.VideoWriter_fourcc(*'mp4v')
FOURCC: int = cv.VideoWriter_fourcc(*'avc1')


def get_timestamp() -> str:
    return str(datetime.timestamp(datetime.now())).split('.')[0]


def time_difference(start_time: int) -> int:
    return (cv.getTickCount() - start_time) / cv.getTickFrequency()


def write_list_frames(frames, camera_index, camera_table):
    if camera_table.is_ram(camera_index):
        path = f'{CAMERAS_DIRECTORY}camera{camera_index}/ram/videos/{get_timestamp()}{VIDEO_EXTENSION}'
    else:
        path = f'{CAMERAS_DIRECTORY}camera{camera_index}/videos/{get_timestamp()}{VIDEO_EXTENSION}'

    out = cv.VideoWriter(
        path,
        FOURCC,
        VIDEO_FRAMERATE,
        VIDEO_RESOLUTION)
    [out.write(frame) for frame in frames]
    out.release()


class ImageThread(Thread):
    def __init__(self, camera_index, camera_table, frame):
        Thread.__init__(self)
        self.ram = camera_table.is_ram(camera_index)
        self.frame = frame
        self.camera_index = camera_index

    def run(self):
        if self.ram:
            path = f'{CAMERAS_DIRECTORY}camera{self.camera_index}/ram/images/{get_timestamp()}.jpg'
        else:
            path = f'{CAMERAS_DIRECTORY}camera{self.camera_index}/images/{get_timestamp()}.jpg'

        cv.imwrite(path, self.frame)


def record(camera_data: CameraData) -> None:
    cv.setUseOptimized(False)

    camera: Camera = Camera(VIDEO_FRAMERATE, camera_data)
    camera_table = CameraTable()

    images_start_time = video_start_time = cv.getTickCount()

    frames = []

    while 1:
        reading_frame_time = cv.getTickCount()

        human_time: str = str(datetime.now()).split('.')[0]

        if (frame := camera.get_frame()) is None:
            print('system exit)))))')
            camera_table.update_status(False, camera_data.camera_index)
            raise SystemExit

        frame = camera.put_text(frame, f'Camera {camera_data.camera_index}: {human_time}')
        frames.append(frame)

        if time_difference(video_start_time) >= VIDEO_DURATION:
            Process(target=write_list_frames, args=(frames, camera_data.camera_index, camera_table),
                    daemon=False).start()
            video_start_time, frames = cv.getTickCount(), []
        if time_difference(images_start_time) >= IMAGE_FREQUENCY:
            ImageThread(camera_data.camera_index, camera_table, frame).start()
            images_start_time = cv.getTickCount()

        # We are trying to achieve perfect framerate by sleeping
        # the camera. For 5 frames it will be:
        # 0.2 - %time_spent_on_frame_capturing%
        # if it will be less than 0, frames will be lost
        adaptive_sleep_time = 1 / VIDEO_FRAMERATE - time_difference(reading_frame_time)
        if adaptive_sleep_time > 0:
            time.sleep(adaptive_sleep_time)

# @dataclass
# class TextSettings:
#     position: Tuple[int, int] = (0, 50)
#     font: int = cv.FONT_HERSHEY_SIMPLEX
#     scale: int = 1
#     color: Tuple[int, int, int] = (255, 255, 255)
#     thickness: int = 2


# class Camera:
#     def __init__(self,
#                  frame_rate,
#                  camera_data: CameraData,
#                  capture_width: int = 1280,
#                  capture_height: int = 720):
#         self.camera_data = camera_data
#         # self.__cap: cv.VideoCapture = cv.VideoCapture(self.camera_data.video_folder)
#         # self.__cap.set(cv.CAP_PROP_FRAME_WIDTH, capture_width)
#         # self.__cap.set(cv.CAP_PROP_FRAME_HEIGHT, capture_height)
#         # self.__cap.set(cv.CAP_PROP_FOURCC, cv.VideoWriter_fourcc(*'MJPG'))
#         self.frame_rate = frame_rate
#         self.reading_thread = _ReadingThread(self.__cap, self.frame_rate)
#         self.reading_thread.start()
#         print('started reading thread')

#     def get_frame(self) -> Union[None, numpy.ndarray]:
#         while not self.reading_thread.is_ready:
#             time.sleep(0.1)
#         return self.reading_thread.frame

#     def release_camera(self) -> None:
#         self.__cap.release()

#     @staticmethod
#     def put_text(frame: numpy.ndarray,
#                  text: str,
#                  text_settings: TextSettings = TextSettings) -> numpy.ndarray:
#         return cv.putText(frame,
#                           text,
#                           text_settings.position,
#                           text_settings.font,
#                           text_settings.scale,
#                           text_settings.color,
#                           text_settings.thickness)


# class _ReadingThread(Thread):
#     def __init__(self, cap, frame_rate):
#         Thread.__init__(self)
#         self.cap = cap
#         self.frame_rate = frame_rate
#         self.frame = None
#         self.is_ready = False

#     def run(self):
#         while 1:
#             ret, frame = self.cap.read()
#             self.is_ready = True
#             if ret:
#                 self.frame = frame
#             else:
#                 self.frame = None
#                 self.cap.release()
#                 return None
#             time.sleep(1 / self.frame_rate)
