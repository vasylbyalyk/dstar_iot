from typing import List, Tuple
from Database.db_util import Singleton, execute_statement


class CameraResolutionTable(metaclass=Singleton):
    table_name: str = 'CameraResolution'
    camera_id: str = 'camera_id'
    resolution_id: str = 'resolution_id'

    @staticmethod
    def __init_table__():
        execute_statement(
            f'''CREATE TABLE if not exists {CameraResolutionTable.table_name} (
                {CameraResolutionTable.camera_id} INTEGER,
                {CameraResolutionTable.resolution_id} INTEGER
            )'''
        )

    @staticmethod
    def insert(camera_resolution: List[Tuple[int, int]]):
        execute_statement(
            f'''INSERT INTO {CameraResolutionTable.table_name} (
                {CameraResolutionTable.camera_id},
                {CameraResolutionTable.resolution_id}) 
                VALUES (?, ?)''',
            camera_resolution
        )

    @staticmethod
    def drop_table():
        execute_statement(
            f'DROP TABLE if exists {CameraResolutionTable.table_name}'
        )
