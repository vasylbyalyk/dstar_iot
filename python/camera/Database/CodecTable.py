from Database.db_util import Singleton, execute_statement
from Database.CameraCodecTable import CameraCodecTable
from typing import List


class CodecTable(metaclass=Singleton):
    table_name: str = 'Codec'
    __id: str = 'id'
    __fourcc: str = 'fourcc'

    @staticmethod
    def __init_table__():
        execute_statement(
            f'''CREATE TABLE if not exists {CodecTable.table_name} (
                {CodecTable.__id} INTEGER PRIMARY KEY AUTOINCREMENT,
                {CodecTable.__fourcc} INTEGER, 
                FOREIGN KEY ({CodecTable.__id})
                        REFERENCES CameraCodecTable ({CameraCodecTable.codec_id}) 
            )'''
        )

    @staticmethod
    def insert(fourccs: List[str]):
        for fourcc in fourccs:
            execute_statement(
                f'''INSERT INTO {CodecTable.table_name} (
                        {CodecTable.__fourcc}) 
                        VALUES (?)''',
                (fourcc,)
            )

    @staticmethod
    def drop_table():
        execute_statement(
            f'DROP TABLE if exists {CodecTable.table_name}'
        )
