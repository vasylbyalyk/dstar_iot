from typing import List, Tuple
from Database.db_util import Singleton, execute_statement
from Database.CameraResolutionTable import CameraResolutionTable


class ResolutionTable(metaclass=Singleton):
    table_name: str = 'Resolution'
    __id: str = 'id'
    __width: str = 'width'
    __height: str = 'height'

    @staticmethod
    def __init_table__():
        execute_statement(
            f'''CREATE TABLE if not exists {ResolutionTable.table_name} (
                {ResolutionTable.__id} INTEGER PRIMARY KEY AUTOINCREMENT,
                {ResolutionTable.__width} INTEGER,
                {ResolutionTable.__height} INTEGER,
                FOREIGN KEY ({ResolutionTable.__id})
                        REFERENCES CameraResolutionTable ({CameraResolutionTable.resolution_id}) 
            )'''
        )

    @staticmethod
    def insert(resolutions: List[Tuple[int, int]]):
        for resolution in resolutions:
            print(resolution)
            execute_statement(
                f'''INSERT INTO {ResolutionTable.table_name} (
                    {ResolutionTable.__width},
                    {ResolutionTable.__height}) 
                    VALUES (?, ?)''',
                resolution
            )

    @staticmethod
    def drop_table():
        execute_statement(
            f'DROP TABLE if exists {ResolutionTable.table_name}'
        )
