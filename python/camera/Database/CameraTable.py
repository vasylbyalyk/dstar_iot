import Camera
from typing import List, Tuple
from Database.db_util import Singleton, execute_statement
from Database.CameraResolutionTable import CameraResolutionTable
from Database.CameraCodecTable import CameraCodecTable


class CameraTable(metaclass=Singleton):
    table_name: str = 'Camera'
    __video_folder__: str = 'video_folder'
    __usb_port__: str = 'usb_port'
    __vendor__: str = 'vendor'
    __camera_index__: str = 'camera_index'
    __is_active__: str = 'is_active'
    __is_ram__: str = 'is_ram'

    @staticmethod
    def __init_table__():
        execute_statement(
            f'''CREATE TABLE if not exists {CameraTable.table_name} (
                {CameraTable.__video_folder__} TEXT,
                {CameraTable.__usb_port__} TEXT,
                {CameraTable.__vendor__} TEXT,
                {CameraTable.__camera_index__} INTEGER PRIMARY KEY,
                {CameraTable.__is_active__} INTEGER,
                {CameraTable.__is_ram__} INTEGER,
                FOREIGN KEY ({CameraTable.__camera_index__})
                        REFERENCES CameraResolutionTable ({CameraResolutionTable.camera_id}) 
                FOREIGN KEY ({CameraTable.__camera_index__})
                        REFERENCES CameraCodecTable ({CameraCodecTable.camera_id}) 
            )'''
        )

    def get_all(self):
        return execute_statement(
            f'SELECT * FROM {self.table_name}',
        ) or []

    def is_ram(self, camera_index) -> bool:
        response = self.__find_where(
            self.__is_ram__, self.__camera_index__, (camera_index,))
        if response:
            camera_tuple: Tuple[bool] = response[0]
            return camera_tuple[0]

    def set_all_inactive(self):
        value = (0,)
        execute_statement(
            f'UPDATE {CameraTable.table_name} SET {self.__is_active__} = ?', value
        )

    @staticmethod
    def insert(camera: Camera.CameraData):
        execute_statement(
            f'''INSERT INTO {CameraTable.table_name} (
                    {CameraTable.__video_folder__},
                    {CameraTable.__usb_port__},
                    {CameraTable.__vendor__},
                    {CameraTable.__camera_index__},
                    {CameraTable.__is_active__},
                    {CameraTable.__is_ram__})
                VALUES (?, ?, ?, ?, ?, ?)''',
            tuple(value for field, value in vars(
                camera).items() if not field.startswith('__'))
        )

    @staticmethod
    def drop_table():
        execute_statement(
            f'DROP TABLE if exists {CameraTable.table_name}'
        )

    # FIX security leak
    def update_video_folder(self, camera: Camera.CameraData):
        execute_statement(
            f'''UPDATE {CameraTable.table_name} 
                SET {CameraTable.__video_folder__} = "{camera.video_folder}"
                WHERE {self.__usb_port__} = ?''',
            (camera.usb_port,)
        )

    # FIX security leak
    def update_status(self, status: bool, camera_index: int):
        execute_statement(
            f"""UPDATE {self.table_name}
                SET {self.__is_active__} = {int(status)}
                WHERE {self.__camera_index__} = ?""",
            (camera_index,)
        )

    def find_busy_video_folders(self) -> List[str]:
        response = self.__find_where(
            self.__video_folder__, self.__is_active__, (1,))
        return [columns[0] for columns in response]

    def find_free_video_folders(self) -> List[str]:
        response = self.__find_where(
            self.__video_folder__, self.__is_active__, (0,))
        return [columns[0] for columns in response]

    def find_free_usb_ports(self) -> List[str]:
        response = self.__find_where(
            self.__usb_port__, self.__is_active__, (0,))
        return [columns[0] for columns in response]

    def get_usb_ports(self) -> List[str]:
        return execute_statement(
            f'SELECT {self.__usb_port__} FROM {self.table_name}',
        ) or []

    def __find_where(self, column, where, values) -> List:
        response = execute_statement(
            f'SELECT DISTINCT {column} FROM {self.table_name} WHERE {where} =?',
            values
        )
        return response or []

    # def find_max_camera_index(self) -> int:
    #     response = execute_statement(f'SELECT MAX(camera_index) FROM Camera;')
    #     return next(iter(response), (0,))[0] or 0

    def find_by_index(self, camera_index) -> Camera.CameraData:
        # FIXME shadow name
        response = self.__find_where(
            '*', self.__camera_index__, (camera_index,))
        if response:
            camera_tuple: Tuple[str, str, str, int, bool, bool] = response[0]
            video_folder, usb_port, camera_id, camera_index, is_active, is_ram = camera_tuple
            return Camera.CameraData(video_folder, usb_port, camera_id, camera_index, is_active, is_ram)

    def find_by_video_folder(self, video_folder) -> Camera.CameraData:
        response = self.__find_where(
            '*', self.__video_folder__, (video_folder,))
        if response:
            camera_tuple: Tuple[str, str, str, int, bool, bool] = response[0]
            video_folder, usb_port, camera_id, camera_index, is_active, is_ram = camera_tuple
            return Camera.CameraData(video_folder, usb_port, camera_id, camera_index, is_active, is_ram)

    def find_by_usb_port(self, usb_port) -> Camera.CameraData:
        response = self.__find_where('*', self.__usb_port__, (usb_port,))
        if response:
            camera_tuple: Tuple[str, str, str, int, bool, bool] = response[0]
            video_folder, usb_port, camera_id, camera_index, is_active, is_ram = camera_tuple
            return Camera.CameraData(video_folder, usb_port, camera_id, camera_index, is_active, is_ram)
