from Database.CameraResolutionTable import CameraResolutionTable
from Database.CameraTable import CameraTable
from Database.ResolutionTable import ResolutionTable
from Database.CodecTable import CodecTable
from Database.CameraCodecTable import CameraCodecTable


class DatabaseManager:

    @staticmethod
    def init_database():
        CameraCodecTable.__init_table__()
        CameraResolutionTable.__init_table__()
        CameraTable.__init_table__()
        CodecTable.__init_table__()
        ResolutionTable.__init_table__()

    @staticmethod
    def get_camera_codec_table() -> CameraCodecTable:
        return CameraCodecTable()

    @staticmethod
    def get_camera_resolution_table() -> CameraResolutionTable:
        return CameraResolutionTable()

    @staticmethod
    def get_camera_table() -> CameraTable:
        return CameraTable()

    @staticmethod
    def get_codec_table() -> CodecTable:
        return CodecTable()

    @staticmethod
    def get_resolution_table() -> ResolutionTable:
        return ResolutionTable()
