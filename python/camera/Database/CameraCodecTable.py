from typing import List, Tuple
from Database.db_util import Singleton, execute_statement


class CameraCodecTable(metaclass=Singleton):
    table_name: str = 'CameraCodec'
    camera_id: str = 'camera_id'
    codec_id: str = 'codec_id'

    @staticmethod
    def __init_table__():
        execute_statement(
            f'''CREATE TABLE if not exists {CameraCodecTable.table_name} (
                {CameraCodecTable.camera_id} INTEGER,
                {CameraCodecTable.codec_id} INTEGER
            )'''
        )

    @staticmethod
    def insert(camera_codec: List[Tuple[int, int]]):
        execute_statement(
            f'''INSERT INTO {CameraCodecTable.table_name} (
                {CameraCodecTable.camera_id},
                {CameraCodecTable.codec_id}) 
                VALUES (?, ?)''',
            camera_codec
        )

    @staticmethod
    def drop_table():
        execute_statement(
            f'DROP TABLE if exists {CameraCodecTable.table_name}'
        )
