"""
The main idea of camera_finder logic is described in db_util.py
Once in a two seconds we are scanning new directories in /dev/video
looking for a new video.
"""
import re
from subprocess import Popen, PIPE
import Camera
from util import Logger, make_dirs
from multiprocessing import Pool, current_process
from Database.DatabaseManager import DatabaseManager

logger = Logger().get_logger()


class BackgroundProcessHandler:

    def __init__(self, camera_data):
        self.camera_data = camera_data
        self.pool = Pool(processes=1, initializer=self.initializer)

        make_dirs(self.camera_data.camera_index)
        DatabaseManager.get_camera_table().update_status(True, self.camera_data.camera_index)
        logger.info(f'Running new process for {self.camera_data}')
        self.pool.apply_async(Camera.CameraData.start,
                              (self.camera_data,),
                              callback=self.database_update_callback)
        self.pool.close()

    def initializer(self):
        current_process().name = f'Rec camera №{self.camera_data.camera_index}/video {self.camera_data.video_folder}'

    def database_update_callback(self, camera_data):
        logger.debug(f'CALLBACK: {self.camera_data}')
        DatabaseManager.get_camera_table().update_status(False, self.camera_data.camera_index)
        self.pool.terminate()


def main():
    logger.info('Initializing database')

    DatabaseManager.init_database()
    camera_table = DatabaseManager.get_camera_table()

    logger.info('Setting all cameras to inactive')
    camera_table.set_all_inactive()

    for camera in camera_table.get_all():
        logger.debug(f'Cameras inside database: {camera}')

    with Popen(['gst-device-monitor-1.0', 'Video/Source', '-f'], stdout=PIPE, bufsize=1, universal_newlines=True) as p:
        for line in p.stdout:
            if 'v4l2.device.bus_info' in line:
                usb_port = line.split('=')[1].strip()
            elif 'device.path' in line:
                video_folder = line.split('=')[1].strip()
            elif 'name  ' in line:
                vendor = re.findall(r'\(([^]]+)\)', line.split('  : ')[1].strip())[0]
            elif 'gst-launch-1.0' in line:
                camera = Camera.CameraData(video_folder, usb_port, vendor)
                logger.info(f'Found next camera: {camera}')
                if (usb_port,) in camera_table.get_usb_ports():
                    logger.debug(f'{camera} registered in DB, but inactive, updating DB')
                    camera_table.update_video_folder(camera)
                else:
                    logger.debug(f'{camera} unregistered, inserting in DB')
                    camera_table.insert(camera)
                camera = camera_table.find_by_usb_port(usb_port)
                logger.debug(f'{camera.camera_index=}')
                BackgroundProcessHandler(camera)


if __name__ == "__main__":
    main()
