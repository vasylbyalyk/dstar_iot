import re
import subprocess
from pathlib import Path
import shlex
import time
import logging
from Database.db_util import Singleton

CAMERAS_DIRECTORY = '/mnt/DATA/usb_cameras/'
RESOLUTION_REGEX = r'(\d{3,4}x\d{3,4})'
CODEC_REGEX = r"('[A-Z]{4}')"


class Logger(metaclass=Singleton):
    logger: logging.Logger

    def __init__(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        self.logger = logging.getLogger('camera')

    def get_logger(self):
        return self.logger


def make_dirs(index):
    if not Path(f'{CAMERAS_DIRECTORY}camera{index}').exists():
        Path(f'{CAMERAS_DIRECTORY}camera{index}/videos').mkdir(parents=True, exist_ok=True)
        Path(f'{CAMERAS_DIRECTORY}camera{index}/images').mkdir(parents=True, exist_ok=True)

    # if not Path(f'{CAMERAS_DIRECTORY}camera{index}/ram').exists():
    #     Path(f'{CAMERAS_DIRECTORY}camera{index}/ram/videos').mkdir(parents=True, exist_ok=True)
    #     Path(f'{CAMERAS_DIRECTORY}camera{index}/ram/images').mkdir(parents=True, exist_ok=True)
    #     create_ram_disks(index)


# def create_ram_disks(index):
#     ram_size = get_ram_size() / 2 / 4 / 1000
#
#     sudo_password = 'raspberry'
#     command = shlex.split(
#         f"mount -t tmpfs -o size={int(ram_size)}m camera{index} {CAMERAS_DIRECTORY}camera{index}/ram")
#     p = subprocess.Popen(['sudo', '-S'] + command, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
#                          universal_newlines=True)
#     sudo_prompt = p.communicate(sudo_password + '\n')[1]
#
#     with open("/etc/fstab", "a") as fstab:
#         fstab.write(
#             f"\ncamera{index}  {CAMERAS_DIRECTORY}camera{index}/ram  tmpfs  defaults,size={int(ram_size)}M  0  0")
#
#     subprocess.run(['sudo', 'mount', '-a'])


def get_ram_size():
    command = shlex.split("grep -Eo '[0-9]+' -m 1 /proc/meminfo")
    result = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return int(result.stdout.read())


def get_res(video_folder):
    result = subprocess.run(
        ['v4l2-ctl', '-d', f'{video_folder}', '--list-formats-ext'], stdout=subprocess.PIPE)
    if li := re.findall(RESOLUTION_REGEX, result.stdout.decode('utf-8')):
        return sorted(set(li), key=lambda x: (int(x.split('x')[0]), int(x.split('x')[1])))


def get_codecs(video_folder):
    result = subprocess.run(
        ['v4l2-ctl', '-d', f'{video_folder}', '--list-formats-ext'], stdout=subprocess.PIPE)
    if li := re.findall(CODEC_REGEX, result.stdout.decode('utf-8')):
        return [codec.replace("'", '') for codec in li]


def check_accessibility(camera_data):
    while Path(camera_data.video_folder).exists():
        time.sleep(1)
    return 0


class V4L2Control:
    name: str
    min_val: int
    max_val: int
    step: int
    default: int
    value: int

    def __init__(self, name, min_val, max_val, step, default, value):
        self.name = name
        self.min_val = min_val
        self.max_val = max_val
        self.step = step
        self.default = default
        self.value = value

    def __repr__(self):
        return f'{self.name=} {self.min_val=} {self.max_val=} {self.step=} {self.default=} {self.value=}'


def set_v4l2_control(device_folder, control, value):
    command = f'v4l2-ctl -d {device_folder} --set-ctrl={control}={value}'
    subprocess.Popen(shlex.split(command))


def get_v4l2_control(device_folder, control):
    command = f'v4l2-ctl -d {device_folder} --get-ctrl={control}'


def get_all_controls(device_folder):
    command = f'v4l2-ctl -d {device_folder} --list-ctrls'
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    controls_str = stdout.decode('utf-8').splitlines()
    controls_list = []
    for line in controls_str:
        line = line.strip()
        name = __parse_control(r'^\w+', line)
        min_val = __parse_control(r'min=([0-9]*)', line)
        max_val = __parse_control(r'max=([0-9]*)', line)
        step_val = __parse_control(r'step=([0-9]*)', line)
        default_val = __parse_control(r'default=([0-9]*)', line)
        value = __parse_control(r'value=([0-9]*)', line)

        controls_list.append(V4L2Control(name, min_val, max_val, step_val, default_val, value))

    return controls_list


def __parse_control(control_regex, control_line):
    match_list = re.findall(control_regex, control_line)
    if match_list:
        return match_list[0] or None
