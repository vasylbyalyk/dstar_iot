import argparse
import broadlink
import pickle

HOME_DIRECTORY = '/home/pi'
CONFIGURATION_DIRECTORY = HOME_DIRECTORY + '/iot-raspberry-pi/python-devices/devices_configuration/'


# The right call of script looks like :
# python3 %name_of_script%.py  %name_of_device_from_resources_properties% %command%

parser = argparse.ArgumentParser()
parser.add_argument('name')
parser.add_argument('--on', action="store_true")
parser.add_argument('--off', action="store_true")
parser.add_argument('--nightlights-on', action="store_true")
parser.add_argument('--nightlights-off', action="store_true")
parser.add_argument('--nightlights-status', action="store_true")
parser.add_argument('--status', action="store_true")
parser.add_argument('--energy', action="store_true")


def turn_on(device):
    device.set_power(True)
    print('ON')


def turn_off(device):
    device.set_power(False)
    print('OFF')


def turn_nightlight_on(device):
    device.set_nightlight(True)
    print('NIGHTLIGHTS ON')


def turn_nightlight_off(device):
    device.set_nightlight(False)
    print('NIGHTLIGHTS OFF')


def check_status(device):
    print(device.check_power())


def check_nightlight_status(device):
    print(device.check_nightlight())


def check_consumption(device):
    print(device.get_energy())


def main():
    args = parser.parse_args()
    name_of_device = args.name
    with open(f'{CONFIGURATION_DIRECTORY}{name_of_device}.dictionary', 'rb') as config_dictionary_file:
        device_data = pickle.load(config_dictionary_file)

    device = broadlink.sp2(devtype=device_data['devtype'], host=device_data['host'], mac=device_data['mac'])
    if device.auth():
        if args.on:
            turn_on(device)
        elif args.off:
            turn_off(device)
        elif args.nightlights_on:
            turn_nightlight_on(device)
        elif args.nightlights_off:
            turn_nightlight_off(device)
        elif args.status:
            check_status(device)
        elif args.nightlights_status:
            check_nightlight_status(device)
        elif args.energy:
            check_consumption(device)


if __name__ == '__main__':
    main()
