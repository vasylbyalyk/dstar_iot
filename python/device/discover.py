#!/usr/bin/env python3
import broadlink
import pickle

HOME_DIRECTORY = '/home/pi'
MAIN_DIRECTORY = HOME_DIRECTORY + '/iot-raspberry-pi/python-devices/'
CONFIGURATION_DIRECTORY = MAIN_DIRECTORY + 'devices_configuration/'

TIMEOUT = 10

PLUG_COMMANDS = 'on, off, nightlights-on, nightlights-off, status, energy, nightlights-status'
RELE_COMMANDS = 'on, off, status'


def find_devices():
    return broadlink.discover(timeout=TIMEOUT)


def write_devices_properties(properties_dict):
    f = open(f"{HOME_DIRECTORY}/iot-raspberry-pi/pi/resources/devices.properties", "w+")
    f.write(f'devices = {list_to_string(list(properties_dict.keys()))}\n')
    for name, device_type in properties_dict.items():
        if device_type == '0x7547':  # SC1
            f.write(f'script_{name} = rele.py\ncommand_{name} = {RELE_COMMANDS}\n')
        elif device_type == '0x2733':  # SP2
            f.write(f'script_{name} = plug.py\ncommand_{name} = {PLUG_COMMANDS}\n')
    f.close()


def list_to_string(s):
    str1 = ", "
    return str1.join(s)


def write_to_file(devices):
    properties_dict = {
    }
    for index, device in enumerate(devices):
        if device.auth():
            name = f'device{index}'
            config_dictionary = {
                'mac': ''.join(format(x, '02x') for x in device.mac),
                'host': (f'{device.host[0]}', 80),
                'devtype': hex(device.devtype),
                'name': name
            }
            properties_dict[f'{name}'] = hex(device.devtype)
            with open(f'{CONFIGURATION_DIRECTORY}{name}.dictionary', 'wb') as config_dictionary_file:
                pickle.dump(config_dictionary, config_dictionary_file)
    return properties_dict


def main():
    devices = find_devices()
    if devices:
        properties_dict = write_to_file(devices)
        write_devices_properties(properties_dict)


if __name__ == "__main__":
    main()
    print("Discovering is done!")
