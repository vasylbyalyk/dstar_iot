import argparse
import broadlink
import pickle

from os.path import expanduser

HOME_DIRECTORY = '/home/pi'
CONFIGURATION_DIRECTORY = HOME_DIRECTORY + '/iot-raspberry-pi/python-devices/devices_configuration/'

parser = argparse.ArgumentParser()
parser.add_argument('name')
parser.add_argument('--on', action="store_true")
parser.add_argument('--off', action="store_true")
parser.add_argument('--status', action="store_true")


def turn_on(device):
    device.set_power(True)
    print('ON')


def turn_off(device):
    device.set_power(False)
    print('OFF')


def check_status(device):
    print(device.check_power())


def main():
    args = parser.parse_args()
    name_of_device = args.name
    with open(f'{CONFIGURATION_DIRECTORY}{name_of_device}.dictionary', 'rb') as config_dictionary_file:
        device_data = pickle.load(config_dictionary_file)

    device = broadlink.sp2(devtype=device_data['devtype'], host=device_data['host'], mac=device_data['mac'])
    if device.auth():
        if args.on:
            turn_on(device)
        elif args.off:
            turn_off(device)
        elif args.status:
            check_status(device)


if __name__ == '__main__':
    main()
