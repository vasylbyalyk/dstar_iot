[ ! -d "bot" ] || [ ! -d "libservice" ] && echo "Directory bot or/and libservice DOES NOT exists. Please cd to source project folder!" && exit 1

CURRENT_USER="$USER"
PROJECT_FOLDER="$HOME"/dstar-iot
DATA_FOLDER="/mnt/DATA"
SOURCE_FOLDER=`pwd`

mkdir -p "$PROJECT_FOLDER"/resources
sudo mkdir -p "$DATA_FOLDER"/usb_cameras
sudo chown "$CURRENT_USER": "$DATA_FOLDER"
sudo chmod -R 777 "$DATA_FOLDER"

sed -i 's/omxh264enc/x264enc/g' "$SOURCE_FOLDER"/bot/src/main/java/com/dstarlab/bot/camera/defaultsettings/SupportedCameraList.java

./gradlew clean --parallel
./gradlew build --parallel
./gradlew fatJar --parallel

sed -i 's/x264enc/omxh264enc/g' "$SOURCE_FOLDER"/bot/src/main/java/com/dstarlab/bot/camera/defaultsettings/SupportedCameraList.java

sudo pkill -f 'java -jar' ; sudo pkill 'python3.8'

find -name 'dstar_iot-all*.jar' -exec cp {} "$PROJECT_FOLDER" \;
rm -rf "$PROJECT_FOLDER"/resources/*
cp -r resources/* "$PROJECT_FOLDER"/resources/

if ! command -v gst-inspect-1.0 &>/dev/null; then
  echo "GStreamer could not be found. Please install it."
  echo "sudo apt-get install -y
  gstreamer1.0-tools
  gstreamer1.0-nice
  gstreamer1.0-plugins-bad
  gstreamer1.0-plugins-ugly gstreamer1.0-plugins-good libgstreamer1.0-dev
  libglib2.0-dev
  libgstreamer-plugins-bad1.0-dev
  libsoup2.4-dev
  libjson-glib-dev
  apt-get install gstreamer1.0-plugins-base-apps
  libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio"
  exit 2
fi

#if ! command -v v4l2-ctl &>/dev/null; then
#  echo "v4l2-ctl could not be found"
#  echo "sudo apt install v4l-utils"
#  exit 2
#fi

java -jar "$PROJECT_FOLDER"/dstar_iot-*.jar